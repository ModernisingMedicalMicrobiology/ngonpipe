#!/usr/bin/env python3
import sys
import pandas as pd
from argparse import ArgumentParser

def getMeta(mp):
    df=pd.read_csv(mp)
    alleles=df[['geneAlle','gene']]
    return alleles

def roundup(x,n=1000):
    return int(n * round(float(x)/n))

def addGene(s):
    genes=['penA','mtrR', 'ponA', 'parC', 'porB','gyrA', '23S','pilQ','rpsJ']
    for gene in genes:
        if s.startswith(gene):
            return gene

def getGenes(genesFile,meta):
    headers=['qseqid', 'sseqid', 'pident', 'length', 'mismatch',\
            'gapopen', 'qstart', 'qend', 'sstart', 'send', 'evalue', 'bitscore']
    df=pd.read_csv(genesFile,sep='\t',names=headers)
    df['fqstart']=df.qstart.map(roundup)
    df=df.merge(meta,how='left',left_on='sseqid',right_on='geneAlle')
    idx=df.groupby(['gene','fqstart'])['bitscore'].transform(max) == df['bitscore']
    topHits=df[idx]
    return topHits

def getSNPs(snpfile):
    snps=pd.read_csv(snpfile)
    snps['composite type'].fillna('FN',inplace=True)
    return snps

def filterGene(r,snpf):
    df=snpf[snpf.POS >= r.qstart]
    df=df[df.POS <= r.qend]
    df['Gene']=r.gene
    return df

def filterGenes(genes,snps):
    dfs=[]
    for index, row in genes.iterrows():
        dfs.append(filterGene(row,snps))
    df=pd.concat(dfs)
    groups=df.groupby(['Gene','composite type'])['POS'].count()
    groups=groups.reset_index()
    return groups

def run(opts):
    meta=getMeta(opts.all_metadata)
    genes=getGenes(opts.blast_file,meta)
    snps=getSNPs(opts.snp_file)
    groups=filterGenes(genes,snps)
    groups['sample_id']=opts.sample_id
    groups['guppy_model']=opts.guppy_model
    groups['depth']=opts.depth
    print(groups)
    groups.to_csv(opts.out_file,index=False)

if __name__=="__main__":
    # args
    parser = ArgumentParser(description='compare SNPs in important genes')
    parser.add_argument('-b', '--blast_file', required=True,
                             help='Specify blast6 file')
    parser.add_argument('-m', '--all_metadata', required=True,
                             help='metadata file')
    parser.add_argument('-s', '--snp_file', required=True,
                             help='Specify snp classifications file')
    parser.add_argument('-o', '--out_file', required=True,
                             help='Specify output file')
    parser.add_argument('-i', '--sample_id', required=False,default=None,
                            help='sample id for reference')
    parser.add_argument('-g', '--guppy_model', required=False,default=None,
                            help='guppy model for reference')
    parser.add_argument('-d', '--depth', required=False,default=None,
                            help='depth for reference')
    opts, unknown_args = parser.parse_known_args()
    run(opts)
