import sys
from functools import reduce
import numpy as np  
import pandas as pd  
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt  
import seaborn as sns  
# roc curve and auc score
from sklearn.neighbors import KNeighborsClassifier  
from sklearn.ensemble import RandomForestClassifier  
from sklearn.ensemble import GradientBoostingClassifier
from itertools import cycle
from sklearn.multiclass import OneVsRestClassifier

from sklearn.model_selection import train_test_split  
from sklearn.metrics import roc_curve,auc  
from sklearn.metrics import precision_recall_curve
from sklearn.utils.fixes import signature
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import label_binarize
from sklearn import svm
from sklearn.model_selection import StratifiedKFold
from itertools import permutations
import pickle
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO

basesN=['A', 'C', 'G', 'T']
perm = permutations(basesN,2)
N,perms=0,{}
for p in perm:
    perms.setdefault(p[0],{}).setdefault(p[1],N)
    N+=1

def plot_roc_curve(d,depth,opts):
    for i in d:
        plt.plot(d[i]['fpr'], d[i]['tpr'], label='{0}'.format(i))
    plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC) Curve, mean depth: {0:.1f}'.format(depth))
    plt.legend()
    plt.savefig('{0}_ROC.pdf'.format(opts.sample))
    #plt.show()


def plot_recall_precision(d,depth,opts):
    for i in d:
        average_precision = average_precision_score(d[i]['y_test'], d[i]['y_score'])
        precision, recall, _ = precision_recall_curve(d[i]['y_test'], d[i]['y_score'])
        step_kwargs = ({'step': 'post'}
               if 'step' in signature(plt.fill_between).parameters
               else {})
        plt.step(recall, precision, alpha=0.2,where='post', label='{0} AP:{1:.2f}'.format(i,average_precision))
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title('Precision-Recall curve, mean depth: {0:.1f}'.format(depth))
    plt.legend()
    plt.savefig('{0}_PR.pdf'.format(opts.sample))
    plt.clf()
    #plt.show()


def addBaseChangeN(row):
    r=row['ref']
    a=row['top_base_seq']
    p=perms[r][a]
    return p

def alphaBeta(row):
    if row['REF'] == 'A' and row['ALT'] == 'G':
        return 0
    elif row['REF'] == 'A' and row['ALT'] == 'T':
        return 1
    elif row['REF'] == 'A' and row['ALT'] == 'C':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'A':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'G':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'T':
        return 0
    elif row['REF'] == 'G' and row['ALT'] == 'A':
        return 0
    elif row['REF'] == 'G' and row['ALT'] == 'C':
        return 1
    elif row['REF'] == 'G' and row['ALT'] == 'T':
        return 1
    elif row['REF'] == 'T' and row['ALT'] == 'A':
        return 1
    elif row['REF'] == 'T' and row['ALT'] == 'C':
        return 0
    elif row['REF'] == 'T' and row['ALT'] == 'G':
        return 1

bi={'A':0,
        'C':1,
        'G':2,
        'T':3}

def baseINT(b):
    try:
        if pd.isna(b['ALT']):
            i=bi[b['REF']]
        else:
            i=bi[b['ALT']]
    except:
        i=None
    return i

def getData(pysamFile,truths):
    # add basecall rations from pysamstats file
    df=pd.read_csv(pysamFile,sep='\t')
    df=truths.merge(df,left_on=['#CHROM','POS'],right_on=['chrom','pos'],how='outer')
    df['refINT']=df.ref.map(bi)
    bases=['A','T','C','G','insertions','deletions']
    for b in bases:
        df['{0} %'.format(b)]=(df[b]/df['reads_all'])*100
    context_pos=[1,2,3,4,5,6,7,8,9,10]
    for i in context_pos:
        #df['REF-{0}'.format(i)]=df.refINT.shift(i)
        #df['REF-{0}'.format(i)].fillna(value=4,inplace=True)
        #df['REF+{0}'.format(i)]=df.refINT.shift(-i)
        #df['REF+{0}'.format(i)].fillna(value=4,inplace=True)
        df['REF-{0}'.format(i)]=df['deletions %'].shift(i)
        df['REF-{0}'.format(i)].fillna(value=0,inplace=True)
        df['REF+{0}'.format(i)]=df['deletions %'].shift(-i)
        df['REF+{0}'.format(i)].fillna(value=0,inplace=True)

    df=df[df['ALT']!='N']
    df=df[df['ALT']!='-']
    df['Illumina base']=df.apply(baseINT,axis=1)
    df=df[~df['Illumina base'].isnull()]
#    df['alphabeta']=df.apply(alphaBeta,axis=1)
    df['reads_all']=df[['A','T','C','G']].sum(axis=1)
    df['top_base'] = df[['A','T','C','G']].max(axis=1)
    df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
    df['majority base %'] = (df['top_base'] / df['reads_all'])*100
    df=df[df['majority base %'].notna()]
    #df['baseChange']=df.apply(addBaseChangeN,axis=1)
    df['top_base_seqINT']=df.top_base_seq.map(bi)
    df['SNP validation']=np.where(df['ALT']==df['top_base_seq'],True,False)
    return df

def classify_bases(features,df,feat,opts):
    # train and test
    X=np.array(df[features])
    y=np.array(df['Illumina base'])
    y = label_binarize(y, classes=[0, 1, 2, 3])
    n_classes = y.shape[1]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=1)
    #model = RandomForestClassifier()
    #model = GradientBoostingClassifier()a
    print('training')
    classifier = OneVsRestClassifier(svm.SVC(kernel='linear', probability=True,
                                         random_state=1))
    y_score = classifier.fit(X_train, y_train).decision_function(X_test)
    print('classifying all')
    preds= classifier.predict(X)
    prob = classifier.predict_proba(X)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    return {'fpr':fpr[2],'tpr':tpr[2],'preds':preds,'y_test':y,'y_score':preds,'probs':prob}

#def classify(features,df,feat,opts):
#    # train and test
#    X=np.array(df[features])
#    y=np.array(df['SNP validation'])
#    trainX, testX, trainy, testy = train_test_split(X, y, test_size=0.1, random_state=1)
#    model = RandomForestClassifier()
#    #model = GradientBoostingClassifier()
#    print('training')
#    model.fit(trainX, trainy)
#    prob = model.predict_proba(testX)
#    probs = prob[:, 1]
#    fpr, tpr, thresholds = roc_curve(testy, probs)
#    filename = '{1}_{0}_model.sav'.format(feat.replace(' ','_'),opts.sample)
#    pickle.dump(model, open(filename, 'wb'))
#    print('classifying all')
#    preds= model.predict(X)
#    prob = model.predict_proba(X)
#    importances = model.feature_importances_
#    std = np.std([tree.feature_importances_ for tree in model.estimators_],axis=0)
#    indices = np.argsort(importances)[::-1]
#    plot_feature_importances(importances,std,indices,features,feat,opts)
#    return {'fpr':fpr,'tpr':tpr,'AUC':auc,'probs':probs,'preds':preds,
#            'y_test':Y,'y_score':preds}

def plot_feature_importances(importances,std,indices,features,feat,opts):
    plt.figure()
    plt.title("Relative importance of features")
    df=pd.DataFrame({'Importance':importances,'std':std,'indices':indices,'feature':features})
    df.to_csv('{0}_{1}_feat_importance.csv'.format(opts.sample,feat.replace(' ','_')))
    g = sns.barplot(y='feature', x="Importance", xerr=df['std'], capsize=.2, data=df)
#    plt.bar(importances[indices],features
#            color="r", yerr=std[indices], align="center")
    #plt.xticks(features, indices)
    #plt.xlim([-1, len(features)])
    plt.tight_layout()
    plt.savefig('{0}_{1}_feat_importance.pdf'.format(opts.sample,feat.replace(' ','_')))
    plt.clf()

def classify(features,df,feat,opts):
    # train and test
    X=np.array(df[features])
    Y=np.array(df['SNP validation'])
    trainX, testX, trainy, testy = train_test_split(X, Y, test_size=0.3, random_state=1)
    model = RandomForestClassifier()
    #model = GradientBoostingClassifier()
    model.fit(trainX, trainy)
    print(trainX)
    print(trainy)
    prob = model.predict_proba(testX)
    print(prob)
    probs = prob[:, 1]
    auc = roc_auc_score(testy, probs)
    fpr, tpr, thresholds = roc_curve(testy, probs)
    filename = '{1}_{0}_model.sav'.format(feat.replace(' ','_'),opts.sample)
    pickle.dump(model, open(filename, 'wb'))
    #saveTree(model,features)
    # classify all
    preds= model.predict(X)
    probs=model.predict_proba(X)
    importances = model.feature_importances_
    std = np.std([tree.feature_importances_ for tree in model.estimators_],axis=0)
    indices = np.argsort(importances)[::-1]
    plot_feature_importances(importances,std,indices,features,feat,opts)
    #probs=probs[:, 1]
    return {'fpr':fpr,'tpr':tpr,'AUC':auc,'probs':probs,'preds':preds,
            'y_test':Y,'y_score':preds}

def terms(r,col='feat'):                                              
    if r['SNP validation'] == True and r['ALT'] != r['REF'] and r[col] == True:
        return 'TP'
    elif r['SNP validation'] == True and r['ALT'] != r['REF'] and r[col] == False:
        return 'FN'
    elif r['SNP validation'] == True and r['ALT'] == r['REF'] and r[col] == True:
        return 'TN'
    elif r['SNP validation'] == False and r['top_base_seq'] != r['ALT'] and r[col] == True:
        return 'FP'

def terms2(r,col='classification'):
    if r['SNP validation'] == True and r[col] == True:
        return 'TP'
    elif r['SNP validation'] == True and r[col] == False:
        return 'FN'
    elif r['SNP validation'] == False and r[col] == False:
        return 'TN'
    elif r['SNP validation'] == False and r[col] == True:
        return 'FP'

def table_recall_precision(df,missed,depth,opts):
    #g=sns.barplot('Type','SNPs',hue='Features used',data=df)
    #plt.show()
    df2=df.pivot(index='Features used',columns='Type',values='SNPs')
    df2=df2.merge(missed,on=['Features used'],how='left')
    tags=['FP','TN','TP','FN']
    for tag in tags:
        if tag not in df2:
            df2[tag] = 0
    df2['FN']=df2['FN']+df2['missed']
    df2['Accuracy'] = (df2['TP'] + df2['TN']) / (df2['TP']+df2['FP']+df2['FN']+df2['TN'])
    df2['Precision']= df2['TP'] / (df2['TP']+df2['FP'])
    df2['Recall']   = df2['TP'] / (df2['TP']+df2['FN'])
    df2['F1 Score'] = 2*(df2['Recall'] * df2['Precision']) / (df2['Recall'] + df2['Precision'])
    df2['depth'] = depth
    df2.to_csv('{0}_recall_precision.csv'.format(opts.sample))
    print(df2)

def saveTree(model,features):
    ## Extract single tree
    estimator = model.estimators_[5]
    from sklearn.tree import export_graphviz
    # Export as dot file
    export_graphviz(estimator, out_file='tree.dot', 
                feature_names = features,
                class_names = ['False','True'],
                rounded = True, proportion = False, 
                precision = 2, filled = True)

def getAllTrueSNPs(opts):
    bases=['A','C','G','T']
    true=open(opts.true,'rt')
    ref=open(opts.ref,'rt')
    n=0
    SNPs=[]
    for true_seq,ref_seq in zip(SeqIO.parse(true,'fasta'),SeqIO.parse(ref,'fasta')):
        for t,r in zip(true_seq.seq,ref_seq):
            n+=1
            if t not in bases or r not in bases: continue
            #if t != r:
            SNPs.append({'POS':n,'REF':r,'ALT':t,'#CHROM':ref_seq.id,'Origin':'truth'})
    df=pd.DataFrame(SNPs)
    ref.close()
    true.close()
    return df

def maskProbs(r,feat='feat'):
    if r[feat]==False:
        return False
    elif r[feat] == True and r['{0} prob'.format(feat)] > 0.95:
        return True
    else:
        return False


def run(opts):
    print('getting truths')
    truths=getAllTrueSNPs(opts)
    print('getting data')
    df=getData(opts.pysam,truths)   
    df.to_csv('dataset.csv')
    d={}

    feature_combinations={
        #    'base percents':['A %','T %','C %','G %'],
            'base per dels':['A %','T %','C %','G %','deletions %'],
            'composite':['A %','T %','C %','G %',
                'deletions %','matches_fwd','matches_rev','reads_all',
                'reads_fwd','reads_rev','matches',
                'majority base %','refINT','top_base_seqINT'],
            'composite2':['A %','T %','C %','G %',
                'deletions %','matches_fwd','matches_rev','reads_all',
                'reads_fwd','reads_rev','matches',
                'majority base %','refINT','top_base_seqINT',
                'REF-1','REF-2','REF-3','REF-4','REF-5','REF-6','REF-7','REF-8','REF-9','REF-10',
                'REF+1','REF+2','REF+3','REF+4','REF+5','REF+6','REF+7','REF+8','REF+9','REF+10']
            }

    dfs=[]
    for feat in feature_combinations:
        print('classifying',feat)
        features=feature_combinations[feat]
        d[feat]=classify(features,df,feat,opts)

        #preds=pd.DataFrame(d[feat]['preds'],columns=['A','C','G','T'])
        #preds[feat] = preds[['A','C','G','T']].idxmax(axis=1)
        #preds2=preds[[feat]]
        #df=df.merge(preds2,left_index=True, right_index=True)

        #probs=pd.DataFrame(d[feat]['probs'],columns=['A','C','G','T'])
        #probs[feat] = probs[['A','C','G','T']].idxmax(axis=1)
        #probs['{0} prob'.format(feat)] = probs[['A','C','G','T']].max(axis=1)
        #probs[feat]=np.where(probs['{0} prob'.format(feat)] > 0.997, probs[feat],'N') 
        #probs2=probs[[feat,'{0} prob'.format(feat)]]
        #df=df.merge(probs2,left_index=True, right_index=True)

        df[feat]=d[feat]['preds']
        probs=pd.DataFrame(d[feat]['probs'],columns=[True,False])
        p=probs.max(axis=1)
        df['{0} prob'.format(feat)] = p
        df[feat]=df.apply(maskProbs,axis=1,feat=feat)
        df['{0} type'.format(feat)] = df.apply(terms,axis=1,col=feat)


        #df['SNP validation']=np.where(df['ALT']==df[feat],True,False)
        #df['type']=df.apply(terms,axis=1,col=feat)
        #g=df.groupby(by=['type'])['POS'].count().reset_index()
        #g=g.rename({'type':'classification','POS':feat},axis=1)
        #dfs.append(g)
        #df.rename({'type':'{0} type'.format(feat)},axis=1,inplace=True)
        #g=df.groupby(by=['SNP validation',feat])['POS'].count().reset_index()
        #g=g.rename({feat:'classification','POS':feat},axis=1)
        g=df.groupby(by=['{0} type'.format(feat)])['POS'].count().reset_index()
        g=g.rename({'{0} type'.format(feat):'Type','POS':feat},axis=1)
        dfs.append(g)

    df.to_csv('{0}_classifications.csv'.format(opts.sample),index=False)

    #stats = reduce(lambda left,right: pd.merge(left,right,on=['SNP validation','classification']), dfs)
    #stats['Type']=stats.apply(terms2,axis=1)
    stats = reduce(lambda left,right: pd.merge(left,right,on=['Type']), dfs)
    cols=list(feature_combinations.keys())
    cols.append('Type')
    stats=stats[cols]
    stats=stats.melt(id_vars=['Type'])
    stats=stats.rename({'variable':'Features used','value':'SNPs'},axis=1)
    print(stats)

    missed=[]
    for feat in feature_combinations:
        df2=df[df.Origin=='truth']
        df2=df2[df2[feat].isnull()]
        missed.append({'Features used':feat,'missed':len(df2)})

    missed=pd.DataFrame(missed)

    # plot and save
    depth=df['reads_all'].mean()
    table_recall_precision(stats,missed,depth,opts)
    #plot_recall_precision(d,depth,opts)
    plot_roc_curve(d,depth,opts)





if __name__ == "__main__":
    parser = ArgumentParser(description='train ML from validated SNPs')
    parser.add_argument('-s', '--sample', required=True,
                             help='sample name')
    parser.add_argument('-p', '--pysam', required=True,
                             help='pysam file')
    parser.add_argument('-t', '--true', required=True,
                             help='Truth sequence, e.g. illumina reference')
    parser.add_argument('-r', '--ref', required=True,
                             help='reference sequence')
    opts, unknown_args = parser.parse_known_args()
    run(opts)

