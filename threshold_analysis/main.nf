
params.cudadevice='auto'
params.porechop='off'
params.guppyConfig='/opt/ont/guppy/data/dna_r9.4.1_450bps_fast.cfg'
base=params.base                                                                
params.kit='SQK-LSK109'                                                         
params.flowcell='FLO-MIN106' 
params.ref='/mnt/Data2/MDR_GC/spiked/ngonpipe/basemixes/R00000419.fasta'
params.inFiles='inputs.txt'

//input sample_name, rawf5s, reference, illuminaMappedref
Channel                                                                         
        .fromPath( "${params.inFiles}" )                                        
        .splitCsv()                                                             
        .map{ row -> tuple(row[0], row[1], row[3], row[4]) }                    
        .view()                                                                 
        .into{ basecallFiles;basecallFiles2 }

Channel                                                                         
        .fromPath( "${params.inFiles}" )                                        
        .splitCsv()                                                             
        .map{ row -> tuple(row[0], row[1], row[2]) }                                    
        .view()                                                                 
        .into{ inputFiles;inputFiles2;inputFiles3;inputFiles4;inputFiles5;inputFiles6 } 

Channel
	.from( 2, 5, 10, 20, 50, 100 )
        .view()
        .set{ depths }


process basecall {
    label 'gpu'
    tag {sample}
    maxForks 1 
    publishDir 'trained', mode: 'COPY'                                      
                                                                            
    input:                                                                  
    set val(sample), val(reads),val(config),val(model) from basecallFiles

    output:
    set val(sample), file('fastqs') into classifier,basecalled
    set val(sample), val(reads), file('fastqs') into indexer
                                                                            
    script:                                                                 
    base=params.base                                                        
    """                                                                     
    guppy_basecaller \
            --device ${params.cudadevice} \
            -i $reads \
            -s fastqs \
            --recursive \
            -q 8000 \
            -c $config \
            -m $model
    
    """

}

process classify {
    tag { sample }

    input:
    set val(sample), file('fastq') from classifier

    output:
    set val(sample), file('fastq'), file('fuge.txt') into binner
    
    script:
    cdb=params.centdb
    """
    cat fastq/*.fastq > in.fastq 
    centrifuge -f -x $cdb \
        --mm -q -U in.fastq \
        -S 'fuge.txt' --min-hitlen 16 -k 1                                      
    """
}

process binReads {
    tag { sample }

    input:
    set val(sample), file('fastq'), file('fuge.txt') from binner    

    output:
    set val(sample),file("bins/${sample}_all_485.fq.gz") into binnedfastq,binnedfastq2


    script:                                                                     
    scriptBase=params.base + '/scripts'                                         
    """             
    python3 ${scriptBase}/binReads.py -s ${sample} \
        -fq fastq/*.fastq \
        -cf fuge.txt \
        -tax 485 \
        -br all                                                                 
                                                                                
    """
}

process map {                                                               
    tag { sample }                                                   
    cpus 3                                                                  
    publishDir 'bams', pattern: '*.bam', overwrite: true                    
                                                                            
    input:
    set val(sample),file("bins/${sample}_all_485.fq.gz") from binnedfastq
                                                                            
    output:
    set val(sample),file("${sample}.sorted.bam"),file("${sample}.sorted.bam.bai") into mapped,mapped2,mapped3 
    script:
    ref=params.ref                             
    """                                                                     
    minimap2 -t 2 -ax map-ont ${ref} bins/${sample}_all_485.fq.gz | samtools view -q 50 -bS - | samtools sort -o ${sample}.sorted.bam
    samtools index ${sample}.sorted.bam                                        
    """                                                                     
}

process subsample {
    tag { sample + sub}
    //publishDir 'bams', overwrite: true                    
    
    input:
    set val(sample),file("${sample}.sorted.bam"),file("${sample}.sorted.bam.bai"),
	val(sub) from mapped3.combine(depths)

    output: 
    set val(sample),val(sub),file("${sample}.depth${sub}.bam"),file("${sample}.depth${sub}.bam.bai") into subsampled, subsampled2,subsampled3,subsampled4,subsampled5,subsampled6

    script:
    scriptBase=params.base + '/scripts'
    """
    python3 ${scriptBase}/subSampleBam.py -b ${sample}.sorted.bam -s $sub
    """
}

process mtrProm {
    tag { sample + sub}
    publishDir 'mtrProm', overwrite: true

    input:
    set val(sample),val(sub),file("${sample}.depth${sub}.bam"),file("${sample}.depth${sub}.bam.bai") from subsampled4

    output:
    set val(sample),val(sub),file("${sample}_${sub}_mtrProm.tab") into mtrProm_ch

    script:
    scriptBase=params.base + '/scripts'
    """
    python3 ${scriptBase}/indel_class.py -b ${sample}.depth${sub}.bam -p 1332809 -c R00000419 -s ${sample} -m ${sub} > ${sample}_${sub}_mtrProm.tab
    """
}

process makeRange {
    tag { sample + ' ' + ref }

    input:
    set val(sample),val(f5s),val(config),val(model) from basecallFiles2

    output:
    set val(sample),file("windows/*.txt") into ranges

    script:
    ref=params.ref 
    """
    mkdir windows
    python ~/soft/nanopolish/scripts/nanopolish_makerange.py $ref > ranges.txt 
    split -l 1 --additional-suffix .txt  ranges.txt windows/range_
    """
}

process medaka {
    errorStrategy 'ignore'
    tag {sample }

    container = '/mnt/nanostore/soft/images/ngonpipe-2019-11-06-293fb1425044.img'

    input:
    set val(sample), val(sub),file('reads.bam'), file('reads.bam.bai') from subsampled5

    output:
    set val(sample), val(sub), file("${sample}_${sub}.vcf") into medakaVCFs,medakaVCFs2

    script:
    ref=params.ref
    """
    medaka consensus --save_features reads.bam medaka_cons.hdf 
    medaka variant $ref medaka_cons.hdf ${sample}_${sub}.vcf
    """
}

process pysamStats {
    tag { sample + ' ' + sub}

    publishDir 'pysams',  mode: 'copy', overwrite: true
    
    input:
    set val(sample),val(sub), file('bam'), file('bam.bai') from subsampled2

    output:
    set val(sample),val(sub),file("${sample}_${sub}_pysamfile.txt") into pysamStats_out, pysamStats_out2,pysamStats_out3,pysamStats_out4,pysamStats_out5,pysamStats_out6

    script:
    ref=params.ref
    """
    pysamstats -t variation_strand bam -f ${ref} > ${sample}_${sub}_pysamfile.txt
    """    

}

// ################# MEDAKA ##########################
process medakaAnalysis {
    tag { sample + ' ' + sub }
    //errorStrategy 'ignore'
    publishDir 'medaka_models', pattern: '*.sav', overwrite: true, mode: 'copy'
    publishDir 'medaka_curves', pattern: '*.pdf', overwrite: true, mode: 'copy'
    publishDir 'medaka_csvs', pattern: '*.csv', overwrite: true, mode: 'copy'


    input:
    set val(sample), val(sub),file("${sample}_${sub}.vcf"),
        val(f5s),val(illumina),
        file('pysamfile.txt') from medakaVCFs.combine(inputFiles3,by:0).combine(pysamStats_out3,by:[0,1])

    output:
    set val(sample),file("*.sav") into medaka_models
    set val(sample),file("*.pdf") into medaka_curves
    set val(sample),file("*.csv") into medaka_csvs
//    set val(sample),val(sub), file('*classifications.csv') into processed
    file("*classifications.csv") into medaka_csvs_train

    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    mkdir vcfs
    mv *.vcf vcfs/
    python3 ${scriptBase}/medaka_vcf_stats.py \
        vcfs/ \
        ${illumina} \
        ${sample}_${sub} \
        11 

    python3 ${scriptBase}/medaka_ROC_AUC.py \
    -snps ${sample}_${sub}_SNPs.csv \
    --sample ${sample} \
    --depth ${sub} \
    --id ${sample}_${sub} \
    -p pysamfile.txt \
    -t ${illumina} \
    -r ${ref}
    """
}

process trainMedakaModel {

    publishDir 'trainedMedakaDetails', mode: 'copy'
    
    input:
    file("*.csv") from medaka_csvs_train.collect()

    output:
    set file('csvs'),file('figs'),file('*_model.sav') into trainedMedakaModel
    file('composite_model.sav') into medakaModel
    set val('medaka'), file('composite_model.sav') into medakaModel2

    script:
    scriptBase=params.base + '/threshold_analysis'
    """
    mkdir csvs figs snps
    mv *.csv snps/
    python3 ${scriptBase}/train_SNP_filterMedaka.py \
	-m ${scriptBase}/all_metadata.csv \
	-b ${scriptBase}/R00000419.fasta.O6.blast.txt
    """
}

// ################## CLAIR ###############################

process clair {
    cpus 4
    //errorStrategy 'ignore'
    tag {sample + ' ' + sub}

    container = '/mnt/nanostore/soft/images/clair_docker_image-2019-12-06-dea8894f6394.img'

    input:
    set val(sample), val(sub),file('reads.bam'), file('reads.bam.bai') from subsampled6

    output:
    set val(sample), val(sub), file("${sample}_${sub}.vcf") into clairVCFs,clairVCFs2

    script:
    ref=params.ref
    CLAIRMODEL=params.CLAIRMODEL
    """
    clair.py callVarBam \
	--chkpnt_fn ${CLAIRMODEL} \
	--ref_fn ${ref} \
	--bam_fn reads.bam \
	--ctgName R00000419 \
	--sampleName ${sample}_${sub} \
	--minCoverage 1 \
	--threads ${task.cpus} \
	--call_fn ${sample}_${sub}.vcf 
    """
}

process clairAnalysis {
    tag { sample + ' ' + sub }
    //errorStrategy 'ignore'
    publishDir 'clair_models', pattern: '*.sav', overwrite: true, mode: 'copy'
    publishDir 'clair_curves', pattern: '*.pdf', overwrite: true, mode: 'copy'
    publishDir 'clair_csvs', pattern: '*.csv', overwrite: true, mode: 'copy'


    input:
    set val(sample), val(sub),file("${sample}_${sub}.vcf"),
        val(f5s),val(illumina),
        file('pysamfile.txt') from clairVCFs.combine(inputFiles5,by:0).combine(pysamStats_out5,by:[0,1])

    output:
    set val(sample),file("*.sav") into clair_models
    set val(sample),file("*.pdf") into clair_curves
    set val(sample),file("*.csv") into clair_csvs
//    set val(sample),val(sub), file('*classifications.csv') into processed
    file("*classifications.csv") into clair_csvs_train

    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    mkdir vcfs
    mv *.vcf vcfs/
    python3 ${scriptBase}/medaka_vcf_stats.py \
        vcfs/ \
        ${illumina} \
        ${sample}_${sub} \
        12

    python3 ${scriptBase}/medaka_ROC_AUC.py \
    -snps ${sample}_${sub}_SNPs.csv \
    --sample ${sample} \
    --depth ${sub} \
    --id ${sample}_${sub} \
    -p pysamfile.txt \
    -t ${illumina} \
    -r ${ref}
    """
}

process trainClairModel {

    publishDir 'trainedClairDetails', mode: 'copy'
    
    input:
    file("*.csv") from clair_csvs_train.collect()

    output:
    set file('csvs'),file('figs'),file('*_model.sav') into trainedClairModel
    file('composite_model.sav') into clairModel
    set val('clair'), file('composite_model.sav') into clairModel2

    script:
    scriptBase=params.base + '/threshold_analysis'
    """
    mkdir csvs figs snps
    
    mv *.csv snps/

    python3 ${scriptBase}/train_SNP_filterMedaka.py \
	-m ${scriptBase}/all_metadata.csv \
	-b ${scriptBase}/R00000419.fasta.O6.blast.txt

    """
}

// ############ NANOPOLISH ################


    container = '/home/nick/soft/ngonpipe/sing_images/ngonpipe-2019-11-06-293fb1425044.img'

    input:
    set val(sample), val(sub),file('reads.bam'), file('reads.bam.bai') from subsampled5

    output:
    set val(sample), val(sub), file('medaka_variant/round_2_final_phased.vcf') into medakaVCFs

    script:
    ref=params.ref
    """
    medaka_variant -f $ref -i reads.bam
    """
}


process pysamStats {
    tag { sample + ' ' + sub}

    //publishDir 'pysams',  mode: 'copy', overwrite: true
    
    input:
    set val(sample),val(sub), file('bam'), file('bam.bai') from subsampled2

    output:
    set val(sample),val(sub),file("${sample}_pysamfile.txt") into pysamStats_out, pysamStats_out2,pysamStats_out3

    script:
    ref=params.ref
    """
    pysamstats -t variation_strand bam -f ${ref} > ${sample}_pysamfile.txt
    """    

}


process medakaAnalysis {
    tag { sample + ' ' + sub }
    maxForks 4
    errorStrategy 'ignore'
    publishDir 'medaka_models', pattern: '*.sav', overwrite: true, mode: 'copy'
    publishDir 'medaka_curves', pattern: '*.pdf', overwrite: true, mode: 'copy'
    publishDir 'medaka_csvs', pattern: '*.csv', overwrite: true, mode: 'copy'


    input:
    set val(sample), val(sub),file('medaka.vcf'),
        val(f5s),val(illumina),
        file('pysamfile.txt') from medakaVCFs.combine(inputFiles3,by:0).combine(pysamStats_out3,by:[0,1])

    output:
    set val(sample),file("*.sav") into medaka_models
    set val(sample),file("*.pdf") into medaka_curves
    set val(sample),file("*.csv") into medaka_csvs
//    set val(sample),val(sub), file('*classifications.csv') into processed
//    file("*classifications.csv") into csvs_train

    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    echo $sample
    mkdir vcfs
    mv *.vcf vcfs/
    python3 ${scriptBase}/medaka_vcf_stats.py \
        vcfs/ \
        ${illumina} \
        ${sample}_${sub} \
        10

    python3 ${scriptBase}/medaka_ROC_AUC.py \
    -snps ${sample}_${sub}_SNPs.csv \
    --sample ${sample} \
    --id ${sample}_${sub} \
    -p pysamfile.txt \
    -t ${illumina} \
    -r ${ref}

    """
}
    




if (params.porechop == 'on'){
process nanoplish {
    tag { sample + ' ' + ref }

    input: 
    set val(sample), val(reads), file('fastq'),file("bins/${sample}_all_485.fq.gz") from indexer.combine(binnedfastq2,by:0)

    output:
    set val(sample), val(reads), file('bins') into indexed,indexed2
    
    script:
    """
    nanopolish index  -d $reads bins/${sample}_all_485.fq.gz
    """
}

ranges
    .transpose()
    .map { sample, file -> tuple(sample, file.baseName, file) }
    //.view()
    .into{ rangeJobs }

process polishRange {
    label 'nanopolish'
    maxForks 19
    tag { sample + ' ' + range + ' ' + sub}
    errorStrategy 'ignore'
    publishDir 'VCFs_map_range', pattern: '*.vcf', overwrite: true, mode: 'copy'

    cpus 1 
    input:
    set val(sample),val(range),file('range.txt'),
        val(sub),file('bam'), file('bam.bai'), 
        val(f5s),file('bins') from rangeJobs.combine(subsampled,by:0).combine(indexed, by:0)

    output:
    set val(sample),val(sub),val(range),file("${sample}_${range}.vcf") into polished
    set val(sample),val(sub),file("${sample}_${range}.vcf") into polishedVCFs, polishedVCFs2

    script:
    ref=params.ref
    """
    cat range.txt
    region=\$(cat range.txt)
    echo \$region
    nanopolish variants -o ${sample}_${range}.vcf \
        -r bins/${sample}_all_485.fq.gz \
        -b bam \
	-t ${task.cpus} \
        -w \$region \
        --methylation-aware dcm,dam \
        --threads 1 \
        --ploidy 1 \
        -d 1 \
        -g $ref \
	-x 10000
    """
}

//
//process processPysam {
//    tag { sample }
//    maxForks 1 
//
//    publishDir 'pysam_models', pattern: '*.sav', overwrite: true, mode: 'copy'        
//    publishDir 'pysam_curves', pattern: '*.pdf', overwrite: true, mode: 'copy'        
//    publishDir 'pysam_csvs', pattern: '*.csv', overwrite: true, mode: 'copy'    
//
//    input:
//    set val(sample), val(bam), val(ref), val(illumina), val(f5s), file('pysamfile.txt') from inputFiles5.combine(pysamStats_out2,by:0)
//
//    output:
//    set file('*.pdf'),file('*.csv'),file('*.sav')
//
//    script:
//    scriptBase='/home/nick/soft/ngonpipe/threshold_analysis'
//    """
//    python3 ${scriptBase}/pysamML.py  \
//    --sample ${sample} \
//    -p pysamfile.txt \
//    -t ${illumina} \
//    -r ${ref}
//    """
//}
//

if (params.porechop == 'on'){
process processVCF {
    tag { sample + ' ' + sub }
    maxForks 4
    errorStrategy 'ignore'
    publishDir 'models', pattern: '*.sav', overwrite: true, mode: 'copy'
    publishDir 'curves', pattern: '*.pdf', overwrite: true, mode: 'copy'
    publishDir 'csvs', pattern: '*.csv', overwrite: true, mode: 'copy'


    input:
    set val(sample), val(sub),file("*"),
        val(f5s),val(illumina),
        file('pysamfile.txt') from polishedVCFs.groupTuple(by:[0,1]).combine(inputFiles,by:0).combine(pysamStats_out,by:[0,1])

    output:
    set val(sample),file("*.sav") into models
    set val(sample),file("*.pdf") into curves
    set val(sample),file("*.csv") into csvs
    set val(sample),val(sub), file('*classifications.csv') into processed
    file("*classifications.csv") into csvs_train

    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    echo $sample
    mkdir vcfs

    mv *.vcf vcfs/

    python3 ${scriptBase}/vcf_stats.py \
        vcfs/ \
        ${illumina} \
        ${sample}_${sub}

    python3 ${scriptBase}/ROC_AUC.py \
    -snps ${sample}_${sub}_SNPs.csv \
    --sample ${sample} \
    --depth ${sub} \
    --id ${sample}_${sub} \
    -p pysamfile.txt \
    -t ${illumina} \
    -r ${ref}
    """
}

process trainModel {

    publishDir 'trainedDetails', mode: 'copy'
    
    input:
    file("*.csv") from csvs_train.collect()

    output:
    set file('csvs'),file('figs'),file('*_model.sav') into trainedModel
    set file('composite_model.sav') into model, model2
    set val('nanopolish'), file('composite_model.sav') into nanomodel

    script:
    scriptBase=params.base + '/threshold_analysis'
    """
    mkdir csvs figs snps
    mv *.csv snps/
    python3 ${scriptBase}/train_SNP_filter.py \
	-m ${scriptBase}/all_metadata.csv \
	-b ${scriptBase}/R00000419.fasta.O6.blast.txt
    """
}
}


// ###################### Assembly ###########################


process binbamReads {
    tag { sample + ' ' + sub }
    maxForks 10
    
    input:
    set val(sample),val(sub), file('bam'), file('bam.bai'),file('fastqs') from subsampled3.combine(basecalled, by:0)

    output:
    set val(sample), val(sub), file("${sample}_${sub}.fastq.gz") into binnedbamfastq,binnedbamfastq2

    script:
    scriptBase=params.base + '/scripts'
    """
    python3 ${scriptBase}/bamtofastq.py --bam bam \
	--fastq fastqs/*.fastq \
        --outfastq ${sample}_${sub}.fastq
    gzip ${sample}_${sub}.fastq
    """

}


params.genomeSize='2.4m'
params.assembler = 'ra'
process assemble {
        errorStrategy 'ignore'
        maxForks 10       
        tag { run  + ' ' + sub }                                                             
        cpus 2                                                                  
        publishDir 'assembledContigs', overwrite: true, mode: 'copy'            

        container '/mnt/nanostore/soft/images/ngonpipe-2019-08-06-1678b6d23ef2.img'
                                                                                
        input:                                                                  
        set val(run), val(sub), file('fastq.fq.gz') from binnedbamfastq
                                                                                
        output:                                                                 
        set val(run), val(sub) ,file("${run}_${sub}.contigs.fa")  into assembled,assembled2      
        set val(run), val(sub), file("${run}_${sub}.contigs.fa"), file('fastq.fq.gz')   into assembled3      
                                                                                
        script:                                                                 
        genomeSize=params.genomeSize                                            
        if(params.assembler == 'wtbdg2')                                        
            """                                                                 
            wtdbg2 -t ${task.cpus} -i fastq.fq.gz -fo contigs -L 3000           
            wtpoa-cns -t ${task.cpus} -i contigs.ctg.lay.gz -fo ${run}_${sub}.contigs.fa
            """                                                                 
        else if(params.assembler == 'flye')                                     
            """                                                                 
            flye --nano-raw fastq.fq.gz -g $genomeSize -t ${task.cpus} -o contigs
            cp contigs/00-assembly/*.fasta ${run}.contigs.fa                    
            """                                                                 
        else if(params.assembler == 'canu')                                     
            """                                                                 
            canu -p ${run} -d canu_contigs \
                genomeSize=$genomeSize \
                -nanopore-raw fastq.fq.gz \
                stopOnLowCoverage=0 useGrid=false \
                minThreads=${task.cpus} \
                maxThreads=${task.cpus}  
            cp canu_contigs/${run}.contigs.fasta ${run}.contigs.fa              
            """                                                                 
        else if(params.assembler == 'ra')                                       
            """                                                                 
            ra -x ont -t ${task.cpus} fastq.fq.gz > ${run}_${sub}.contigs.fa           
            """                                                                 
    }

params.blastdb=params.base + '/genes/genes.fasta'                               
params.ngstarMeta=params.base + '/genes/'                                       

process blastn {                                                            
    tag { sample  + ' ' + sub }                                                          
                                                                            
//    publishDir 'blasts', mode: 'copy', overwrite: true                      
                                                                            
    input:                                                                  
    set val(sample), val(sub), file('fasta') from assembled                           
                                                                            
    output:                                                                 
    set val(sample), val(sub), file("*.O6.blast.txt") into blastOut                   
                                                                            
    script:                                                                 
    db=params.blastdb                                                       
    """                                                                     
    blastn -query fasta -db $db -outfmt 6 -max_target_seqs 10000 > ${sample}_${sub}.O6.blast.txt
    """                                                                     
}                                                                           
                                                                            
                                                                            
process parseBlast6 {                                                       
    errorStrategy 'ignore'                                                  
    tag { sample + ' ' + sub }                                                          
                                                                            
//    publishDir 'ngstarFullAssembly', mode: 'copy', overwrite: true, pattern: '*.csv'
                                                                            
    input:                                                                  
    set val(sample), val(sub), file('O6.blast.txt') from blastOut                     
                                                                            
    output:                                                                 
    set val(sample), val(sub), file("${sample}.csv"),file('windows') into assembly_pb6_out
    set val(sample), val(sub), file("windows/*.txt") into assembly_gene_windows        
                                                                            
    script:                                                                 
    meta=params.ngstarMeta                                                  
    scriptBase=params.base + '/scripts'                                     
    """                                                                     
    python3 ${scriptBase}/pb6.py -b O6.blast.txt -m $meta -o ${sample}.csv  
    mkdir windows                                                           
    awk -F, '{print \$2":"\$8"-"\$9  > "windows/"\$15"_"\$8".txt" }' ${sample}.csv
    rm windows/gene_x*                                                      
    """                                                                     
}

process remap {                                                             
    tag { run + ' ' + sub }                                                             
    cpus 3                                                                  
                                                                            
    input:                                                                  
    set val(run), val(sub), file("${run}.ctg.lay.fa"),file('fastq.fq.gz') from assembled3
                                                                            
    output:                                                                 
    set val(run), val(sub), file("${run}.ctg.lay.fa"),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai") into remapped
                                                                            
    script:                                                                 
    """                                                                     
    minimap2 -t 2 -ax map-ont ${run}.ctg.lay.fa fastq.fq.gz | samtools view -F4 -bS - | samtools sort -o ${run}.sorted.bam 
    samtools index ${run}.sorted.bam                                        
    """                                                                     
}

process cutRemapBam {                                                       
    tag {run + ' ' + sub}                                                               
//    publishDir 'cutRemapDepths', pattern: '*_cutDepths', overwrite: true, mode: 'copy'
                                                                            
    input:                                                                  
    set val(run), val(sub), file('ref.fa'),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai"),file("${run}.csv"),file('windows') from remapped.combine(assembly_pb6_out, by:[0,1])
                                                                            
    output:                                                                 
    set val(run), val(sub), file('ref.fa'),file("${run}.cut.bam"),file("${run}.cut.bam.bai"),file("${run}_cut.fastq") into RemapCuts1,RemapCuts2
    set val(run), val(sub), file("${run}_cutDepths") into cutRemapDepths               
    set val(run), val(sub), file("fasta_cuts/*") into WGAgenes                         
    set val(run), val(sub), file("fastq_cuts/*") into binnedWGAGenes                 
                                                                            
    script:                                                                 
    """                              
    samtools faidx ref.fa                                       
    mkdir cuts fastq_cuts ${run}_cutDepths fasta_cuts                       
    regions=windows/*.txt                                                   
    for region in \${regions}                                               
    do                                                                      
        reg=\$(cat \${region})                                              
        fileName=\$(basename \$region .txt)                                 
        geneName=(\${fileName//_/ })                                        
        echo \$reg \$geneName
        samtools view -b ${run}.sorted.bam \$reg -o cuts/${run}.\${geneName}.bam
        samtools index cuts/${run}.\${geneName}.bam                         
        samtools depth -r \$reg cuts/${run}.\${geneName}.bam > ${run}_cutDepths/${run}.\${geneName}_depth.tsv
        bamToFastq -i cuts/${run}.\${geneName}.bam -fq fastq_cuts/\${geneName}.fastq
        cat fastq_cuts/\${geneName}.fastq >> ${run}_cut.fastq
        /mnt/nanostore/soft/old_versions/samtools_0.1.19 faidx ref.fa \$reg > fasta_cuts/\${geneName}.fasta
    done                                                                    
    samtools merge ${run}.cut.bam cuts/*bam                                 
    samtools index ${run}.cut.bam                                           
    """                                                                     
}

WGAgenes                                                                    
    .transpose()                                                            
    .map { run, sub, file -> tuple(run, sub, file.baseName, 'WGA_genes', file) }      
    .view()                                                                 
    .into{ WGAgenebins }                                                    
                                                                            
binnedWGAGenes                                                              
    .transpose()                                                            
    .map { run, sub, file -> tuple(run, sub, file.baseName, 'WGA_genes',file) }      
    .filter{ it[2] in [/penA/,/POR/,/TBPB/] }                               
    .view()                                                                 
    .into{ WGA_geneFQBins }

//#################### gene bining and local assemblies ####################### 

process mapGenes {                                                              
    tag { run + ' ' + sub }                                                                 
    cpus 3                                                                      
//    publishDir 'genebams', pattern: '*.bam', overwrite: true                    
                                                                                
    input:                                                                      
    set val(run), val(sub), file('fastq.fq.gz') from binnedbamfastq2
                                                                                
    output:                                                                     
    set val(run),val(sub), file("${run}.sorted.bam"),file('fastq.fq.gz') into geneBams
                                                                                
    script:                                                                    
    ref=params.blastdb
    """                                                                         
    minimap2 -t 2 -ax map-ont $ref fastq.fq.gz | samtools view -F4 -bS - | samtools sort -o ${run}.sorted.bam       
    """                                                                         
}                                                                               
                                                                                
process binGenes {                                                              
//    errorStrategy 'ignore'                                                    
    tag { run + ' ' + sub }                                                                 
                                                                                
    input:                                                                      
    set val(run),val(sub), file("${run}.sorted.bam"),file('fastq.fq.gz') from geneBams
                                                                                
    output:                                                                     
    set val(run), val(sub),file("*.fastq") into binnedGenes,binnedGenes2,binnedGenes3     
                                                                                
    script:                                                                     
    meta=params.ngstarMeta                                                      
    scriptBase=params.base + '/scripts'                                         
    """                                                                         
    samtools index ${run}.sorted.bam                                            
    python3 ${scriptBase}/binGenes.py -f fastq.fq.gz -b ${run}.sorted.bam -m ${meta} -s 400 
    """                                                                         
}                                                                               

binnedGenes                                                                     
    .transpose()                                                                
    .map { run, sub, file -> tuple(run, sub, file.baseName, 'mapBinGenes', file) }        
    .filter{ it[2] in [/penA/,/POR/,/TBPB/] }                                   
    .view()                                                                     
    .into{ geneBins;geneBins2 }


params.geneAssembler = 'wtbdg2'
process assembleGenes {
        errorStrategy 'ignore'
        maxForks 10       
        tag { run  + ' ' + sub + ' ' + gene } 
        cpus 2                                                                  
//        publishDir 'assembledGeneContigs', overwrite: true, mode: 'copy'            

        container '/mnt/nanostore/soft/images/ngonpipe-2019-08-06-1678b6d23ef2.img'
                                                                                
        input:                                                                  
        set val(run), val(sub), val(gene), val(oper), file('fastq.fq') from geneBins
                                                                                
        output:                                                                 
        set val(run), val(sub), val(gene), val(oper), file("${run}_${sub}.contigs.fa")  into assembledGenes
        set val(run), val(sub), val(gene), val(oper), file("${run}_${sub}.contigs.fa"), file('fastq.fq') into assembledGenes3      
                                                                                
        script:                                                                 
        genomeSize=params.genomeSize                                            
        if(params.geneAssembler == 'wtbdg2')                                        
            """                                                                 
            wtdbg2 -t ${task.cpus} -i fastq.fq -fo contigs -L 3000           
            wtpoa-cns -t ${task.cpus} -i contigs.ctg.lay.gz -fo ${run}_${sub}.contigs.fa
            """                                                                 
        else if(params.geneAssembler == 'flye')                                     
            """                                                                 
            flye --nano-raw fastq.fq -g $genomeSize -t ${task.cpus} -o contigs
            cp contigs/00-assembly/*.fasta ${run}.contigs.fa                    
            """                                                                 
        else if(params.geneAssembler == 'canu')                                     
            """                                                                 
            canu -p ${run} -d canu_contigs \
                genomeSize=$genomeSize \
                -nanopore-raw fastq.fq \
                stopOnLowCoverage=0 useGrid=false \
                minThreads=${task.cpus} \
                maxThreads=${task.cpus}  
            cp canu_contigs/${run}.contigs.fasta ${run}.contigs.fa              
            """                                                                 
        else if(params.geneAssembler == 'ra')                                       
            """                                                                 
            ra -x ont -t ${task.cpus} fastq.fq > ${run}_${sub}.contigs.fa           
            """                                                                 
    }


//########################### analysis ############################################

analysisBins=assembledGenes.mix(WGAgenebins)

process geneblastn {                                                            
    tag { run + ' ' + sub + ' ' + gene + ' ' + oper}                                        
//    publishDir 'blasts', mode: 'copy', overwrite: true                          
                                                                                
    input:                                                                      
    set val(run), val(sub), val(gene),val(oper),file("${run}_${gene}.fa") from analysisBins
                                                                                
    output:                                                                     
    set val(run), val(sub), val(gene),file("*.O6.blast.txt"),val(oper) into blastOutPolish 
                                                                                
    script:                                                                     
    db=params.blastdb                                                           
    """                                                                         
    blastn -query ${run}_${gene}.fa -db $db -outfmt 6 -max_target_seqs 10000 > ${run}_${gene}_${oper}.O6.blast.txt
    """                                                                         
}                                                                               
                                                                                
process geneparseBlast6 {                                                       
    tag { sample + ' ' + sub + ' ' + gene + ' ' + oper}                                     
                                                                                
//    publishDir 'ngstar', mode: 'copy', overwrite: true                          
                                                                                
    input:                                                                      
    set val(sample), val(sub), val(gene), file('O6.blast.txt'),val(oper) from blastOutPolish
                                                                                
    output:                                                                     
    set val(sample), val(sub), val(gene), file("${sample}_${gene}_${oper}.csv"),val(oper) into pb6_out_polish
    //set val(sample),file('windows/*.txt') into pb6_out                        
                                                                                
    script:                                                                     
    meta=params.ngstarMeta                                                      
    scriptBase=params.base + '/scripts'                                         
    """                                                                         
    python3 ${scriptBase}/pb6.py -b O6.blast.txt -m $meta -o ${sample}_${gene}_${oper}.csv
    """                                                                         
}


process chooseGeneRef {                                                         
    tag { sample + ' ' + sub + ' ' + gene + ' ' + oper}                                     
    errorStrategy 'ignore'                                                      
                                                                                
    input:                                                                      
    set val(sample),val(sub),val(gene), file("${sample}_${gene}_${oper}.csv"),val(oper) from pb6_out_polish.filter{var_bool || it[2] in [/penA/,/POR/,/TBPB/]}
                                                                                
    output:                                                                     
    set val(sample), val(sub), val(gene),val(oper),file("${gene}.fa") into geneRefs        
                                                                                
    script:                                                                     
    genes=params.base + '/genes/genes.fasta'
    """                                                                         
    awk -F, 'FNR==2{print \$3}' ${sample}_${gene}_${oper}.csv > ${gene}.txt     
    samtools faidx $genes -r ${gene}.txt > ${gene}.fa                              
    """                                                                         
}

fqGeneBinMix=geneBins2.mix(WGA_geneFQBins)

process remapGene {                                                             
    tag { run + ' ' + sub + ' ' + gene + ' ' + oper}                                        
                                                                                
    input:                                                                      
    set val(run),val(sub),val(gene),val(oper),file("${gene}.fa"),file("genes.fastq") from geneRefs.combine(fqGeneBinMix, by:[0,1,2,3])
                                                                                
    output:                                                                     
    set val(run),val(sub),val(oper),val(gene),file("${gene}.fa"),file("genes.fastq"),file("${run}_${gene}.sorted.bam"),file("${run}_${gene}.sorted.bam.bai") into remappedGene,remappedGene2
                                                                                
    script:                                                                     
    """                                                                         
    minimap2 -t 2 -ax map-ont ${gene}.fa genes.fastq | samtools view -F4 -bS - | samtools sort -o ${run}_${gene}.sorted.bam

    samtools index ${run}_${gene}.sorted.bam                                    
    """                                                                         
}

process pysamstatsGene {
    tag { run + ' ' + sub + ' ' + gene + ' ' + oper}

    input:
    set val(run),val(sub),val(oper),val(gene),file("${gene}.fa"),file("genes.fastq"),file("${run}_${gene}.sorted.bam"),file("${run}_${gene}.sorted.bam.bai") from remappedGene2

    output:
    set val(run),val(sub),val(oper),val(gene),file("${run}_${sub}_pysamfile.txt") into pysamstatsGene_ch

    script: 
    """
    pysamstats -t variation_strand ${run}_${gene}.sorted.bam -f ${gene}.fa > ${run}_${sub}_pysamfile.txt 
    """

}


if (params.porechop == 'on'){
process polishRemapGene {
    label 'nanopolish' 
    maxForks 19
    tag { run + ' ' +  sub + ' ' + gene + ' ' + oper }                                       
//    publishDir 'VCF_RemapGene', pattern: '*.vcf', mode: 'copy'                  
    maxForks 10
    cpus 4                                                                      
                                                                                
    input:                                                                      
    set val(run),val(sub),val(oper),val(gene),file("${gene}.fa"),file("genes.fastq"),file("${run}_${gene}.sorted.bam"),file("${run}_${gene}.sorted.bam.bai"),val(reads), file('bins') from remappedGene.combine(indexed2, by:0)
                                                                                
    output:                                                                     
    set val(run),val(sub),val(oper),val(gene),file("${gene}.fa"),file("${run}_${gene}.vcf") into polishedRemapGene
                                                                                
    script:                                                                     
    """                                                                         
    nanopolish variants -o ${run}_${gene}.vcf \
        -r bins/*.fq.gz \
        -b ${run}_${gene}.sorted.bam \
        -x 10000 \
        --threads ${task.cpus} \
        --ploidy 1 \
        -d 1 \
        -g ${gene}.fa                                                           
                                                                                
    """                                                                         
}

process makePolishRemapGeneConsensus {                                          
    tag { run + ' ' + sub + ' ' + gene + ' ' + operRG }                                     
//    publishDir 'polishGenes', pattern: "*_${operRG}.fa", overwrite: true, mode: 'copy'
    publishDir 'filteredVCFs', pattern: "*_filtered.vcf", overwrite: true, mode: 'copy'

    container '/mnt/nanostore/soft/images/ngonpipe-2019-08-02-627c13800477.img'
                                                                                
    input:                                                                      
    set val(run),val(sub), val(oper),val(gene),file("${run}_${gene}.fa"),file("${run}_${gene}.vcf"),file('pysamstats.txt'), file('composite_model.sav') from polishedRemapGene.combine(pysamstatsGene_ch, by:[0,1,2,3]).combine(model)
                                                                                
    output:                                                                     
    set val(run),val(sub),val(oper),val(gene),file("${run}_${gene}_${operRG}.fa") into RGpcons,RGpcons2
    set val(run),val(sub),val(oper),file("${run}_${gene}_filtered.vcf") into filteredVCFs          
                                                                                
    script:                                                                     
    operRG=oper + 'RemapGene'                                                   
    filt=params.vcf_qual_filter                                                 
    scriptBase=params.base + '/scripts'                                         
    modelBase=params.base + '/models'                                           
    """                                                                         
    python3 ${scriptBase}/filterVCF_model.py -i ${run}_${gene}.vcf -o ${run}_${gene}_filtered.vcf \
        -m composite_model.sav  -p pysamstats.txt 
    nanopolish vcf2fasta --skip-checks -g ${run}_${gene}.fa ${run}_${gene}_filtered.vcf > ${run}_${gene}_${operRG}_all.fa
    python3 ${scriptBase}/maskDepth.py -f ${run}_${gene}_${operRG}_all.fa \
		-p pysamstats.txt \
		-d 1 \
		-o ${run}_${gene}_${operRG}.fa 
    """                                                                         
                                                                                
}

process geneblastnRemapGene {                                                   
    tag { run + ' ' + sub + ' ' + gene + ' ' + oper}                                        
//    publishDir 'blasts', mode: 'copy', overwrite: true                          
                                                                                
    input:                                                                      
    set val(run),val(sub),val(oper),val(gene),file("${run}_${gene}.fa") from RGpcons     
                                                                                
    output:                                                                     
    set val(run),val(sub),val(oper),val(gene),file("*.O6.blast.txt") into blastOutRGpcon 
                                                                                
    script:                                                                     
    db=params.blastdb                                                           
    """                                                                         
    blastn -query ${run}_${gene}.fa -db $db -outfmt 6 -max_target_seqs 10000 > ${run}_${gene}_${oper}.O6.blast.txt
                                                                                
    """                                                                         
}                                                                               
                                                                                
//process geneparseBlast6RemapGene {                                              
//    tag { sample + ' ' + gene + ' ' + oper}                                     
//                                                                                
////    publishDir 'ngstar', mode: 'copy', overwrite: true                          
//                                                                                
//    input:                                                                      
//    set val(sample),val(sub),val(gene), file('O6.blast.txt'),val(oper) from blastOutRGpcon
//                                                                                
//    output:                                                                     
//    set val(sample),val(sub),val(gene), file("${sample}_${gene}_${oper}.csv"),val(oper) into pb6_out_RemapGene
//    set val(sample),val(sub),val(oper), val(gene), file("${sample}_${gene}_${oper}.csv") into mast_POR
//    //.filter{var_bool || it[2] in [/POR/]}                                     
//    set val(sample),val(sub),val(oper),val(gene),file("${sample}_${gene}_${oper}.csv") into mast_TBPB//.filter{var_bool || it[2] in [/TBPB/] }
//                                                                                
//    script:                                                                     
//    meta=params.ngstarMeta                                                      
//    scriptBase=params.base + '/scripts'                                         
//    """                                                                         
//    python3 ${scriptBase}/pb6.py -b O6.blast.txt -m $meta -o ${sample}_${gene}_${oper}.csv
//    """                                                                         
//}

process getPenA {                                                               
    tag { run + ' ' + sub + ' ' + gene + ' ' + oper}                                        
    publishDir 'penA', mode: 'copy', overwrite: true                           
    errorStrategy 'ignore' 

    container '/mnt/nanostore/soft/images/ngonpipe-2019-08-23-adeb3d9e8271.img'
                                                                                
    input:                                                                      
    set val(run),val(sub),val(oper),val(gene),file("${run}_${gene}.fa") from RGpcons2.filter{var_bool || it[3] in [/penA/]}
                                                                                
    output:                                                                     
    set val(run),val(sub),val(gene),file('*.csv') into penAOut                           
                                                                                
    script:                                                                     
    geneBase=params.base + '/genes'                                             
    scriptBase=params.base + '/scripts'                                         
    """ 
    sixpack -mstart -sequence ${run}_${gene}.fa -outfile penA.sixpack -outseq ${run}_${gene}.prots.fa
    blastp -query ${run}_${gene}.prots.fa -db ${geneBase}/penA_alleles_prot.fasta -evalue 0.001 -outfmt 5 > bp5.txt
    python3 ${scriptBase}/getPenA.py -b bp5.txt -s ${run} -d ${sub} -op ${oper} -m ${geneBase}/penA_alleles_metadata.xlsx                                                                        
    """                                                                         
} 
}

process mergeMedakaVCFs {
    tag { sample + ' ' + sub }

    publishDir "mergedmedakaVCFs"

    input:
    set val(sample), val(sub), file("*"),
        val(f5s),val(illumina),
        file('pysamfile.txt') from medakaVCFs2.groupTuple(by:[0,1]).combine(inputFiles4,by:0).combine(pysamStats_out4,by:[0,1])

    output:
    set val(sample), val(sub), file("${sample}_${sub}_vcfs") into medakaMergedVCFs
    file("*_vcfs") into medakaMergedVCFs2
    set val('medaka'), val(sample), val(sub), file("${sample}_${sub}_vcfs") into medakaMergedVCFs3

    script:
    """
    mkdir "${sample}_${sub}_vcfs"
    cp *.vcf "${sample}_${sub}_vcfs"/
    cp pysamfile.txt "${sample}_${sub}_vcfs"/
    """
}

process mergeClairVCFs {
    tag { sample + ' ' + sub }

    publishDir "mergedClairVCFs"

    input:
    set val(sample), val(sub), file("*"),
        val(f5s),val(illumina),
        file('pysamfile.txt') from clairVCFs2.groupTuple(by:[0,1]).combine(inputFiles6,by:0).combine(pysamStats_out6,by:[0,1])

    output:
    set val(sample), val(sub), file("${sample}_${sub}_vcfs") into clairMergedVCFs
    file("*_vcfs") into clairMergedVCFs2
    set val('clair'), val(sample), val(sub), file("${sample}_${sub}_vcfs") into clairMergedVCFs3

    script:
    """
    mkdir "${sample}_${sub}_vcfs"
    cp *.vcf "${sample}_${sub}_vcfs"/
    cp pysamfile.txt "${sample}_${sub}_vcfs"/
    """
}

process mergeVCFs {
    tag { sample + ' ' + sub }

    publishDir "mergedVCFs"

    input:
    set val(sample), val(sub), file("*"),
        val(f5s),val(illumina),
        file('pysamfile.txt') from polishedVCFs2.groupTuple(by:[0,1]).combine(inputFiles2,by:0).combine(pysamStats_out2,by:[0,1])

    output:
    set val(sample), val(sub), file("${sample}_${sub}_vcfs") into mergedVCFs
    file("*_vcfs") into mergedVCFs2
    set val('nanopolish'), val(sample), val(sub), file("${sample}_${sub}_vcfs") into mergedVCFs3

    script:
    """
    mkdir "${sample}_${sub}_vcfs"
    cp *.vcf "${sample}_${sub}_vcfs"/
    cp pysamfile.txt "${sample}_${sub}_vcfs"/
    """
}


process checkGenes {
//    publishDir 'checkGenes', mode: 'copy'

    container '/mnt/nanostore/soft/images/ngonpipe-2019-11-19-b7ce3b42fc17.img'

    input:
    file("*") from mergedVCFs2.collect()
    file('composite_model.sav') from model2

    output:
    file('filtered_vcfs') into checkedGenes


    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    mkdir VCFs/
    mv *_vcfs VCFs/
    python3 ${scriptBase}/check_genes.py -i VCFs/ \
	-o filtered_vcfs \
	-m composite_model.sav \
	-r ${ref}

    """
}


process checkGenesPlot {
    publishDir 'geneConcordances', mode: 'copy'

    container '/mnt/nanostore/soft/images/ngonpipe-2019-11-19-b7ce3b42fc17.img'

    input:
    file('filtered_vcfs') from checkedGenes

    output:
    set file('*.csv'),file('*.xlsx') into geneConcordances

    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    cp ${scriptBase}/gene_check/* ./
    python3 ${scriptBase}/check_genes_plot.py

    """

}

process checkmedakaGenes {
//    publishDir 'checkGenes', mode: 'copy'

    container '/mnt/nanostore/soft/images/ngonpipe-2019-11-19-b7ce3b42fc17.img'

    input:
    file("*") from medakaMergedVCFs2.collect()
    file('composite_model.sav') from medakaModel 

    output:
    file('filtered_vcfs') into medakacheckedGenes


    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    mkdir VCFs/
    mv *_vcfs VCFs/
    python3 ${scriptBase}/checkMedaka_genes.py -i VCFs/ \
	-o filtered_vcfs \
	-m composite_model.sav \
	-r ${ref}

    """
}


process checkmedakaGenesPlot {
    publishDir 'medakageneConcordances', mode: 'copy'

    container '/mnt/nanostore/soft/images/ngonpipe-2019-11-19-b7ce3b42fc17.img'

    input:
    file('filtered_vcfs') from medakacheckedGenes

    output:
    set file('*.csv'),file('*.xlsx') into medakageneConcordances

    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    cp ${scriptBase}/gene_check/* ./
    python3 ${scriptBase}/check_genes_plot.py

    """

}

process checkClairGenes {
//    publishDir 'checkGenes', mode: 'copy'

    container '/mnt/nanostore/soft/images/ngonpipe-2019-11-19-b7ce3b42fc17.img'

    input:
    file("*") from clairMergedVCFs2.collect()
    file('composite_model.sav') from clairModel 

    output:
    file('filtered_vcfs') into claircheckedGenes


    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    mkdir VCFs/
    mv *_vcfs VCFs/
    python3 ${scriptBase}/checkMedaka_genes.py -i VCFs/ \
	-o filtered_vcfs \
	-m composite_model.sav \
	-r ${ref}

    """
}


process checkClairGenesPlot {
    publishDir 'clairGeneConcordances', mode: 'copy'

    container '/mnt/nanostore/soft/images/ngonpipe-2019-11-19-b7ce3b42fc17.img'

    input:
    file('filtered_vcfs') from claircheckedGenes

    output:
    set file('*.csv'),file('*.xlsx') into clairgeneConcordances

    script:
    scriptBase=params.base + '/threshold_analysis'
    ref=params.ref
    """
    cp ${scriptBase}/gene_check/* ./
    python3 ${scriptBase}/check_genes_plot.py

    """

}
consVCFS=medakaMergedVCFs3.mix(mergedVCFs3,clairMergedVCFs3)
models=medakaModel2.mix(nanomodel,clairModel2)
Channel
        .from( 0, 95 )
        .view()
        .set{ probFilts }

process makeConsensus {
    tag { variantCaller + ' ' + sample + ' ' + sub }

    container '/mnt/nanostore/soft/images/ngonpipe-2019-11-19-b7ce3b42fc17.img'

    publishDir 'consensusSeqs', mode:'copy'


    input:
    set val(variantCaller), val(sample), val(sub), file('vcfs'), file('model.sav'), val(probFilt) from consVCFS.combine(models, by:0).combine(probFilts)

    output:
    set val(variantCaller), val(sample), val(sub), val(probFilt), file("${sample}_${sub}_${variantCaller}_${probFilt}_masked.fa") into conseqs

    script:
    scriptBase=params.base + '/bin'
    ref=params.ref
    """
    python3 ${scriptBase}/filterVCF_model.py -i vcfs/*.vcf -o filtered.vcf \
        -m model.sav  -p vcfs/pysam* -v ${variantCaller} -f ${probFilt}

    nanopolish vcf2fasta --skip-checks -g ${ref} filtered.vcf > ${sample}_${sub}_${variantCaller}.fa

    python3 ${scriptBase}/maskDepth.py -f ${sample}_${sub}_${variantCaller}.fa \
                -p vcfs/pysam* \
                -d 4 \
                -o ${sample}_${sub}_${variantCaller}_${probFilt}_masked.fa \
		-mw 0.8
    """
}

//process recall_genes {
//	tag { sample + ' ' + sub }
//	maxForks 4
//
//	publishDir 'csvs', pattern: '*.csv', overwrite: true, mode: 'copy'
//
//	input:
//	set val(sample), val(sub), file('classifications.csv') from processed
//
//	output:
//	file('*recall_genes.csv') into recall_genes_ch
//
//	script:
//	scriptBase=params.base + '/threshold_analysis'
//	"""
//	python3 ${scriptBase}/recall_genes.py \
//		-m ${scriptBase}/all_metadata.csv  \
//		-b ${scriptBase}/R00000419.fasta.O6.blast.txt \
//		-s classifications.csv \
//		-o ${sample}_${sub}_recall_genes.csv \
//		-i ${sample} \
//		-d ${sub}
//
//	"""
//
//}
