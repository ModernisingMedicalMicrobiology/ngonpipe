//input sample_name, bam_file, reference, illumina, fast5s,
Channel                                                                         
        .fromPath( "${params.inFiles}" )                                        
        .splitCsv()                                                             
        .map{ row -> tuple(row[0], row[1], row[2], row[3],row[4]) }                    
        .view()                                                                 
        .into{ inputFiles;inputFiles2;inputFiles3;inputFiles4;inputFiles5 }

process makeRange {
    tag { sample + ' ' + ref }

    input:
    set val(sample),val(bam), val(ref), val(illumina),val(f5s) from inputFiles

    output:
    set val(sample),file("windows/*.txt") into ranges

    """
    mkdir windows
    python ~/soft/nanopolish/scripts/nanopolish_makerange.py --segment-length 2000 $ref > ranges.txt 
    split -l 1 --additional-suffix .txt  ranges.txt windows/range_
    """
}

process binReads {
    tag { sample + ' ' + ref }

    input:
    set val(sample),val(bam), val(ref), val(illumina),val(f5s) from inputFiles2

    output:
    set val(sample),val(bam), val(ref), val(illumina),val(f5s),file('fqs') into binnedfastq

    script:
    """
    mkdir fqs
    bamToFastq -i $bam -fq fqs/out.fastq
    """
}

//process rebinf5s {
//    tag { sample + ' ' + ref } 
//    input:
//    set val(sample),val(bam), val(ref), val(illumina),val(f5s),file('fqs') from binnedfastq
//
//    output:
//    set val(sample),val(bam), val(ref), val(illumina),file("${sample}_f5s"),file('fqs') into binnedf5
//
//    script:
//    """
//    rebinner.py -f5s ${f5s}/*.fast5 -fq fqs/out.fastq -o ${sample}_f5s
//    """
//}

process nanoplish {
    tag { sample + ' ' + ref }

    input: 
    set val(sample),val(bam), val(ref), val(illumina),val(f5s),file('fqs') from binnedfastq

    output:
    set val(sample),val(bam), val(ref), val(illumina),val(f5s),file('dedup') into indexed

    
    script:
    scriptBase='/home/nick/soft/ngonpipe/scripts'
    """
    mkdir dedup
    python3 ${scriptBase}/dedupe.py fqs/out.fastq dedup/dedup.fastq
    nanopolish index  -d $f5s dedup/dedup.fastq
    """

}

ranges
    .transpose()
    .map { sample, file -> tuple(sample, file.baseName, file) }
    .view()
    .into{ rangeJobs }

process polishRange {
    tag { sample + ' ' + range }
//    publishDir 'VCFs_map', pattern: '*.vcf', overwrite: true, mode: 'copy'

    input:
    set val(sample),val(range),file('range.txt'),val(bam), val(ref), val(illumina),val(f5s),file('dedup') from rangeJobs.combine(indexed, by:0)

    output:
    set val(sample),val(range),file("${sample}_${range}.vcf") into polished
    set val(sample),file("${sample}_${range}.vcf") into polishedVCFs

    """
    cat range.txt
    region=\$(cat range.txt)
    echo \$region
    nanopolish variants -o ${sample}_${range}.vcf \
        -r dedup/dedup.fastq \
        -b $bam \
        --fix-homopolymers \
        -w \$region \
        --methylation-aware dcm,dam \
        --threads 1 \
        --ploidy 1 \
        -d 1 \
        -g $ref
        
    """

}

process pysamStats {
    tag { sample }

    publishDir 'pysams',  mode: 'copy', overwrite: true
    
    input:
    set val(sample),val(bam), val(ref), val(illumina),val(f5s) from inputFiles4

    output:
    set val(sample),file("${sample}_pysamfile.txt") into pysamStats_out, pysamStats_out2

    script:
    """
    pysamstats -t variation_strand ${bam} -f ${ref} > ${sample}_pysamfile.txt
    """    

}

process processPysam {
    tag { sample }
    maxForks 1 

    publishDir 'pysam_models', pattern: '*.sav', overwrite: true, mode: 'copy'        
    publishDir 'pysam_curves', pattern: '*.pdf', overwrite: true, mode: 'copy'        
    publishDir 'pysam_csvs', pattern: '*.csv', overwrite: true, mode: 'copy'    

    input:
    set val(sample), val(bam), val(ref), val(illumina), val(f5s), file('pysamfile.txt') from inputFiles5.combine(pysamStats_out2,by:0)

    output:
    set file('*.pdf'),file('*.csv'),file('*.sav')

    script:
    scriptBase='/home/nick/soft/ngonpipe/threshold_analysis'
    """
    python3 ${scriptBase}/pysamML.py  \
    --sample ${sample} \
    -p pysamfile.txt \
    -t ${illumina} \
    -r ${ref}
    """
}

process processVCF {
    tag { sample }
    publishDir 'models', pattern: '*.sav', overwrite: true, mode: 'copy'
    publishDir 'curves', pattern: '*.pdf', overwrite: true, mode: 'copy'
    publishDir 'csvs', pattern: '*.csv', overwrite: true, mode: 'copy'


    input:
    set val(sample), file("*"),val(bam), val(ref), val(illumina),val(f5s),file('pysamfile.txt') from polishedVCFs.groupTuple().combine(inputFiles3,by:0).combine(pysamStats_out,by:0)

    output:
    set val(sample),file("*.sav") into models
    set val(sample),file("*.pdf") into curves
    set val(sample),file("*.csv") into csvs

    script:
    scriptBase='/home/nick/soft/ngonpipe/threshold_analysis'
    """

    mkdir vcfs
    mv *.vcf vcfs/
    python3 ${scriptBase}/vcf_stats.py \
        vcfs/ \
        ${illumina} \
        $sample

    python3 ${scriptBase}/ROC_AUC.py \
    -snps ${sample}_SNPs.csv \
    --sample ${sample} \
    -p pysamfile.txt \
    -t ${illumina} \
    -r ${ref}

    """
}
