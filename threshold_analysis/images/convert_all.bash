
pdfs=*.pdf

for pdf in ${pdfs}
do
	b=$(basename $pdf .pdf)
	echo $b
	convert ${pdf} ${b}.png
done
