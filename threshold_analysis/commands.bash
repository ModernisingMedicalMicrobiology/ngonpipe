nextflow ~/soft/ngonpipe/threshold_analysis/consensus.nf --inFiles inputs.txt --base ~/soft/ngonpipe/ -resume -with-trace
python3 ~/soft/ngonpipe/scripts/parseDNAdiff.py diffs/
python3 ~/soft/ngonpipe/threshold_analysis/analysis.py
