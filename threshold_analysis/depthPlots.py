import pandas as pd
import seaborn as sns
sns.set_style('darkgrid')
import matplotlib.pyplot as plt

depths=[0.01, 0.02, 0.03, 0.05, 0.1, 1]
def getDepths(depth):
    f='/mnt/Data2/MDR_GC/spiked/ngonpipe/variants/bams/PHE_208_209.{0}_depth.csv'.format(str(depth))
    df=pd.read_csv(f)
    df['subsample']=depth
    return df
dfs=[]

for depth in depths:
    dfs.append(getDepths(depth))
df=pd.concat(dfs)

dfStats=[]
def getStats(depth):
    f='/mnt/Data2/MDR_GC/spiked/ngonpipe/variants/H18-208-{0}_recall_precision.csv'.format(str(depth).replace('.',''))
    df=pd.read_csv(f)
    df['subsample']=depth
    return df

for depth in depths:
    dfStats.append(getStats(depth))

dfs=pd.concat(dfStats)
df2=dfs.merge(df,on=['subsample'],how='outer')

g=sns.lineplot('avDepth','FP',data=df2,hue='Features used')
plt.savefig('FP_avDepth_line.pdf')
plt.show()

g=sns.scatterplot('avDepth','TP',data=df2,hue='Features used')
plt.savefig('TP_avDepth.pdf')
plt.clf()

g=sns.scatterplot('avDepth','FN',data=df2,hue='Features used')
plt.savefig('FN_avDepth.pdf')


#df3=df2.melt(id_vars=['Features used','subsample', 'chrom','length','bases','avDepth', 'position cov1', 'position cov10','covBreadth1x','covBreadth10x'])
#
#
#SNPs=df3[df3.variable.isin(['TP','TN','FN','FP'])]
#scores=df3[~df3.variable.isin(['TP','TN','FN','FP'])]
#
#g=sns.FacetGrid(SNPs, row='variable', hue="Features used")
#g.map(plt.scatter, "avDepth", "value")
#g.add_legend()
#plt.xscale('symlog')
#plt.show()
#
#g=sns.FacetGrid(scores, row='variable', hue="Features used")
#g.map(plt.scatter, "avDepth", "value")
#g.add_legend()
#plt.xscale('symlog')
#plt.show()
#
