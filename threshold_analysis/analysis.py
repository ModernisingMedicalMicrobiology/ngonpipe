import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df=pd.read_csv('all_bases.csv',index_col=0)
sns.set_style('darkgrid')
df['ref']=df.index
df=df[df.ref=='qry']
df['Percent genome called']=(df['AlignedBases']/df['TotalBases'])*100
g=sns.lineplot('prop support','Number',hue='sample',data=df)
g.set(xlabel='Percent reads supporting call',ylabel='Number of false substitutions')
plt.savefig('SNPs_over_supporting_reads.pdf')
#plt.show()
plt.clf()

g=sns.lineplot('prop support','Percent genome called',hue='sample',data=df)
g.set(xlabel='Percent reads supporting call')
plt.savefig('Coverage_over_supporting_reads.pdf')
#plt.show()
plt.clf()
