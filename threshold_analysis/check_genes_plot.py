#!/usr/bin/env python3
import sys
import os
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

def getModel(f):
    models=['fast','HAC','HAC-trained']
    for model in models:
        if model in f: m = model
    return m

def getStrain(f):
    strains=['WHOQ','WHOF','WHOX','WHOV','H18-208']
    for strain in strains:
         if strain in f: s = strain
    return s

def getDepth(f):
    return int(f.split('_')[-1])


def get23s(fol):
    runs=os.listdir(fol)
    runs=[run for run in runs if run.endswith('vcfs')]
    dfs=[]
    for run in runs:
        f='{0}/{1}/23s.csv'.format(fol,run)
        df=pd.read_csv(f)
        df['Sample']=run.replace('_vcfs','')
        df['Strain']=df.Sample.map(getStrain)
        df['Model']=df.Sample.map(getModel)
        df['Depth']=df.Sample.map(getDepth)
        dfs.append(df)
    df=pd.concat(dfs)
    return df

def plot23s(df):
    g=df.groupby(['Strain','Model','Depth'])['POS'].count()
    print(g)
    g.to_csv('23s_stats.csv')

def getRefStats(fol):
    runs=os.listdir(fol)
    runs=[run for run in runs if run.endswith('vcfs')]
    dfs=[]
    names=["Sample","abx","gene","Pos","residue"]
    for run in runs:
        f='{0}/{1}/refgenes.txt'.format(fol,run)
        df=pd.read_csv(f,sep='\t',names=names)
        df['Sample']=run.replace('_vcfs','')
        df['Strain']=df.Sample.map(getStrain)
        df['Model']=df.Sample.map(getModel)
        df['Depth']=df.Sample.map(getDepth)
        dfs.append(df)
    df=pd.concat(dfs)
    return df

def gyrA(df):
    expected=pd.read_csv('gyrA_expected.csv')
    df=df[df.gene=='gyrA']
    df=df.merge(expected,how='left', 
            left_on=['Strain','gene','Pos'],
            right_on=['Strain','gene','Pos'])
    df['Correct residue']=np.where(df['residue']==df['Expected residue'], 0, 1)

    df2=df.pivot_table(index=['gene','Strain','Model','Pos','Expected residue'], 
            columns='Depth', 
            values='residue', 
            aggfunc=lambda x: ' '.join(str(v) for v in x))
    print(df2)
    g=df.groupby(['Model','Depth'])['Correct residue'].sum()
    print(g)
    g.to_csv('gyrA_concordance.csv')
    df.to_csv('gyrA_results.csv')
    return df2

def parC(df):
    expected=pd.read_csv('parC_expected.csv')
    df=df[df.gene=='parC']
    df=df.merge(expected,how='left',
            left_on=['Strain','gene','Pos'],
            right_on=['Strain','gene','Pos'])
    df['Correct residue']=np.where(df['residue']==df['Expected residue'], 0, 1)
    df=df.dropna()
    print(df)
    df2=df.pivot_table(index=['gene','Strain','Model','Pos','Expected residue'], 
            columns='Depth', 
            values='residue', 
            aggfunc=lambda x: ' '.join(str(v) for v in x))
    print(df2)
    g=df.groupby(['Model','Depth'])['Correct residue'].sum()
    print(g)
    g.to_csv('parC_concordance.csv')
    df.to_csv('parC_results.csv')
    return df2

def ponA(df):
    expected=pd.read_csv('ponA_expected.csv')
    df=df[df.gene=='ponA']
    df=df.merge(expected,how='left',
            left_on=['Strain','gene','Pos'],
            right_on=['Strain','gene','Pos'])
    df['Correct residue']=np.where(df['residue']==df['Expected residue'], 0, 1)
    df=df.dropna()
    print(df)
    df2=df.pivot_table(index=['gene','Strain','Model','Pos','Expected residue'], 
            columns='Depth', 
            values='residue', 
            aggfunc=lambda x: ' '.join(str(v) for v in x))
    print(df2)
    g=df.groupby(['Model','Depth'])['Correct residue'].sum()
    print(g)
    g.to_csv('ponA_concordance.csv')
    df.to_csv('ponA_results.csv')
    return df2

def mtrR(df):
    expected=pd.read_csv('mtrR_expected.csv')
    df=df[df.gene=='mtrR']
    df=df.merge(expected,how='left',
            left_on=['Strain','gene','Pos'],
            right_on=['Strain','gene','Pos'])
    df['Correct residue']=np.where(df['residue']==df['Expected residue'], 0, 1)
    df=df.dropna()
    print(df)
    df2=df.pivot_table(index=['gene','Strain','Model','Pos','Expected residue'], 
            columns='Depth', 
            values='residue', 
            aggfunc=lambda x: ' '.join(str(v) for v in x))
    print(df2)
    g=df.groupby(['Model','Depth'])['Correct residue'].sum()
    print(g)
    g.to_csv('mtrR_concordance.csv')
    df.to_csv('mtrR_results.csv')
    return df2

def rpsJ(df):
    expected=pd.read_csv('rpsJ_expected.csv')
    df=df[df.gene=='rpsJ']
    df=df.merge(expected,how='left',
            left_on=['Strain','gene','Pos'],
            right_on=['Strain','gene','Pos'])
    df['Correct residue']=np.where(df['residue']==df['Expected residue'], 0, 1)


    df=df.dropna()
    print(df)
    df2=df.pivot_table(index=['gene','Strain','Model','Pos','Expected residue'], 
            columns='Depth', 
            values='residue', 
            aggfunc=lambda x: ' '.join(str(v) for v in x))
    print(df2)
    #g=df.groupby(['Model','Depth'])['Correct residue'].sum()
    #print(g)
    #g.to_csv('rpsJ_concordance.csv')
    df.to_csv('rpsJ_results.csv')
    return df2


def run():
    df23s=get23s('filtered_vcfs')
    plot23s(df23s)
    refdf=getRefStats('filtered_vcfs')
    refdf.to_csv('ref_muts.csv')
    spread=[]
    spread.append(gyrA(refdf))
    spread.append(parC(refdf))
    spread.append(ponA(refdf))
    spread.append(mtrR(refdf))
    spread.append(rpsJ(refdf))
    spreads=pd.concat(spread)
    print(spreads)
    spreads.to_csv('combined_refGenes.csv')
    spreads.to_excel('combined_refGenes.xlsx')


run()

