#!/usr/bin/env python3
import sys
import pandas as pd
import seaborn as sns
sns.set_style('darkgrid')
import matplotlib.pyplot as plt
import os


def getModel(f):
    models=['fast','HAC','HAC-trained']
    for model in models:
        if model in f: m = model
    return m

def getStrain(f):
    strains=['WHOQ','WHOF','WHOX','WHOV','H18-208']
    for strain in strains:
         if strain in f: s = strain
    return s
         

def getCsvs(f):
    df=pd.read_csv('csvs/'+f)
    #df['Sample']=f.replace('_recall_precision.csv','')
    return df

csvs=os.listdir('csvs/')
csvs=[f for f in csvs if f.endswith('_recall_precision.csv')]
dfs=map(getCsvs,csvs)
df=pd.concat(dfs)
df=df[['Sample','Features used','TP','TN','FN','FP','missed','Accuracy','Precision','Recall','F1 Score','depth']]
#df=df[df['Features used']=='composite']
df.dropna(inplace=True)
df['Strain']=df.Sample.map(getStrain)
df['Model']=df.Sample.map(getModel)
#ignore_strains=['WHOV','WHOF']
#df=df[~df.Strain.isin(ignore_strains)]
df=df.sort_values(by=['Sample','Features used'])
df.to_csv('combined_recall_precision.csv',index=False)


def plot_recall_depth(df):
    df=df[df['Features used'] == 'composite']
    g=sns.scatterplot('depth','Recall',hue='Sample',data=df)
    g.legend(loc='bottom left', bbox_to_anchor=(1.25, 0.5), ncol=1)
    plt.show()
    plt.savefig('depth_recall.png')
    plt.clf()

def plot_FP_depth(df):
    df=df[df['Features used'] == 'composite']
    g=sns.scatterplot('depth','FP',hue='Model',style='Strain',data=df)
    g.legend(loc='bottom left', bbox_to_anchor=(1.25, 0.5), ncol=1)
    plt.show()
    plt.savefig('depth_FP.png')
    plt.clf()


def plot_recall_depth_comp(df):
    df=df[df['Features used'] == 'composite']
    g=sns.scatterplot('depth','Recall',hue='Model',style='Strain',data=df,s=50)
    g.legend(loc='lower right',ncol=2)
    g.set(ylim=(0, 1))
    g.set(xlim=(0, 130))
    plt.savefig('depth_recall.png')
    plt.show()
    plt.clf()

def plot_TN_depth_comp(df):
    df=df[df['Features used'] == 'composite']
    g=sns.scatterplot('depth','TN',hue='Model',style='Strain',data=df,s=50)
    g.legend(loc='upper left',bbox_to_anchor=(1.04,1), ncol=1)
    g.set(ylim=(0, None))
    g.set(xlim=(0, 130))
    #plt.tight_layout()
    plt.savefig('TN_recall.png')
    plt.show()
    plt.clf()

def plot_accuracy_depth_comp(df):
    df=df[df['Features used'] == 'composite']
    g=sns.scatterplot('depth','Accuracy',hue='Model',style='Strain',data=df,s=50)
    g.legend(loc='lower right',ncol=2)
    g.set(ylim=(0, 1))
    g.set(xlim=(0, 130))
    plt.savefig('Accuracy_recall.png')
    plt.show()
    plt.clf()

def plot_F1_depth_comp(df):
    df=df[df['Features used'] == 'composite']
    g=sns.scatterplot('depth','F1 Score',hue='Model',style='Strain',data=df,s=50)
    g.legend(loc='lower right',ncol=2)
    g.set(ylim=(0, 1))
    g.set(xlim=(0, 130))
    plt.savefig('F1_depth.png')
    plt.show()
    plt.clf()
#plot_recall_depth(df)
#plot_FP_depth(df)
plot_TN_depth_comp(df)
plot_recall_depth_comp(df)
plot_accuracy_depth_comp(df)
plot_F1_depth_comp(df)
