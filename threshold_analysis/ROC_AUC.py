import sys
from functools import reduce
import numpy as np  
import pandas as pd  
import matplotlib.pyplot as plt  
import seaborn as sns  
# roc curve and auc score
from sklearn.neighbors import KNeighborsClassifier  
from sklearn.ensemble import RandomForestClassifier  
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import train_test_split  
from sklearn.metrics import roc_curve  
from sklearn.metrics import precision_recall_curve
#from sklearn.utils.fixes import signature
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
from sklearn import svm
from sklearn.model_selection import StratifiedKFold
from itertools import permutations
import pickle
from argparse import ArgumentParser, SUPPRESS
from Bio import SeqIO

basesN=['A', 'C', 'G', 'T']
perm = permutations(basesN,2)
N,perms=0,{}
for p in perm:
    perms.setdefault(p[0],{}).setdefault(p[1],N)
    N+=1

def plot_roc_curve(d,depth,opts):
    for i in d:
        plt.plot(d[i]['fpr'], d[i]['tpr'], label='{0}, AUC:{1:.2f}'.format(i,d[i]['AUC']))
    plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver Operating Characteristic (ROC) Curve, mean depth: {0:.1f}'.format(depth))
    plt.legend()
    plt.tight_layout()
    plt.savefig('{0}_ROC.pdf'.format(opts.id))
    #plt.show()
    plt.clf()


#def plot_recall_precision(d,depth,opts):
#    for i in d:
#        average_precision = average_precision_score(d[i]['y_test'], d[i]['y_score'])
#        precision, recall, _ = precision_recall_curve(d[i]['y_test'], d[i]['y_score'])
#        step_kwargs = ({'step': 'post'}
#               if 'step' in signature(plt.fill_between).parameters
#               else {})
#        plt.step(recall, precision, alpha=0.2,where='post', label='{0} AP:{1:.2f}'.format(i,average_precision))
#    plt.xlabel('Recall')
#    plt.ylabel('Precision')
#    plt.ylim([0.0, 1.05])
#    plt.xlim([0.0, 1.0])
#    plt.title('Precision-Recall curve, mean depth: {0:.1f}'.format(depth))
#    plt.legend()
#    plt.tight_layout()
#    plt.savefig('{0}_PR.pdf'.format(opts.sample))
#    plt.clf()
#    #plt.show()


def addBaseChangeN(row):
    r=row['REF']
    a=row['ALT']
    p=perms[r][a]
    return p

def alphaBeta(row):
    if row['REF'] == 'A' and row['ALT'] == 'G':
        return 0
    elif row['REF'] == 'A' and row['ALT'] == 'T':
        return 1
    elif row['REF'] == 'A' and row['ALT'] == 'C':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'A':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'G':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'T':
        return 0
    elif row['REF'] == 'G' and row['ALT'] == 'A':
        return 0
    elif row['REF'] == 'G' and row['ALT'] == 'C':
        return 1
    elif row['REF'] == 'G' and row['ALT'] == 'T':
        return 1
    elif row['REF'] == 'T' and row['ALT'] == 'A':
        return 1
    elif row['REF'] == 'T' and row['ALT'] == 'C':
        return 0
    elif row['REF'] == 'T' and row['ALT'] == 'G':
        return 1



def getData(inputFile,pysamFile):
    df=pd.read_csv(inputFile)
    df=df[df['Illumina base']!='N']
    df=df[df['Illumina base']!='-']
    df.drop_duplicates(subset=['#CHROM','POS'],inplace=True)
    df.sort_values(by=['#CHROM','POS'],inplace=True)
    df['5prime proximity']=df['POS']-df['POS'].shift(1)
    df['3prime proximity']=df['POS']-df['POS'].shift(-1)
    df['3prime proximity']=df['3prime proximity'].abs()
    df['proximty']=df[['5prime proximity','3prime proximity']].min(axis=1)
    df['alphabeta']=df.apply(alphaBeta,axis=1)
    df=df[df.REF_len==1]
    df=df[df.ALT_len==1]
    df['baseChange']=df.apply(addBaseChangeN,axis=1)
    # add basecall rations from pysamstats file
    #psdf=pd.read_csv(pysamFile,sep='\t')
    psdf=pd.read_csv(pysamFile,sep='\t')
    psdf1=psdf[psdf['reads_all']>0]
    psdf5=psdf[psdf['reads_all']>=5]
    psdf10=psdf[psdf['reads_all']>=10]
    psdf20=psdf[psdf['reads_all']>=20]
    df=df.merge(psdf,left_on=['#CHROM','POS'],right_on=['chrom','pos'],how='left')
    bases=['A','T','C','G','insertions','deletions']
    for b in bases:
        df['{0} %'.format(b)]=(df[b]/df['reads_all'])*100
    df['reads_all']=df[['A','T','C','G']].sum(axis=1)
    df['top_base'] = df[['A','T','C','G']].max(axis=1)
    df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
    df['majority base %'] = (df['top_base'] / df['reads_all'])
    df['Top Base matches Nanopolish'] = np.where(df.ALT == df.top_base_seq,1,0)
    df=df[df['majority base %'].notna()]

    #df['Nanopolish base'] = df.loc(df.ALT)
    #df['Nanopolish base %'] = (df['Nanopolish base'] / df['reads_all'])*100

    return df,len(psdf1),len(psdf5),len(psdf10),len(psdf20)

def plots(df):
    #g = sns.FacetGrid(df,col='SNP validation')
    #g = g.map(plt.scatter,'proximty','QUAL',s=0.3, alpha=0.75)
    #plt.show()

    # Qual distplot 
    f=df[df['SNP validation']==False]
    t=df[df['SNP validation']==True]
    g=sns.distplot(f.QUAL,hist=False,label='False SNPs')
    g=sns.distplot(t.QUAL,hist=False,label='True SNPs')
    #plt.show()

def plot_feature_importances(importances,std,indices,features,feat,opts):
    plt.figure()
    plt.title("Relative importance of features")
    df=pd.DataFrame({'Importance':importances,'std':std,'indices':indices,'feature':features})
    df.to_csv('{0}_{1}_feat_importance.csv'.format(opts.id,feat.replace(' ','_')))
    g = sns.barplot(y='feature', x="Importance", xerr=df['std'], capsize=.2, data=df)
#    plt.bar(importances[indices],features
#            color="r", yerr=std[indices], align="center")
    #plt.xticks(features, indices)
    #plt.xlim([-1, len(features)])
    plt.tight_layout()
    plt.savefig('{0}_{1}_feat_importance.pdf'.format(opts.id,feat.replace(' ','_')))
    plt.clf()

def classify(features,df,feat,opts):
    # train and test
    X=np.array(df[features])
    Y=np.array(df['SNP validation'])
    trainX, testX, trainy, testy = train_test_split(X, Y, test_size=0.3, random_state=1)
    model = RandomForestClassifier()
    #model = GradientBoostingClassifier()
    model.fit(trainX, trainy)
    prob = model.predict_proba(testX)
    probs = prob[:, 1]
    auc = roc_auc_score(testy, probs)
    fpr, tpr, thresholds = roc_curve(testy, probs)
    filename = '{1}_{0}_model.sav'.format(feat.replace(' ','_'),opts.sample)
    pickle.dump(model, open(filename, 'wb'))
    #saveTree(model,features)
    # classify all
    preds= model.predict(X)
    probs=model.predict_proba(X)
    importances = model.feature_importances_
    std = np.std([tree.feature_importances_ for tree in model.estimators_],axis=0)
    indices = np.argsort(importances)[::-1]
    plot_feature_importances(importances,std,indices,features,feat,opts)
    #probs=probs[:, 1]
    return {'fpr':fpr,'tpr':tpr,'AUC':auc,'probs':probs,'preds':preds,
            'y_test':Y,'y_score':preds}


def terms(r,col='classification'):
    if r['SNP validation'] == True and r[col] == True:
        return 'TP'
    elif r['SNP validation'] == True and r[col] == False:
        return 'FN'
    elif r['SNP validation'] == False and r[col] == False:
        return 'TN'
    elif r['SNP validation'] == False and r[col] == True:
        return 'FP'


def table_recall_precision(df,missed,opts,depth, allSites, IlluminaIgnored, IlluminaSNPs, wt1,wt5,wt10,wt20):
    #g=sns.barplot('Type','SNPs',hue='Features used',data=df)
    #plt.show()
    df2=df.pivot(index='Features used',columns='Type',values='SNPs')
    df2=df2.merge(missed,on=['Features used'],how='left')
    if 'FP' not in df2:
        df2['FP'] = 0
    df2['All sites']=allSites
    df2['Illumina covered']=IlluminaIgnored
    df2['IlluminaSNPs']=IlluminaSNPs
    df2['d1']=wt1
    df2['d5']=wt5
    df2['d10']=wt10
    df2['d20']=wt20
    df2['FN']=df2['FN']+df2['missed']
    df2['FP rate']=df2['FP']/df2['Illumina covered']
    df2['Accuracy'] = (df2['TP'] + df2['TN']) / (df2['TP']+df2['FP']+df2['FN']+df2['TN'])
    df2['Precision']= df2['TP'] / (df2['TP']+df2['FP'])
    df2['Recall']   = df2['TP'] / (df2['TP']+df2['FN'])
    df2['F1 Score'] = 2*(df2['Recall'] * df2['Precision']) / (df2['Recall'] + df2['Precision'])
    df2['depth']=depth
    df2['Sample']=opts.sample
    df2.to_csv('{0}_recall_precision.csv'.format(opts.id))
    print(df2)

def saveTree(model,features):
    ## Extract single tree
    estimator = model.estimators_[5]
    from sklearn.tree import export_graphviz
    # Export as dot file
    export_graphviz(estimator, out_file='tree.dot', 
                feature_names = features,
                class_names = ['False','True'],
                rounded = True, proportion = False, 
                precision = 2, filled = True)

def getAllTrueSNPs(opts):
    bases=['A','C','G','T']
    true=open(opts.true,'rt')
    ref=open(opts.ref,'rt')
    n,p=0,0
    SNPs=[]
    for true_seq,ref_seq in zip(SeqIO.parse(true,'fasta'),SeqIO.parse(ref,'fasta')):
        for t,r in zip(true_seq.seq,ref_seq):
            n+=1
            if t not in bases or r not in bases: continue
            p+=1
            if t != r:
                SNPs.append({'POS':n,'REF':r,'ALT':t,'#CHROM':ref_seq.id,'Origin':'truth'})
    df=pd.DataFrame(SNPs)
    ref.close()
    true.close()
    return df, n, p, len(df)

def maskProbs(r,feat='feat'):
    if r['{0} pred'.format(feat)]==False:
        return False
    elif r['{0} pred'.format(feat)] == True and r['{0} prob'.format(feat)] > 0.95:
        return True
    else:
        return False


def run(opts):
    truths, allSites, IlluminaIgnored, IlluminaSNPs=getAllTrueSNPs(opts)
    df,wt1,wt5,wt10,wt20=getData(opts.snps,opts.pysam)
    d={}

    feature_combinations={
            'QUAL only':['QUAL'],
            'SF only':['Support fraction'],
            'QUAL SF':['QUAL','Support fraction'],
#            'QUAL SF TR':['QUAL','Support fraction','Total reads'],
#            'QUAL SF TR PR':['QUAL','Support fraction','Total reads','proximty'],
#            'QUAL SF BC':['QUAL','Support fraction','baseChange'],
#            'QUAL SF TR BC':['QUAL','Support fraction','Total reads','baseChange','deletions %'],
#            'QUAL SF MB':['QUAL','Support fraction','majority base %'],
#            'QUAL SF TR BC MB':['QUAL','Support fraction','Total reads','baseChange','majority base %'],
#            'QUAL SF TR BC MB TB':['QUAL','Support fraction','Total reads','baseChange','majority base %','Top Base matches Nanopolish'],
            'composite':['QUAL','Support fraction','Total reads','proximty','baseChange','majority base %','Top Base matches Nanopolish','deletions %','A %','T %','C %','G %','insertions %']
#            'QUAL SF TR PR BC MB TB BCF':['QUAL','Support fraction','Total reads','proximty','baseChange','majority base %','Top Base matches Nanopolish','BaseCalledFraction']
#            'QUAL SF TR PR BC MB TB B%':['QUAL','Support fraction','Total reads','proximty','baseChange','majority base %','Top Base matches Nanopolish','A %','T %','C %','G %','insertions %','deletions %'],
#            'QUAL SF TR PR BC MB TB B% AB':['QUAL','Support fraction','Total reads','proximty','baseChange','majority base %','Top Base matches Nanopolish','A %','T %','C %','G %','insertions %','deletions %','alphabeta']
            }

    dfs=[]
    for feat in feature_combinations:
        features=feature_combinations[feat]
        d[feat]=classify(features,df,feat,opts)
        #df['{0} probs'.format(feat)]=d[feat]['probs']
        df['{0} pred'.format(feat)]=d[feat]['preds']

        # probs
        probs=pd.DataFrame(d[feat]['probs'],columns=[True,False])
        #probs[feat] = probs[[True, False]].idxmax(axis=1)
        p=probs.max(axis=1)
        df['{0} prob'.format(feat)] = p
        df['{0} prob true'.format(feat)] = d[feat]['probs'][:, 1]
        df['{0} prob false'.format(feat)] = d[feat]['probs'][:, 0]
        #df[feat] = np.where(df['{0} prob'.format(feat)] > 0.997, True,False)
        df[feat]=df.apply(maskProbs,axis=1,feat=feat)
        df['{0} type'.format(feat)] = df.apply(terms,axis=1,col=feat)

        # groupby classifications/validations to work out FPs,TPs etc
        g=df.groupby(by=['SNP validation',feat])['POS'].count().reset_index()
        g=g.rename({feat:'classification','POS':feat},axis=1)
        dfs.append(g)
    # merge all 
    df=truths.merge(df,on=['#CHROM','POS','REF','ALT'],how='outer')
    df.sort_values(by=['#CHROM','POS'],inplace=True)
    psdf=pd.read_csv(opts.pysam,sep='\t')
    psdf=psdf[['chrom','pos','reads_all']]
    psdf.rename(columns={'reads_all':'read depth'},inplace=True)
    df=df.merge(psdf,left_on=['#CHROM','POS'],right_on=['chrom','pos'],how='left')
    # calculate missed SNPs
    missed=[]
    for feat in feature_combinations:
        df2=df[df.Origin=='truth']
        df2=df2[df2[feat].isnull()]
        missed.append({'Features used':feat,'missed':len(df2)})

    missed=pd.DataFrame(missed)
    stats = reduce(lambda left,right: pd.merge(left,right,on=['SNP validation','classification']), dfs)
    stats['Type']=stats.apply(terms,axis=1)
    cols=list(feature_combinations.keys())
    cols.append('Type')
    stats=stats[cols]
    stats=stats.melt(id_vars=['Type'])
    stats=stats.rename({'variable':'Features used','value':'SNPs'},axis=1)

    # plot and save
    depth=int(opts.depth)
    df['sub depth']=depth
    df.to_csv('{0}_classifications.csv'.format(opts.id),index=False)
#    depth=df['reads_all'].mean()
    
    #plot_recall_precision(d,depth,opts)
    table_recall_precision(stats,missed,opts,depth,allSites, IlluminaIgnored, IlluminaSNPs,wt1,wt5,wt10,wt20)
    plot_roc_curve(d,depth,opts)





if __name__ == "__main__":
    parser = ArgumentParser(description='train ML from validated SNPs')
    parser.add_argument('-snps', '--snps', required=True,
                             help='csv file of SNPs to classify')
    parser.add_argument('-s', '--sample', required=True,
                             help='sample name')
    parser.add_argument('-d', '--depth', required=True,
                             help='subsample depth')
    parser.add_argument('-i', '--id', required=True,
                             help='identifier')
    parser.add_argument('-p', '--pysam', required=True,
                             help='pysam file')
    parser.add_argument('-t', '--true', required=True,
                             help='Truth sequence, e.g. illumina reference')
    parser.add_argument('-r', '--ref', required=True,
                             help='reference sequence')
    opts, unknown_args = parser.parse_known_args()
    run(opts)

