#!/usr/bin/env python3
import sys
import os
import numpy as np
import pandas as pd
from argparse import ArgumentParser, SUPPRESS
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from itertools import permutations
import pickle
import vcf
import subprocess
import gzip
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastnCommandline, NcbiblastpCommandline
from io import StringIO


def getRefVariants(outf,inf,ref,sampleName):
    DEBUG = True
    
    o = open(outf, 'a')
    
    refFile = ref
    refFa = SeqIO.read(refFile, 'fasta')
    # relative to ref geneome - http://www.ncbi.nlm.nih.gov/nuccore/NC_011035.1
    
    #list of genes to extract
    # gene: name, start, end, drug
    #number starting from one, will convert to python form in script: gene, start, end, strand, antibiotic
    geneList = [
                #['penA', 1524397, 1526148, -1,'cfx'], 
                #['penB', 2049351, 2050397, -1,'cfx'], 
                ['mtrR', 1332867, 1333499, 1,'cro'], 
                ['pilQ', 103068, 105263, -1,'cro'], 
                ['ponA', 108362, 110758, 1,'cro'],
                ['mtrR', 1332867, 1333499, 1,'cfx'], 
                ['pilQ', 103068, 105263, -1,'cfx'], 
                ['ponA', 108362, 110758, 1,'cfx'],
                ['gyrA', 1051396, 1054146, 1, 'cip'], 
                #['gyrB', 2098152, 2100542, -1, 'cip'], 
                ['parC', 193663, 195966, -1, 'cip'], #,
                #['parE', 1296483, 1298468, -1, 'cip'],
                ['norM_promoter', 460253, 460253, 0, 'cip'],
                ['rpsJ', 2031311, 2031622, 1, 'tet'],
                ['pilQ', 103068, 105263, -1,'tet'],
                ['mtrR', 1332867, 1333499, 1,'tet'],
                ['mtrR', 1332867, 1333499, 1,'pen'], 
                ['pilQ', 103068, 105263, -1,'pen'], 
                ['ponA', 108362, 110758, 1,'pen'],
                ['mtrR', 1332867, 1333499, 1,'azt'],
                ['macAB_promoter', 1415365, 1415365, 0, 'azt'] 
                ]
    
    #get list of known AA variants, except mtr promoter which analyses nucleotides
    #http://cmr.asm.org/content/27/3/587.full.pdf
    
    knownSNPs = dict()
    knownSNPs['cro'] = dict()
    #do from de novo
    #knownSNPs['cfx']['penA'] = [311, 312, 316, 483, 501, 512, 542, 545, 551] 
    knownSNPs['cro']['penB'] = [120, 121] #G120K and G120D/A121D
    knownSNPs['cro']['mtrR'] = [45, 39] #G45D A39T
    #mtr - promoter - deletion - nucleotide
    knownSNPs['cro']['pilQ'] = [666] #E666K
    knownSNPs['cro']['ponA'] = [421] #L421P
    
    knownSNPs['cfx'] = dict()
    #do from de novo
    #knownSNPs['cfx']['penA'] = [311, 312, 316, 483, 501, 512, 542, 545, 551] 
    knownSNPs['cfx']['penB'] = [120, 121] #G120K and G120D/A121D
    knownSNPs['cfx']['mtrR'] = [45, 39] #G45D A39T
    #mtr - promoter - deletion - nucleotide
    knownSNPs['cfx']['pilQ'] = [666] #E666K
    knownSNPs['cfx']['ponA'] = [421] #L421P
    
    knownSNPs['pen'] = dict()
    knownSNPs['pen']['mtrR'] = [39, 45] #G45D
    knownSNPs['pen']['pilQ'] = [666] #E666K
    knownSNPs['pen']['ponA'] = [421] #L421P
    
    
    knownSNPs['cip'] = dict()
    knownSNPs['cip']['gyrA'] = [91, 95] #['S91F', 'D95N', 'D95G'] 
    knownSNPs['cip']['parC'] = [86, 87, 88, 91] #['D86N', 'S88P', 'E91K']
    knownSNPs['cip']['norM_promoter'] = [1] #SNP C to T
    
    knownSNPs['tet'] = dict()
    knownSNPs['tet']['mtrR'] = [39,45] #G45D
    knownSNPs['tet']['pilQ'] = [666] #E666K
    knownSNPs['tet']['rpsJ'] = [57] #V57M
    
    knownSNPs['azt'] = dict()
    knownSNPs['azt']['mtrR'] = [39, 45] #G45D, A39T
    knownSNPs['azt']['macAB_promoter'] = [1] #SNP C to A
    
    f = open( inf )
    fa = SeqIO.read( f, 'fasta' )
    if DEBUG: sys.stdout.write('Mapped file: %s\n'%inf)
    
    for gene in geneList:
        if DEBUG: sys.stdout.write('Gene: %s\n'%gene[0])
        strand = gene[3]
        abx = gene[4]
        
        if strand == 1:
            proteinSeq = fa[gene[1]-1:gene[2]-1].seq.translate()
            refProteinSeq = refFa[gene[1]-1:gene[2]-1].seq.translate()
        elif strand == -1:
            proteinSeq = fa[gene[1]:gene[2]].seq.reverse_complement().translate()
            refProteinSeq = refFa[gene[1]:gene[2]].seq.reverse_complement().translate()
        elif strand ==0: #DNA sequence
            base = fa[gene[1]]
        
        for site in knownSNPs[abx][gene[0]]:
            if abs(strand)==1: 
                aa = proteinSeq[site-1]
            else:
                aa = base
                site = ''
            outputString = '%s\t%s\t%s\t%s\t%s\n'%(sampleName, abx, gene[0], site, aa)
            if DEBUG: sys.stdout.write(outputString)
            o.write(outputString)
    
    o.close()

basesN=['A', 'C', 'G', 'T']
perm = permutations(basesN,2)
N,perms=0,{}
for p in perm:
    perms.setdefault(p[0],{}).setdefault(p[1],N)
    N+=1

def totalReads(row):
    l=row['INFO'].split(';')
    return int(l[2].replace('TotalReads=',''))

def supportFraction(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[4].replace('SupportFraction=',''))
    else:
        return None

def BaseCalledReadsWithVariant(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[0].replace('BaseCalledReadsWithVariant=',''))
    else:
        return None

def BaseCalledFraction(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[1].replace('BaseCalledFraction=',''))
    else:
        return None

def maskProbs(r):
    if r['preds']==False:
        return False
    elif r['preds'] == True and r['probs'] > 0.95:
        return True
    else:
        return False

def alphaBeta(row):
    if row['REF'] == 'A' and row['ALT'] == 'G':
        return 0
    elif row['REF'] == 'A' and row['ALT'] == 'T':
        return 1
    elif row['REF'] == 'A' and row['ALT'] == 'C':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'A':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'G':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'T':
        return 0
    elif row['REF'] == 'G' and row['ALT'] == 'A':
        return 0
    elif row['REF'] == 'G' and row['ALT'] == 'C':
        return 1
    elif row['REF'] == 'G' and row['ALT'] == 'T':
        return 1
    elif row['REF'] == 'T' and row['ALT'] == 'A':
        return 1
    elif row['REF'] == 'T' and row['ALT'] == 'C':
        return 0
    elif row['REF'] == 'T' and row['ALT'] == 'G':
        return 1

def addBaseChangeN(row):
    if len(row['REF']) > 1 or len(row['ALT']) > 1:
        return None 
    r=row['REF']
    a=row['ALT']
    p=perms[r][a]
    return p


def getRuns(fol):
    runs=os.listdir(fol)
#    runs=[run for run in runs if run.endswith('vcfs')]
    return runs

def readVCFs(fol,run):
    names=['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO','FORMAT','EXTRA']
    dfs=[]
    vcfs=os.listdir('{0}/{1}'.format(fol,run))
    vcfs=[vcf for vcf in vcfs if vcf.endswith('.vcf')]
    for vcf in vcfs:
        df=pd.read_csv('{0}/{1}/{2}'.format(fol,run,vcf),sep='\t',comment='#',names=names)
        dfs.append(df)
    df=pd.concat(dfs)
    df['ALT_len']=df.ALT.map(len)
    df['REF_len']=df.REF.map(len)
    df['INDEL length']=df['ALT_len'] -  df['REF_len']
    df=df[df['ALT_len']==1]
    df=df[df['REF_len']==1]
#    df['Total reads']=df.apply(totalReads,axis=1)
#    df['Support fraction']=df.apply(supportFraction,axis=1)
#    df['BaseCalledReadsWithVariant']=df.apply(BaseCalledReadsWithVariant,axis=1)
#    df['BaseCalledFraction']=df.apply(BaseCalledFraction,axis=1)
    df['5prime proximity']=df['POS']-df['POS'].shift(1)
    df['3prime proximity']=df['POS']-df['POS'].shift(-1)
    df['3prime proximity']=df['3prime proximity'].abs()
    df['proximty']=df[['5prime proximity','3prime proximity']].min(axis=1)
    df['alphabeta']=df.apply(alphaBeta,axis=1)
    df['baseChange']=df.apply(addBaseChangeN,axis=1)
    # add basecall rations from pysamstats file
    pysamFile='{0}/{1}/pysamfile.txt'.format(fol,run)
    psdf=pd.read_csv(pysamFile,sep='\t')
    df['CHROM']=df['CHROM'].map(str)
    df['POS']=df['POS'].map(int)
    psdf['chrom']=psdf['chrom'].map(str)
    psdf['pos']=psdf['pos'].map(int)
    df=df.merge(psdf,left_on=['CHROM','POS'],right_on=['chrom','pos'],how='left')
    bases=['A','T','C','G','insertions','deletions']
    for b in bases:
        df['{0} %'.format(b)]=(df[b]/df['reads_all'])*100
    df['reads_all']=df[['A','T','C','G']].sum(axis=1)
    df['top_base'] = df[['A','T','C','G']].max(axis=1)
    df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
    df['majority base %'] = (df['top_base'] / df['reads_all'])
    df['Top Base matches Nanopolish'] = np.where(df.ALT == df.top_base_seq,1,0)
    df=df[df['majority base %'].notna()]

    return df

def classify(df,model):
    feature_combinations={
            'composite':['QUAL','reads_all','proximty','baseChange','majority base %','Top Base matches Nanopolish','deletions %','insertions %']
        }

    features=feature_combinations['composite']
    X=np.array(df[features])
    preds = model.predict(X)
    probs = model.predict_proba(X)
    probs=pd.DataFrame(probs,columns=[True,False])
    p=probs.max(axis=1)
    df['preds']=preds
    keep=df[df.preds==True]
    s=set(keep.POS)
    return s

def loadModel(modelFile):
    model = pickle.load(open(modelFile, 'rb'))
    return model

def filterVCFs(fol,run,outfol,keep):
    vcfs=os.listdir('{0}/{1}'.format(fol,run))
    vcfs=[vcf for vcf in vcfs if vcf.endswith('.vcf')]
    outfol='{0}/{1}'.format(outfol,run)
    if not os.path.exists(outfol):
        os.makedirs(outfol)
    for v in vcfs:
        vcf_reader = vcf.Reader(open('{0}/{1}/{2}'.format(fol,run,v), 'r'))
        vcf_writer = vcf.Writer(open('{0}/{1}'.format(outfol,v), 'w'), vcf_reader)
        for record in vcf_reader:
            if record.POS not in keep: continue
            elif record.POS in keep:
                vcf_writer.write_record(record)
            else:
                print('This is not right!')
                sys.exit()
    f = open('{0}/{1}'.format(outfol,v), 'rt')
    contents = f.readlines()
    f.close()

    contents.insert(1, '##nanopolish_window=R00000419:0-2500000\n')

    f = open('{0}/{1}'.format(outfol,v), 'wt')
    contents = "".join(contents)
    f.write(contents)
    f.close()


def get23s(df,outfol,run):
    positions=[1725642,1621743,1264255,1958532]
    df=df[df.POS.isin(positions)]
    df=df[['CHROM','POS','ID','REF','ALT','QUAL']]
    print(df)
    df.to_csv('{0}/{1}/23s.csv'.format(outfol,run))

def mtrR_prom(df,outfol,run):
    positions=[1332810]
    df=df[df.POS.isin(positions)]
    df=df[['CHROM','POS','ID','REF','ALT','QUAL']]
    print(df)
    df.to_csv('{0}/{1}/mtrR_prom.csv'.format(outfol,run))

def vcf2fasta(outfol,run,ref):
    c="nanopolish vcf2fasta -g {0} {1}/{2}/*.vcf".format(ref, outfol,run)
    with open("{0}/{1}/genome.fa".format(outfol,run),'wt') as outf:
        subprocess.run(c, stdout=outf,shell=True)


def run(opts):
    model=loadModel(opts.model)
    runs=getRuns(opts.inVCF)
    for run in runs:
        print(run)
        df=readVCFs(opts.inVCF,run)


        # SNPs
        SNPs=df[df['INDEL length']==0]
        keep=classify(SNPs,model)
        filterVCFs(opts.inVCF, run, opts.outVCF, keep)
        get23s(SNPs,opts.outVCF,run)
        vcf2fasta(opts.outVCF,run,opts.ref)
        inf="{0}/{1}/genome.fa".format(opts.outVCF,run)
        outf="{0}/{1}/refgenes.txt".format(opts.outVCF,run)
        getRefVariants(outf,inf,opts.ref,run)

        # indels
        indels=df[df['INDEL length']!=0]
        mtrR_prom(df,opts.outVCF,run)


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='Filter VCF file using pre trained model file')
    parser.add_argument('-i', '--inVCF', required=True,
                             help='Input VCF file')
    parser.add_argument('-o', '--outVCF', required=True,
                             help='Output VCF file')
    parser.add_argument('-m', '--model', required=True,
                             help='model file')
    parser.add_argument('-r', '--ref', required=True,
                             help='reference fasta file used')
    opts, unknown_args = parser.parse_known_args()
    run(opts)


