#!/usr/bin/env python3
import sys
import pandas as pd
import os
from Bio import SeqIO
import seaborn as sns
import matplotlib.pyplot as plt

# get VCfs into dataframe
def readFile(f):
    df=pd.read_csv(f,sep='\t',skiprows=8)
    df['Sample']=f.split('/')[-1].split('_')[0]
    return df

VCFs=os.listdir(sys.argv[1])
dfs=[]
for VCF in VCFs:
    f=sys.argv[1] + '/' + VCF
    dfs.append(readFile(f))

df=pd.concat(dfs)


# add reference information
seqs={}
with open(sys.argv[2],'rt') as inf:
    for seq in SeqIO.parse(inf,'fasta'):
        seqs[str(seq.id)]=str(seq.seq)

def checkRef(row):
    chrom=str(row['#CHROM'])
    pos  =int(row['POS']) -1 
    ref_base=seqs[chrom][pos]
    if ref_base==row['ALT'][0]:
        return True
    else:
        return False

def illuminaCall(row):
    chrom=str(row['#CHROM'])
    pos  =int(row['POS']) -1
    ref_base=seqs[chrom][pos]
    return ref_base

def maskSame(row):
    if row['SNP validation'] == True:
        return 'same'
    else:
        return row['ONT -> Illumina']

def totalReads(row):
    l=row['INFO'].split(';')
    return int(l[2].replace('TotalReads=',''))

def supportFraction(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[4].replace('SupportFraction=',''))
    else:
        return None

def BaseCalledReadsWithVariant(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[0].replace('BaseCalledReadsWithVariant=',''))
    else:
        return None

def BaseCalledFraction(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[1].replace('BaseCalledFraction=',''))
    else:
        return None


df['SNP validation']=df.apply(checkRef,axis=1)
df['ALT_len']=df.ALT.map(len)
df['REF_len']=df.REF.map(len)
df['Illumina base']=df.apply(illuminaCall,axis=1)
df['ONT -> Illumina']=df['ALT'] + '->' + df['Illumina base']
df['ONT -> Illumina']=df.apply(maskSame,axis=1)
df['INDEL length']=df['ALT_len'] -  df['REF_len']
df['Total reads']=df.apply(totalReads,axis=1)
df['Support fraction']=df.apply(supportFraction,axis=1)
df['BaseCalledReadsWithVariant']=df.apply(BaseCalledReadsWithVariant,axis=1)
df['BaseCalledFraction']=df.apply(BaseCalledFraction,axis=1)
# add Illumina INDEL info from VCFs files
names=['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO','FORMAT','EXTRA']
#Illumina_indels=pd.read_csv(sys.argv[3],sep='\t',comment='#',names=names)
#Illumina_indels=Illumina_indels[Illumina_indels.FILTER == 'PASS']
#print(Illumina_indels)


#df.to_csv('H18-208_SNPs.csv',index=False)
df.to_csv('{}_SNPs.csv'.format(sys.argv[3]),index=False)
# plot indel length
#g=sns.swarmplot('INDEL length','QUAL',data=df)
#plt.yscale('log')
#plt.savefig('indel_lengths_qual.pdf')
#plt.show()
#plt.clf()

# filtering to only substitutions
df=df[df.ALT_len == 1]
df=df[df.REF_len == 1]
df=df[df['Illumina base'] != 'N']

print(df)

def  vis(df):
    #g=sns.swarmplot('SNP validation','QUAL',data=df)
    g=sns.catplot('SNP validation','QUAL',data=df,kind='violin',col='Sample')
    #plt.yscale('log')
#    plt.savefig('SNP_validated_qualts.pdf')
    plt.show()
    plt.clf()
    
    g=sns.scatterplot('Total reads','QUAL',data=df,hue='SNP validation')
    plt.show()
#    plt.savefig('QUAL_over_reads.pdf')
    plt.clf()
    g=sns.scatterplot('Support fraction','QUAL',data=df,hue='SNP validation')
#    plt.savefig('QUAL_over_support_fraction.pdf')
    plt.show()

#vis(df)
