
Channel
        .fromPath( "${params.inFiles}" )
        .splitCsv()
        .map{ row -> tuple(row[0], row[1], row[2], row[3]) }
        .view()
        .into{ inputFiles }

minFreq_ch = Channel.from( 50, 60, 70, 75, 80, 85, 90, 95 )


process makeConsensus {
    tag { sample + ' ' + minFreq  }

    input:
    set val(sample),val(ont),val(ref),val(illumina),val(minFreq) from inputFiles.combine(minFreq_ch)

    output:
    set val(sample),val(minFreq),file("${sample}_${minFreq}.fasta"),val(illumina) into cons_ch

    script:
    scriptBase=params.base + '/scripts'
    """
    python3 ${scriptBase}/callPysamStats.py -F ${minFreq} -b ${ont} -r ${ref} -f ${sample}_${minFreq}.fasta
    """
}

process DNAdiff {
    tag { sample + ' ' + minFreq  }

    publishDir 'diffs', overwrite: true, mode: 'copy'

    input:
    set val(sample),val(minFreq),file("${sample}_${minFreq}.fasta"),val(illumina) from cons_ch

    output:
    file("${sample}_${minFreq}*") into diff_ch
    
    script:
    """
    dnadiff ${illumina} ${sample}_${minFreq}.fasta -p ${sample}_${minFreq}
    """
}
