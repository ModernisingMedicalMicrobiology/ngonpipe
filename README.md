# ngonpipe

Neisseria gonorrhoeae ONT Nextflow pipeline (NGONpipe).

This workflow is for the analysis of Neisseria gonorrhoeae genomes using Oxford Nanopore sequenced (demultiplexed fastq files) direct from metagemomic samples. It also works on cultured isolates sequenced by ONT. The workflow requires many dependencies that have been packaged into a docker container. It requires a seperate container for Clair that only works with conda. The workflow also requires a centrifuge database including at least one Neisseria gonorrhoeae genome. 

## run
Some experience with command line and nextflow will be necassary to run this workflow.

The workflow requres a csv file of the sample ids and location of fastq files. 

For example the contents of a runFiles.csv would look like this. 

| Sample name | path to fastq files |
| ---------- | -------------------------------------------------------------------- |
| 206_UB_UBS | /data/206_UB_UBS/206_UB_UBS/fastq_pass/barcode01/  |
| 207_UB_UBS | /data/206_UB_UBS/206_UB_UBS/fastq_pass/barcode02/  |

### running localy/natively
To run the workflow locally if you have all the software componants installed. You will need a centrifuge index (download from https://ccb.jhu.edu/software/centrifuge/) and the clair model file for ont (https://github.com/HKU-BAL/Clair).
```
git clone https://gitlab.com/ModernisingMedicalMicrobiology/ngonpipe

nextflow run <PATH TO>/ngonpipe/main.nf \
        --infastq runFiles.csv \
        --centdb ~/dbs/centrifuge/centrifuge_2018-08-10/centrifuge_2018-08-10 \
        -profile local
```

### running with Docker
You will need docker installed. This isn't tested on macs. You will need a centrifuge index (download from https://ccb.jhu.edu/software/centrifuge/) 

```
nextflow run https://gitlab.com/ModernisingMedicalMicrobiology/ngonpipe/main.nf \
        --infastq runFiles.csv \
        --centdb ~/dbs/centrifuge/centrifuge_2018-08-10/centrifuge_2018-08-10 \
        --CLAIRMODEL /opt/clair/ont/model \
        -profile docker
```


### running with conda
The clair componant of the conda environments prevents using the environment.yml file natively with nextflow. So the environment needs to be built and a few command run before it can work.

``` 
#Build the environment
conda env create -f /home/nick/soft/ngonpipe/environment.yml
conda activate ngonpipe
pypy3 -m ensurepip
pypy3 -m pip install --no-cache-dir intervaltree==3.0.2

```
Then each time the workflow is run the conda environment doesn't need to be rebuilt.
```
# run in conda enviroment
conda activate ngonpipe
base='/home/nick/soft/ngonpipe'
nextflow run ${base}/main.nf \
        --infastq runFiles.csv \
        --centdb ~/dbs/centrifuge/centrifuge_2018-08-10/centrifuge_2018-08-10 \
        --CLAIRMODEL <PATH TO>/Clair/ont/model \
        -local
```

## Results folders
Results are published from the workflow into a few different output folders.

### genome variants (refVariants/)
Chromosomal gene variations from within mtrR, pilQ, ponA, gyrA, parC, parE, rpsJ, and 23S rRNA are reported within the refVariants/ folder. The csv file contains the sample name, gene, start and end on the chromosome, the amino acid or nucleotide postion within the gene protein or nucleotide sequence. The wild type base/AA (ref), the potential alternate base (alt), the reference amino acid, and the 'amino acid' or 'base'  determined during the analysis. The antibiotics affected (abx), the mean depth over the gene and the min depth over the gene.

| Sample     | gene           |     start |       end |   pos | ref | alt | ref amino acid | amino acid | ref base | base | abx             | mean depth | min depth |
| ---------- | -------------- | --------- | --------- | ----- | --- | --- | -------------- | ---------- | -------- | ---- | --------------- | ---------- | --------- |
| 202_UB_UBS | penB           | 2,049,351 | 2,050,397 |   120 | G   | K   | K              | G          |          |      | cro cfx         |    32.752… |        30 |
| 202_UB_UBS | penB           | 2,049,351 | 2,050,397 |   121 | A   | D   | G              | A          |          |      | cro cfx         |    32.752… |        30 |
| 202_UB_UBS | mtrR           | 1,332,867 | 1,333,499 |    45 | G   | D   | G              | G          |          |      | cro pen tet azt |     7.227… |         5 |
| 202_UB_UBS | mtrR           | 1,332,867 | 1,333,499 |    39 | A   | T   | A              | A          |          |      | cro pen tet azt |     7.227… |         5 |
| 202_UB_UBS | pilQ           |   103,068 |   105,263 |   666 | E   | K   | E              | E          |          |      | cro cfx pen tet |    24.942… |        15 |
| 202_UB_UBS | ponA           |   108,362 |   110,758 |   421 | L   | P   | P              | P          |          |      | cro cfx pen     |    45.708… |        37 |
| 202_UB_UBS | gyrA           | 1,051,396 | 1,054,146 |    91 | S   | F   | F              | S          |          |      | cip             |    27.749… |        23 |
| 202_UB_UBS | gyrA           | 1,051,396 | 1,054,146 |    95 | D   | N   | G              | D          |          |      | cip             |    27.749… |        23 |
| 202_UB_UBS | gyrA           | 1,051,396 | 1,054,146 |    95 | D   | G   | G              | D          |          |      | cip             |    27.749… |        23 |
| 202_UB_UBS | gyrB           | 2,098,152 | 2,100,542 |       |     |     |                |            |          |      |                 |    13.429… |        10 |
| 202_UB_UBS | parC           |   193,663 |   195,966 |       |     |     |                |            |          |      |                 |    22.064… |        19 |
| 202_UB_UBS | parE           | 1,296,483 | 1,298,468 |       |     |     |                |            |          |      |                 |     9.198… |         5 |
| 202_UB_UBS | norM_promoter  |   460,253 |   460,253 |       |     |     |                |            |          |      |                 |    34.000… |        34 |
| 202_UB_UBS | rpsJ           | 2,031,311 | 2,031,622 |    57 | V   | M   | M              | V          |          |      | tet             |    22.327… |        21 |
| 202_UB_UBS | macAB_promoter | 1,415,365 | 1,415,365 |       |     |     |                |            |          |      |                 |    12.000… |        12 |
| 202_UB_UBS | 23S            | 1,263,410 | 1,266,299 | 2,045 | A   | G   |                |            | A        | A    | azt             |     7.594… |         4 |
| 202_UB_UBS | 23S            | 1,263,410 | 1,266,299 | 2,597 | C   | T   |                |            | C        | C    | azt             |     7.594… |         4 |
| 202_UB_UBS | 23S            | 1,956,488 | 1,959,377 | 2,045 | A   | G   |                |            | A        | A    | azt             |    11.719… |         7 |
| 202_UB_UBS | 23S            | 1,956,488 | 1,959,377 | 2,597 | C   | T   |                |            | C        | C    | azt             |    11.719… |         7 |
| 202_UB_UBS | 23S            | 1,724,797 | 1,727,686 | 2,045 | A   | G   |                |            | A        | A    | azt             |     9.544… |         4 |
| 202_UB_UBS | 23S            | 1,724,797 | 1,727,686 | 2,597 | C   | T   |                |            | C        | C    | azt             |     9.544… |         4 |
| 202_UB_UBS | 23S            | 1,620,898 | 1,623,787 | 2,045 | A   | G   |                |            | A        | A    | azt             |    14.696… |        10 |
| 202_UB_UBS | 23S            | 1,620,898 | 1,623,787 | 2,597 | C   | T   |                |            | C        | C    | azt             |    14.696… |        10 |
| 206_UB_UBS | penB           | 2,049,351 | 2,050,397 |   120 | G   | K   | K              | G          |          |      | cro cfx         |    45.297… |        40 |
| 206_UB_UBS | penB           | 2,049,351 | 2,050,397 |   121 | A   | D   | G              | A          |          |      | cro cfx         |    45.297… |        40 |
| 206_UB_UBS | mtrR           | 1,332,867 | 1,333,499 |    45 | G   | D   | G              | G          |          |      | cro pen tet azt |     9.365… |         5 |
| 206_UB_UBS | mtrR           | 1,332,867 | 1,333,499 |    39 | A   | T   | A              | T          |          |      | cro pen tet azt |     9.365… |         5 |
| 206_UB_UBS | pilQ           |   103,068 |   105,263 |   666 | E   | K   | E              | E          |          |      | cro cfx pen tet |    41.903… |        35 |
| 206_UB_UBS | ponA           |   108,362 |   110,758 |   421 | L   | P   | P              | L          |          |      | cro cfx pen     |    56.987… |        47 |
| 206_UB_UBS | gyrA           | 1,051,396 | 1,054,146 |    91 | S   | F   | F              | F          |          |      | cip             |    36.552… |        20 |
| 206_UB_UBS | gyrA           | 1,051,396 | 1,054,146 |    95 | D   | N   | G              | A          |          |      | cip             |    36.552… |        20 |
| 206_UB_UBS | gyrA           | 1,051,396 | 1,054,146 |    95 | D   | G   | G              | A          |          |      | cip             |    36.552… |        20 |
| 206_UB_UBS | gyrB           | 2,098,152 | 2,100,542 |       |     |     |                |            |          |      |                 |    30.046… |        26 |
| 206_UB_UBS | parC           |   193,663 |   195,966 |       |     |     |                |            |          |      |                 |    40.737… |        37 |
| 206_UB_UBS | parE           | 1,296,483 | 1,298,468 |       |     |     |                |            |          |      |                 |    31.431… |        27 |
| 206_UB_UBS | norM_promoter  |   460,253 |   460,253 |       |     |     |                |            |          |      |                 |    59.000… |        59 |
| 206_UB_UBS | rpsJ           | 2,031,311 | 2,031,622 |    57 | V   | M   | M              | M          |          |      | tet             |    80.750… |        76 |
| 206_UB_UBS | macAB_promoter | 1,415,365 | 1,415,365 |       |     |     |                |            |          |      |                 |    33.000… |        33 |
| 206_UB_UBS | 23S            | 1,263,410 | 1,266,299 | 2,045 | A   | G   |                |            | A        | A    | azt             |    23.813… |        18 |
| 206_UB_UBS | 23S            | 1,263,410 | 1,266,299 | 2,597 | C   | T   |                |            | C        | C    | azt             |    23.813… |        18 |
| 206_UB_UBS | 23S            | 1,956,488 | 1,959,377 | 2,045 | A   | G   |                |            | A        | A    | azt             |    24.311… |        17 |
| 206_UB_UBS | 23S            | 1,956,488 | 1,959,377 | 2,597 | C   | T   |                |            | C        | C    | azt             |    24.311… |        17 |
| 206_UB_UBS | 23S            | 1,724,797 | 1,727,686 | 2,045 | A   | G   |                |            | A        | A    | azt             |    19.037… |        10 |
| 206_UB_UBS | 23S            | 1,724,797 | 1,727,686 | 2,597 | C   | T   |                |            | C        | C    | azt             |    19.037… |        10 |
| 206_UB_UBS | 23S            | 1,620,898 | 1,623,787 | 2,045 | A   | G   |                |            | A        | A    | azt             |    32.680… |        20 |
| 206_UB_UBS | 23S            | 1,620,898 | 1,623,787 | 2,597 | C   | T   |                |            | C        | C    | azt             |    32.680… |        20 |


### mtrR promotor (mtrProm/)
The tab seperated file contains the chromosome reference for our GC genome (R00000419). The position of the insertion ( the reference used has the mutant deletion ). The number of reads and percentage of ACTG at the position.

| Chrom     |       pos | Reads |       %A | %T | %C |     %G | strain      |
| --------- | --------- | ----- | -------- | -- | -- | ------ | ----------- |
| R00000419 | 1,332,809 |     3 |   0.000… |  0 |  0 | 0.000… | 202_UB_UBS  |
| R00000419 | 1,332,809 |     6 |  50.000… |  0 |  0 | 0.000… | 206_UB_UBS  |
| R00000419 | 1,332,809 |    99 |   6.061… |  0 |  0 | 0.000… | 250_UB_UBS  |
| R00000419 | 1,332,809 |    45 |  53.333… |  0 |  0 | 0.000… | 271_UB_UB-S |
| R00000419 | 1,332,809 |    31 |  58.065… |  0 |  0 | 0.000… | 294_UB_UB-S |
| R00000419 | 1,332,809 |    85 |   5.882… |  0 |  0 | 0.000… | 301_UB_UBS  |
| R00000419 | 1,332,809 |     8 |  37.500… |  0 |  0 | 0.000… | 303_UB_UBS  |
| R00000419 | 1,332,809 |     2 | 100.000… |  0 |  0 | 0.000… | 304_UB_UBS  |
| R00000419 | 1,332,809 |   108 |   5.556… |  0 |  0 | 0.000… | 314_UB_UBS  |
| R00000419 | 1,332,809 |   151 |  49.007… |  0 |  0 | 0.662… | 315_UB_UBS  |



### penA (penA/)
There are two output `*`._mapBinGenesRemapGene_penA_results.csv are from local assemblies, and `*`_WGA_genesRemapGene_penA_results.csv are fromt the whole genome assemblies. The csv files contain the following fields:
 sample_name,  allele, Allele Type,  AMR Markers, Beta-Lactamase, score, bits, muts, matchLength, oper, sub, qseqid, sseqid,  pident, length, mismatch, gapopen, qstart, qend, sstart, send, evalue, bitscore 

| sample_name | allele      | Allele Type | AMR Markers                   | Beta-Lactamase | score |     bits | muts | matchLength | oper                 | sub  | qseqid      | sseqid      |   pident | length | mismatch | gapopen | qstart |  qend | sstart |  send | evalue | bitscore |
| ----------- | ----------- | ----------- | ----------------------------- | -------------- | ----- | -------- | ---- | ----------- | -------------------- | ---- | ----------- | ----------- | -------- | ------ | -------- | ------- | ------ | ----- | ------ | ----- | ------ | -------- |
| 202_UB_UBS  | penA2.001   |       2.001 | penA Type II NonMosaic        | Negative       | 3,084 | 1,192.56 |      |         582 | mapBinGenesRemapGene | penA | penA2.001   | penA2.001   | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 202_UB_UBS  | penA2.001   |       2.001 | penA Type II NonMosaic        | Negative       | 3,084 | 1,192.56 |      |         582 | WGA_genesRemapGene   | penA | penA2.001   | penA2.001   | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 206_UB_UBS  | penA43.002  |      43.002 | penA Type 43 NonMosaic; A502V | Positive       | 3,083 | 1,192.18 |      |         582 | mapBinGenesRemapGene | penA | penA43.002  | penA43.002  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 206_UB_UBS  | penA43.002  |      43.002 | penA Type 43 NonMosaic; A502V | Positive       | 3,083 | 1,192.18 |      |         582 | WGA_genesRemapGene   | penA | penA43.002  | penA43.002  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 250_UB_UBS  | penA2.001   |       2.001 | penA Type II NonMosaic        | Negative       | 3,084 | 1,192.56 |      |         582 | mapBinGenesRemapGene | penA | penA2.001   | penA2.001   | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 250_UB_UBS  | penA2.001   |       2.001 | penA Type II NonMosaic        | Negative       | 3,084 | 1,192.56 |      |         582 | WGA_genesRemapGene   | penA | penA2.001   | penA2.001   | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 271_C_CB    | penA43.002  |      43.002 | penA Type 43 NonMosaic; A502V | Positive       | 3,083 | 1,192.18 |      |         582 | mapBinGenesRemapGene | penA | penA43.002  | penA43.002  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 271_D       | penA43.002  |      43.002 | penA Type 43 NonMosaic; A502V | Positive       | 3,083 | 1,192.18 |      |         582 | mapBinGenesRemapGene | penA | penA43.002  | penA43.002  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 271_D       | penA43.002  |      43.002 | penA Type 43 NonMosaic; A502V | Positive       | 3,083 | 1,192.18 |      |         582 | WGA_genesRemapGene   | penA | penA43.002  | penA43.002  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 271_UB_UB-S | penA43.002  |      43.002 | penA Type 43 NonMosaic; A502V | Positive       | 3,083 | 1,192.18 |      |         582 | mapBinGenesRemapGene | penA | penA43.002  | penA43.002  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 294_UB_UB-S | penA43.002  |      43.002 | penA Type 43 NonMosaic; A502V | Positive       | 3,083 | 1,192.18 |      |         582 | mapBinGenesRemapGene | penA | penA43.002  | penA43.002  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 294_UB_UB-S | penA43.002  |      43.002 | penA Type 43 NonMosaic; A502V | Positive       | 3,083 | 1,192.18 |      |         582 | WGA_genesRemapGene   | penA | penA43.002  | penA43.002  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 301_C_CB    | penA106.001 |     106.001 | penA Type 106 NonMosaic       | Negative       | 3,092 | 1,195.65 |      |         583 | WGA_genesRemapGene   | penA | penA106.001 | penA106.001 |  99.886… |  1,752 |        2 |       0 |      1 | 1,752 |      1 | 1,752 |      0 |    3,225 |
| 301_D       | penA5.002   |       5.002 | penA Type V NonMosaic         | Negative       | 3,092 | 1,195.65 |      |         583 | mapBinGenesRemapGene | penA | penA5.002   | penA5.002   | 100.000… |  1,752 |        0 |       0 |      1 | 1,752 |      1 | 1,752 |      0 |    3,236 |
| 301_D       | penA5.002   |       5.002 | penA Type V NonMosaic         | Negative       | 3,092 | 1,195.65 |      |         583 | WGA_genesRemapGene   | penA | penA5.002   | penA5.002   | 100.000… |  1,752 |        0 |       0 |      1 | 1,752 |      1 | 1,752 |      0 |    3,236 |
| 301_UB_UBS  | penA5.002   |       5.002 | penA Type V NonMosaic         | Negative       | 3,092 | 1,195.65 |      |         583 | mapBinGenesRemapGene | penA | penA5.002   | penA5.002   | 100.000… |  1,752 |        0 |       0 |      1 | 1,752 |      1 | 1,752 |      0 |    3,236 |
| 301_UB_UBS  | penA5.002   |       5.002 | penA Type V NonMosaic         | Negative       | 3,092 | 1,195.65 |      |         583 | WGA_genesRemapGene   | penA | penA5.002   | penA5.002   | 100.000… |  1,752 |        0 |       0 |      1 | 1,752 |      1 | 1,752 |      0 |    3,236 |
| 304_UB_UBS  | penA14.001  |      14.001 | penA Type XIV NonMosaic       | Negative       | 3,082 | 1,191.79 |      |         582 | mapBinGenesRemapGene | penA | penA14.001  | penA14.001  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 304_UB_UBS  | penA14.001  |      14.001 | penA Type XIV NonMosaic       | Negative       | 3,082 | 1,191.79 |      |         582 | WGA_genesRemapGene   | penA | penA14.001  | penA14.001  | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 314_UB_UBS  | penA2.001   |       2.001 | penA Type II NonMosaic        | Negative       | 3,084 | 1,192.56 |      |         582 | mapBinGenesRemapGene | penA | penA2.001   | penA2.001   | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 314_UB_UBS  | penA2.001   |       2.001 | penA Type II NonMosaic        | Negative       | 3,084 | 1,192.56 |      |         582 | WGA_genesRemapGene   | penA | penA2.001   | penA2.001   | 100.000… |  1,749 |        0 |       0 |      1 | 1,749 |      1 | 1,749 |      0 |    3,230 |
| 315_UB_UBS  | penA5.002   |       5.002 | penA Type V NonMosaic         | Negative       | 3,092 | 1,195.65 |      |         583 | mapBinGenesRemapGene | penA | penA5.002   | penA5.002   | 100.000… |  1,752 |        0 |       0 |      1 | 1,752 |      1 | 1,752 |      0 |    3,236 |
| 315_UB_UBS  | penA5.002   |       5.002 | penA Type V NonMosaic         | Negative       | 3,092 | 1,195.65 |      |         583 | WGA_genesRemapGene   | penA | penA5.002   | penA5.002   | 100.000… |  1,752 |        0 |       0 |      1 | 1,752 |      1 | 1,752 |      0 |    3,236 |




### accessory genes tetM and blaTEM-1 (accessoryResults)
Within the accessoryResults folder the csv files contain the following fields:
Sample name (name of the sample), qseqid (contig name from local assemblies), sseqid_x (plasmid name from blastn of plasmid database), sseqid_y (gene name from accessrory gene blastn), Query prop (proportion of the contig query that matches the plasmid), pident (proportion of the contig query identity that matches the plasmid), Sub prop (proportion of the gene that matches the gene blastn subject), qlen ( contig length, i.e. how big was the assembled plasmid), slen (gene length, i.e. how long was the identifed gene.)

| Sample name | qseqid | sseqid_x   | sseqid_y | Query prop |  pident | Sub prop |   qlen |  slen |
| ----------- | ------ | ---------- | -------- | ---------- | ------- | -------- | ------ | ----- |
| 206_UB_UBS  | ctg1   | GU479466.1 | tetM     |     1.004… | 99.741… |   1.000… | 10,675 | 1,932 |
| 206_UB_UBS  | ctg1   | GU479464.1 | tetM     |     0.649… | 99.741… |   1.000… | 10,675 | 1,932 |
| 271_D       | ctg1   | GU479466.1 | tetM     |     1.003… | 99.793… |   1.000… | 11,049 | 1,932 |
| 271_D       | ctg1   | GU479464.1 | tetM     |     0.627… | 99.793… |   1.000… | 11,049 | 1,932 |
| 294_C_CB    | ctg1   | HM756641.1 | blaTEM-1 |     0.879… | 99.187… |   1.000… |  5,430 |   861 |
| 294_UB_UB-S | ctg1   | GU479466.1 | tetM     |     1.004… | 99.689… |   1.000… | 14,149 | 1,932 |
| 303_UB_UBS  | ctg2   | HM756641.1 | blaTEM-1 |     0.843… | 97.443… |   0.818… |  5,353 |   861 |
| 304_UB_UBS  | ctg1   | GU479464.1 | tetM     |     0.601… | 96.222… |   1.000… | 13,023 | 1,932 |
| 304_UB_UBS  | ctg1   | GU479466.1 | tetM     |     0.935… | 96.222… |   1.000… | 13,023 | 1,932 |



## Workflow diagram

![](images/dag.png)


