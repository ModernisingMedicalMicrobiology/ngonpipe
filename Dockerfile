# Use an official Python runtime as a parent image
FROM ubuntu:16.04

# Set the working directory to /app
WORKDIR /soft

# Copy the current directory contents into the container at /app
#ADD soft/ /soft

# Install any needed packages specified in requirements.txt
#RUN pip install -r requirements.txt

# Install software
RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential python3-setuptools wget python-dev parallel


RUN apt-get -y install git

# centrifuge
RUN git clone https://github.com/infphilo/centrifuge
WORKDIR /soft/centrifuge
RUN pwd
RUN make
RUN make install prefix=/usr/local

# minimap
WORKDIR /soft
RUN git clone https://github.com/lh3/minimap2
WORKDIR /soft/minimap2
RUN apt-get install libz-dev
RUN make
RUN cp minimap2 /usr/local/bin/

#porechop
# install forked porechop
WORKDIR /soft
RUN git clone https://github.com/nick297/Porechop.git
WORKDIR /soft/Porechop
RUN python3 setup.py install


# prinseq
WORKDIR /soft
RUN wget https://downloads.sourceforge.net/project/prinseq/standalone/prinseq-lite-0.20.4.tar.gz
RUN tar -xzf prinseq-lite-0.20.4.tar.gz
RUN chmod 755 /soft/prinseq-lite-0.20.4/prinseq-lite.pl
ENV PATH="/soft/prinseq-lite-0.20.4:${PATH}"

# script
WORKDIR /
#COPY crumpit/lib /soft/lib/
#COPY bash_scripts /soft/bash_scripts
#RUN mkdir /soft/crumpit
#COPY * /soft/CRuMPIT/
#WORKDIR /soft/
#RUN git clone https://gitlab.com/ModernisingMedicalMicrobiology/CRuMPIT.git -b mapbetter 
#WORKDIR /soft/CRuMPIT
#RUN python3 setup.py install
#RUN python3 setup.py develop
#RUN cp configs/crumpit.config /etc/
RUN apt-get install -y python-pip python-biopython vim bwa python-dateutil python3-biopython python3-pip jq
RUN pip3 install --upgrade pip
RUN pip3 install pymongo tqdm ete3 six scipy matplotlib seaborn h5py cigar bokeh 

# for centrifuge dbs
#RUN mkdir -p /home/nick/dbs

# update ncbi taxids
#RUN python3 /soft/CRuMPIT/crumpit/crumpitTools/updateNCBI.py


# guppy
#RUN apt-get update
#RUN wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | apt-key add -
#RUN echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | tee /etc/apt/sources.list.d/nanoporetech.sources.list 
#  
#RUN apt-get install apt-transport-https
#RUN apt-get update
#RUN DEBIAN_FRONTEND=noninteractive apt-get install -y ont-guppy
#
#RUN pip3 install ont-fastq-deconcatenate pandas>=0.23
#RUN pip3 install numpy>=1.12.0

# blast+
WORKDIR /soft
RUN wget ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.7.1/ncbi-blast-2.7.1+-x64-linux.tar.gz
RUN tar zxvpf ncbi-blast-2.7.1+-x64-linux.tar.gz
ENV PATH="/soft/ncbi-blast-2.7.1+/bin:${PATH}"

# filtlong
WORKDIR /soft
RUN git clone https://github.com/rrwick/Filtlong.git
WORKDIR /soft/Filtlong
RUN make -j
RUN cp bin/filtlong /usr/local/bin

# bedtools
RUN apt-get install bedtools

#RUN apt install samtools
# htslib
RUN apt-get install -y autoconf libbz2-dev  zlib1g-dev liblzma-dev hdf5-tools eigensoft
WORKDIR /soft
RUN git clone https://github.com/samtools/htslib.git
WORKDIR htslib
RUN autoheader     # If using configure, generate the header template...
RUN autoconf       # ...and configure script (or use autoreconf to do both)
RUN ./configure   # Optional, needed for choosing optional functionality
RUN make
RUN make install
#samtools git
RUN apt-get install -y libncurses5-dev
WORKDIR /soft
RUN git clone https://github.com/samtools/samtools.git
WORKDIR /soft/samtools
RUN autoheader
RUN autoconf -Wno-syntax
RUN ./configure
RUN make
RUN make install


# vcf
RUN pip3 install pyvcf 
RUN pip3 install pysam pysamstats
RUN pip3 install openpyxl scikit-learn
#nopolish
WORKDIR /soft
RUN git clone --recursive https://github.com/jts/nanopolish.git
WORKDIR /soft/nanopolish
RUN make 
#HTS=noinstall
ENV PATH="/soft/nanopolish/:${PATH}"

# clair
WORKDIR /soft
RUN git clone https://github.com/HKU-BAL/Clair.git
WORKDIR /soft/Clair
ENV PATH="/soft/Clair/:${PATH}"

# update ubuntu packages
RUN apt-get update --fix-missing && \
    yes|apt-get upgrade && \
    apt-get install -y \
        wget \
        bzip2 \
        make \
        gcc \
        vcftools  


# sklearn
RUN pip3 install numpy scipy scikit-learn PyVCF biopython pysam pysamstats

# sixpack
RUN apt-get install -y emboss

## assemblers
# canu
RUN apt install -y default-jre
RUN apt install -y gnuplot
WORKDIR /soft
RUN wget https://github.com/marbl/canu/releases/download/v1.8/canu-1.8.Linux-amd64.tar.xz
RUN tar -xJf canu-1.8.*.tar.xz
ENV PATH="/soft/canu-1.8/Linux-amd64/bin/:${PATH}"
# redbean
WORKDIR /soft
RUN git clone https://github.com/ruanjue/wtdbg2
WORKDIR /soft/wtdbg2
RUN make
ENV PATH="/soft/wtdbg2/:${PATH}"
# ra
WORKDIR /soft
RUN git clone --recursive https://github.com/rvaser/ra.git ra
WORKDIR /soft/ra
RUN mkdir build
WORKDIR /soft/ra/build
RUN apt install -y cmake
RUN cmake -DCMAKE_BUILD_TYPE=Release ..
RUN make
ENV PATH="/soft/ra/build/bin/:${PATH}"

##
RUN pip3 install xlrd

# runlistCompare
RUN pip2 install networkx==2.2 cogent
RUN apt install -y iqtree
WORKDIR /soft
RUN git clone https://github.com/xavierdidelot/ClonalFrameML
WORKDIR /soft/ClonalFrameML/src
RUN bash make.sh
ENV PATH="/soft/ClonalFrameML/src/:${PATH}"

# clair setup
ENV PATH="/soft/Clair/:${PATH}"
RUN chmod 755 /soft/Clair/clair
RUN chmod 755 /soft/Clair/clair.py
RUN apt install -y software-properties-common
RUN add-apt-repository ppa:pypy/ppa
RUN apt update
RUN apt install -y pypy3
RUN pip3 install intervaltree
RUN pip2 install intervaltree
RUN sed -i "1s/python/python3/" /soft/Clair/clair.py
RUN sed -i "s/python/python3/g" /soft/Clair/clair/callVarBam.py

# update ncbi taxids
ADD bin/ /soft/ngonpipe/bin
RUN python3 /soft/ngonpipe/bin/updateNCBI.py
