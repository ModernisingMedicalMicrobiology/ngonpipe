#!/usr/bin/env python3
import pysam
from argparse import ArgumentParser

def getPosInfo(bam, chrom, pos):
    samfile = pysam.AlignmentFile(bam, "rb" )
    n,b=0,[]
    for pileupcolumn in samfile.pileup(chrom, int(pos)-10000, int(pos)+10000):
        if pileupcolumn.pos != int(pos): continue
        for pileupread in pileupcolumn.pileups:
            n+=1
            if pileupread.indel != 1: continue
#            print(pileupread.indel,
#                    pileupread.query_position_or_next)

#           print(pileupread.alignment.query_sequence[pileupread.query_position], 
#                   pileupread.alignment.query_sequence[pileupread.query_position+1])
            b.append(pileupread.alignment.query_sequence[pileupread.query_position+1])
    
    samfile.close()
    return n,b


def run(opts):
    reads,bases=getPosInfo(opts.bam,opts.chrom,opts.pos)
    if reads > 0:
        A=bases.count('A')
        AP=(A/reads)*100
        T=bases.count('T')
        TP=(T/reads)*100
        C=bases.count('C')
        CP=(C/reads)*100
        G=bases.count('G')
        GP=(G/reads)*100
    else:
        AP, TP, CP, GP = 0, 0, 0, 0 
    print('Chrom', 'pos', 'Reads','%A','%T','%C','%G','strain','model', sep='\t')
    print(opts.chrom, opts.pos, reads, AP, TP, CP, GP , opts.strain, opts.model, sep='\t')


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='determine positional INDEL in bam file')
    parser.add_argument('-b', '--bam', required=True,
                             help='Input bam file')
    parser.add_argument('-c', '--chrom', required=True,
                             help='chrom name')
    parser.add_argument('-p', '--pos', required=True,
                             help='position')
    parser.add_argument('-s', '--strain', required=False,default=None,
                             help='strain information')
    parser.add_argument('-m', '--model', required=False,default=None,
                             help='model information')
    opts, unknown_args = parser.parse_known_args()
    run(opts)
