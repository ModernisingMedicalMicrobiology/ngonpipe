#!/usr/bin/env python3
import sys
import pandas as pd
import numpy as np
from argparse import ArgumentParser


def refLogic(r):
    if pd.isna(r['ref amino acid']) == False:
        if r['min depth'] < 2:
            return 'F'
        elif r['amino acid'] == r['alt']:
            return 'R'
        elif r['amino acid'] == r['ref']:
            return 'S'
        else: 
            return 'U'
    if pd.isna(r['ref base']) == False:
        if r['min depth'] < 2:
            return 'F'
        if r['base'] == r['alt']:
            return 'R'
        if r['base'] == r['ref']:
            return 'S'
        else: 
            return 'U'
    return 'W'

def penALogic(r):
    if 'NonMosaic' in r['AMR Markers'].replace(';','').split(' '):
        return 'S'
    elif 'mosaic' in r['AMR Markers'].replace(';','').split(' '):
        return 'R'
    else:
        return 'U'

def mtrRLogic(r):
    if r['Reads'] >= 5:
        if r['%A'] <= 40:
            return 'R'
        elif r['%A'] > 40:
            return 'S'
    else:
        return 'U'


def phenotype(refV, penAV, mtrRV):

    # load text files
    refs=pd.read_csv(refV)
    penA=pd.read_csv(penAV)
    mtrR=pd.read_csv(mtrRV, sep='\t')

    # interpret ref variants
    refs['phenotype']=refs.apply(refLogic,axis=1)

    penA['abx']= 'cfx cro'
    penA['phenotype']=penA.apply(penALogic, axis=1)
    penA['gene']='penA'

    mtrR['abx']='azm cfx'
    mtrR['phenotype']=mtrR.apply(mtrRLogic, axis=1)
    mtrR['gene']='mtrR_prom'

    # combine and calculate

    dfs=[]
    dfs.append(refs[['gene', 'pos','abx','phenotype']])
    dfs.append(penA[['gene','abx','phenotype']])
    dfs.append(mtrR[['gene', 'pos','abx','phenotype']])
    df=pd.concat(dfs, sort=True)
    df2=df['abx'].str.split(' ', expand=True)
    df=df.merge(df2, left_index=True, right_index=True)
    df=df.melt(id_vars=['gene', 'pos','abx','phenotype'])
    df=df[pd.isnull(df['value'])==False]
    df.rename(columns={'value': 'ABX', 'variable': 'abx num'}, inplace=True)
    print(df)
    g=df.groupby(['ABX','phenotype']).count()
    print(g)


if __name__ == "__main__":
    parser = ArgumentParser(description='predict resistance and sensitivities from ngonpipe')
    parser.add_argument('-r', '--refVariants', required=True,
            help='')
    parser.add_argument('-p', '--penAVariants', required=True,
            help='')
    parser.add_argument('-m', '--mtrRVariants', required=True,
            help='')
    opts, unknown_args = parser.parse_known_args()

    phenotype(opts.refVariants,
            opts.penAVariants,
            opts.mtrRVariants)



