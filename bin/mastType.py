#!/usr/bin/env python3
import sys
import pandas as pd

def loadSTs(f):
    names=['ST','POR','TBPB']
    df=pd.read_csv(f,dtype=str,names=names)
    return df

def replaceStr(r):
    return r.geneAlle_x.replace(r.gene_x,'')

def getGene(f):
    df=pd.read_csv(f,dtype=str)
    df['allele']=df.apply(replaceStr,axis=1)
    return df.allele[0]

def getST(df,POR,TBPB):
    df=df[df.POR == str(POR)]
    df=df[df.TBPB == str(TBPB)]
    if len(df)==0:
        d={}
        d['ST']=None
        d['TBPB'] = str(TBPB)
        d['POR'] = str(POR)
        print(d)
        df2=pd.DataFrame([d])
        df2=df2[['ST','POR','TBPB']]
        return df2
    else:
        return df

STs=loadSTs(sys.argv[1])
POR=getGene(sys.argv[2])
TBPB=getGene(sys.argv[3])
print(POR)
print(TBPB)
ST=getST(STs,POR,TBPB)
ST['Sample']=sys.argv[4]
ST=ST[['Sample','ST','POR','TBPB']]
print(ST)

ST.to_csv('{0}_mast_type.csv'.format(sys.argv[4]),index=False)
