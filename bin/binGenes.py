#!/usr/bin/env python3
import sys
import pysam
import pandas as pd
from argparse import ArgumentParser
from Bio import SeqIO
import gzip

def getMeta(mp):
    df=pd.read_csv('{0}/all_metadata.csv'.format(mp))
    df=df[['geneAlle','gene']]
    d=df.set_index('geneAlle').to_dict()
    return d

def getReadBins(bam,md):
    samfile = pysam.AlignmentFile(bam, "rb")
    bins={}
    for read in samfile.fetch():
        b=md['gene'][read.reference_name]
        bins.setdefault(b,[]).append(read.query_name)
    return bins

def _fqIter(fq):
    for seq in SeqIO.parse(gzip.open(fq,'rt'),'fastq'):
        yield seq

def binSeqs(bins,fastq,limit):
    invertBins={}
    for k,v in bins.items():
        for x in v:
            invertBins.setdefault(x,k)

    binSeqs={}
    fqs=_fqIter(fastq)
    for seq in fqs:
        if seq.id in invertBins:
            b=invertBins[seq.id]
            if b in binSeqs:
                if len(binSeqs[b]) <= int(limit):
                    binSeqs.setdefault(b,[]).append(seq)
                else:
                    continue
            else:
                binSeqs.setdefault(b,[]).append(seq)

    for b in binSeqs:
        with open('{0}.fastq'.format(b),'wt') as outf:
            SeqIO.write(binSeqs[b],outf,'fastq')



def runGeneExtract(opts):
    md=getMeta(opts.meta_files)
    bins=getReadBins(opts.bam_file,md)
    binSeqs(bins,opts.fastq_file,opts.bin_size)

if __name__=="__main__":
    parser = ArgumentParser(description='parse sam file and extract genes into bins')
    parser.add_argument('-b', '--bam_file', required=True,
            help='Specify bam file')
    parser.add_argument('-m', '--meta_files', required=True,
            help='Specify ngstar directory containing allele meta data files')
    parser.add_argument('-f', '--fastq_file', required=True,
            help='Specify input fastq file')
    parser.add_argument('-s', '--bin_size', required=False,default=200,
            help='number of reads to bin')
    opts, unknown_args = parser.parse_known_args()
    runGeneExtract(opts)

