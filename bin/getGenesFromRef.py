#!/usr/bin/env python3
import sys
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import gzip

geneList = [
    #['penA', 1524397, 1526148, -1,'cfx'], 
    #['penB', 2049351, 2050397, -1,'cfx'], 
    ['mtrR', 1332867, 1333499, 1,'cro'], 
    ['pilQ', 103068, 105263, -1,'cro'], 
    ['ponA', 108362, 110758, 1,'cro'],
    ['mtrR', 1332867, 1333499, 1,'cfx'], 
    ['pilQ', 103068, 105263, -1,'cfx'], 
    ['ponA', 108362, 110758, 1,'cfx'],
    ['gyrA', 1051396, 1054146, 1, 'cip'], 
    #['gyrB', 2098152, 2100542, -1, 'cip'], 
    ['parC', 193663, 195966, -1, 'cip'], #,
    #['parE', 1296483, 1298468, -1, 'cip'],
    ['norM_promoter', 460253, 460253, 0, 'cip'],
    ['rpsJ', 2031311, 2031622, 1, 'tet'],
    ['pilQ', 103068, 105263, -1,'tet'],
    ['mtrR', 1332867, 1333499, 1,'tet'],
    ['mtrR', 1332867, 1333499, 1,'pen'], 
    ['pilQ', 103068, 105263, -1,'pen'], 
    ['ponA', 108362, 110758, 1,'pen'],
    ['mtrR', 1332867, 1333499, 1,'azt'],
    ['macAB_promoter', 1415365, 1415365, 0, 'azt'] 
    ]

def convertList(geneList):
    d={}
    for g in geneList:
        d.setdefault( g[0],
           {'start':g[1],
            'end':g[2],
            'strand':g[3],
            'abx':g[4] })
    return d

def extractGenes(ref,genes):
    for seq in SeqIO.parse(ref,'fasta'):
        for gene in genes:
            with open('cutGenes/{0}.fasta'.format(gene),'wt') as outf:
                start=genes[gene]['start']-1
                end=genes[gene]['end']
                i=gene
                s=seq[start:end]
                d=seq.description
                cutSeq=SeqRecord(s.seq,id=i,description=d)
                SeqIO.write(cutSeq,outf,'fasta')

genes=convertList(geneList)
for gene in genes:
    print(gene,genes[gene])

refSeq=sys.argv[1]

if refSeq.endswith('gz'):
    mo=gzip.open
else:
    mo=open

with mo(refSeq,'rt') as ref:
    extractGenes(ref,genes)


