#!/usr/bin/env python3
from Bio import SeqIO
import sys


def getSeqs():
    ids=set()
    with open(sys.argv[1],'rt') as inf:
        for seq in SeqIO.parse(inf,'fastq'):
            if seq.id not in ids:
                yield seq
            ids.add(seq.id)

def getSeqsGZ():
    ids=set()
    with gzip.open(sys.argv[1],'rt') as inf:
        for seq in SeqIO.parse(inf,'fastq'):
            if seq.id not in ids:
                yield seq
            ids.add(seq.id)

if (sys.argv[1]).endswith('gz'):
    seqs=getSeqsGZ()
else:
    seqs=getSeqs()

with open(sys.argv[2],'wt') as outf:
    SeqIO.write(seqs,outf,'fastq')
