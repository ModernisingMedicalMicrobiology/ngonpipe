#/usr/bin/env python3
import numpy as np
import pandas as pd
from Bio import SeqIO
import gzip
import sys
import re
from tqdm import tqdm
from argparse import ArgumentParser, SUPPRESS
import pysam
import pysamstats
import logging
logging.basicConfig(filename='callPysam.log',level=logging.DEBUG)

def accuracy(r,base):
    if r['ref'] == r[base]:
        return 0
    else:
        return 1
def mut_type(r,base):
    if r['ref'] == r[base]:
        return None
    else:
        return r['ref'] + r[base]

class callPysam:
    def __init__( self, bamFile, outf, refFile, depth, minRev, minFreq ):
        self.bamFile = bamFile
        self.outfast = outf
        self.refs = refFile
        self.depthMin = depth
        self.minRev = minRev
        self.minFreq = minFreq

    def refInfo(self):
        self.refStats,self.refDescript={},{}
        refFile=self.refs
        if refFile.endswith('gz'):
            op=gzip.open
        else:
            op=open
        with op(refFile,'rt') as inF:
        #with open(refFile,'rt') as inF:
            for seq in SeqIO.parse(inF,'fasta'):
                chrom=str(seq.id)#.replace('.','_')
                self.refStats[chrom] = { 'len' : len(seq.seq) ,
                        'covpos' : np.zeros(len(seq.seq) ),
                        'prediction' :  np.zeros((len(seq.seq), 6)),
                        'baspos' : np.chararray(len(seq.seq) ) }
                self.refStats[chrom]['baspos'][:] = 'N'
                self.refDescript[chrom] = seq.description

    def _pysamStats(self,b,r):
        mybam = pysam.AlignmentFile(b)
        refLen=sum([self.refStats[chrom]['len'] for chrom in self.refStats])
        n,c,l=0,10000,[]
        for rec in tqdm(pysamstats.stat_variation_strand(mybam,fafile=r),total=refLen):
            if n > c:
                yield l 
                l=[]
                n=0
            else:
                l.append(rec)
                n+=1
        yield l

    def fillNewRef(self,row):
        self.refStats[row['chrom']]['baspos'][row['pos']] = row['top_base_seq']
        self.refStats[row['chrom']]['covpos'][row['pos']] = row['reads_all']

    def maskDels(self,row):
        if row['deletions %'] >= 30:
            self.deletions+=1
            return 'N'
        else:
            return row['top_base_seq']

    def maskBase(self,row):
        if row['reads_all'] < int(self.depthMin):
            return 'N'
        elif row['majority base %'] <= float(self.minFreq):
            self.mixBase+=1
            return 'N'
        else:
            return row['top_base_seq']

    def callbam(self):
        self.deletions=0
        self.mixBase=1
        bases=['A','T','C','G','insertions','deletions']
        ps=self._pysamStats(self.bamFile,self.refs)
        for recs in ps:
            df=pd.DataFrame(recs)
            #print('new chnk!')
            for b in bases:
                df['{0} %'.format(b)]=(df[b]/df['reads_all'])*100
            df['reads_all']=df[['A','T','C','G']].sum(axis=1)
            df['top_base'] = df[['A','T','C','G']].max(axis=1)
            df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
            df['majority base %'] = (df['top_base'] / df['reads_all'])*100
            df['top_base_seq'] = df.apply(self.maskDels,axis=1)
            df['top_base_seq'] = df.apply(self.maskBase,axis=1)
            #df['top_base_accuracy']=df.apply(accuracy,axis=1,args=('top_base_seq',))
            #df['mut type']=df.apply(mut_type,axis=1,args=('top_base_seq',))
            df.apply(self.fillNewRef,axis=1)
        print('Deletions masked:',self.deletions)
        print('Mixed bases masked:',self.mixBase)



    def makeFasta(self):
        with open(self.outfast,'wt') as outf:
            s=''
            for chrom in self.refStats:
                s+='>{0}\n{1}\n'.format( chrom,
                        self.refStats[chrom]['baspos'].tostring().decode('utf-8') )
            outf.write(s)


def runCaller(opts):
    cp = callPysam(opts.bamFile,opts.fasta,opts.refs,opts.depth,opts.minRev,opts.minFreq)
    print('Getting ref info')
    cp.refInfo()
    print('Parsing bam file')
    cp.callbam()
    print('making output files...')
    cp.makeFasta()

def callerGetArgs(parser):
    parser.add_argument('-b', '--bamFile', required=True,
                                 help='mpileup file from mapped bam')
    parser.add_argument('-f','--fasta',required=False,default='cons.fasta',
                                             help='outfile fasta file name')
    parser.add_argument('-r','--refs',required=True,
                                             help='reference fasta file')
    parser.add_argument('-d','--depth',required=False,default=8,type=int,
                                             help='depth of reads needed, default=8')
    parser.add_argument('-R','--minRev',required=False,default=2,type=int,
                                             help='minimum supporting seqs in reverse orientation, default=2')
    parser.add_argument('-F','--minFreq',required=False,default=75,type=int,
                                             help='frequency (percent) of majority bases needed')
    return parser


if __name__ == '__main__':
    parser = ArgumentParser(description='call consensus from bam file using pysamstats')
    parser = callerGetArgs(parser)
    opts, unknown_args = parser.parse_known_args()
    # run
    runCaller(opts)
