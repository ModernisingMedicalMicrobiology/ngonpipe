#!/usr/bin/env python3
import sys, os, gzip
from argparse import ArgumentParser
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Blast import NCBIXML
from Bio.Blast.Applications import NcbiblastnCommandline, NcbiblastpCommandline
from io import StringIO
import pandas as pd

class refVariants:
    def __init__(self,outf,inf,ref,polishedGene,sampleName,metaFile,pysamstats):
        self.outf=outf
        self.inf=inf
        self.ref=ref
        self.getSeqs()
        self.polishedGene=polishedGene
        self.sampleName=sampleName
        self.metaFile=metaFile
        self.pysamstats=pysamstats
        self.getPysamstats()

    def getPysamstats(self):
        self.pysamdf=pd.read_csv(self.pysamstats,sep='\t')
        self.pysamdf=self.pysamdf[['chrom','pos','reads_all']]

    def getSeqs(self):
        self.refFa = SeqIO.read( self.ref, 'fasta' )
        self.fa =    SeqIO.read( self.inf, 'fasta' )

    def getGeneSeqs(self,r):
        if r['strand'] == 1:
            r['ref sequence']=str(self.refFa[r['start']-1:r['end']-1].seq)
            r['sequence']=str(self.fa[r['start']-1:r['end']-1].seq)
            print(r['strand'],r['gene'],'\n',r['ref sequence'])
            print(r['strand'],r['gene'],'\n',r['sequence'])
        elif r['strand'] == -1:
            r['ref sequence']=str(self.refFa[r['start']:r['end']].seq.reverse_complement())
            r['sequence']=str(self.fa[r['start']:r['end']].seq.reverse_complement())
        return r

    def getProtein(self,r):
        if r['coding'] == 1:
            if r['strand'] == 1:
                seq=self.refFa[r['start']-1:r['end']-1].seq.translate()
                r['ref protein'] = seq
                seq=self.fa[r['start']-1:r['end']-1].seq.translate()
                r['protein'] = seq
                print(r['strand'],r['gene'],'\n',r['ref protein'])
                print(r['strand'],r['gene'],'\n',r['protein'])
            elif r['strand'] == -1:
                r['ref protein']=self.refFa[r['start']:r['end']].seq.reverse_complement().translate()
                r['protein']=self.fa[r['start']:r['end']].seq.reverse_complement().translate()
                print(r['strand'],r['gene'],'\n',r['ref protein'])
                print(r['strand'],r['gene'],'\n',r['protein'])
        return r
    
    def getMuts(self,r):
        if r['type'] == 'substitution':
            r['ref amino acid'] = str(r['ref protein'])[int(r['pos'])-1]
            r['amino acid']  = r['protein'][int(r['pos'])-1]
        elif r['type'] == 'SNP':
            r['ref base'] = r['ref sequence'][int(r['pos'])-1]
            r['base'] = r['sequence'][int(r['pos'])-1]
        return r

    def getDepths(self,r):
        df=self.pysamdf[self.pysamdf['pos'] >= r['start']]
        df=df[df['pos'] <= r['end']]
        r['mean depth'] = df['reads_all'].mean()
        r['min depth'] = df['reads_all'].min()
        return r

    def getRefVariants(self):
        # relative to ref geneome - http://www.ncbi.nlm.nih.gov/nuccore/NC_011035.1
        geneList=pd.read_excel(self.metaFile,sheet_name='gene_pos')
        geneList=geneList.apply(self.getGeneSeqs,axis=1)
        geneList=geneList.apply(self.getProtein,axis=1)
        mutList=pd.read_excel(self.metaFile,sheet_name='mutations')
        geneList=geneList.merge(mutList,on='gene',how='outer')
        geneList=geneList[~geneList['chrom'].isna()]
        geneList=geneList.apply(self.getMuts,axis=1)
        geneList['Sample']=self.sampleName
        df=geneList[['Sample','gene','start','end','pos','ref','alt','ref amino acid','amino acid','ref base','base','abx']]
        df=df.apply(self.getDepths,axis=1)
        df.to_csv(self.outf)
        


if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='bam file to fastq file, using original fastq files so no trimming')
    parser.add_argument('-f', '--inf', required=True,
                             help='input fasta file')
    parser.add_argument('-o', '--outf', required=True,
                             help='output tab file')
    parser.add_argument('-r', '--ref', required=True,
                             help='ref file')
    parser.add_argument('-g', '--gene', required=False,nargs='+',
                             help='gene(s) to look at')
    parser.add_argument('-s', '--sampleName', required=True,
                             help='ref file')
    parser.add_argument('-m', '--metaFile', required=True,
                             help='meta data spreadsheet file')
    parser.add_argument('-p', '--pysamstats', required=True,
                             help='pysamstats file')

    opts, unknown_args = parser.parse_known_args()

    r=refVariants(opts.outf,
            opts.inf,
            opts.ref,
            opts.gene,
            opts.sampleName,
            opts.metaFile,
            opts.pysamstats)
    r.getRefVariants()

