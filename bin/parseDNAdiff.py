#!/usr/bin/env python3
import sys
import six
import os
import re
from collections import defaultdict
from collections import namedtuple
import subprocess
import tempfile
from subprocess import STDOUT
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

dnadiff_extensions = (
            '.1coords', '.1delta', '.delta', '.mcoords', '.mdelta', '.qdiff', '.rdiff', '.report', '.snps', '.unref', '.unqry')

Property = namedtuple('Property', 'ref query')
PropertyWithPerc = namedtuple('PropertyWithPerc', 'ref ref_perc query query_perc')

def _parse_complex_section(lines):
    """Parse a complex dnadiff report section."""
    section = "NO_SECTION"
    sections = defaultdict(list)
    results = defaultdict(dict)
    # Parse alignment section into subsections:
    for line in lines:
        if len(line) == 0:
            continue
        # FIXME: Very specific to current dnadiff output:
        if line.startswith('1-to-1') or line.startswith('M-to-M') or re.match("Total(S|G|I)", line):
            tmp = re.split("\s+", line)
            section = tmp[0]
            results[section]['Number'] = {'ref':float(tmp[1]), 'qry':float(tmp[2])}
        else:
            sections[section].append(line)

    # Parse subsections and update results dictionary:
    for section, lines in six.iteritems(sections):
        parsed = _parse_simple_section(lines)
        for name, prop in six.iteritems(parsed):
            results[section][name] = prop
    return results

def _parse_percent_field(field):
    """Parse dnadiff field with percent value."""
    tmp = field.split('(')
    perc = tmp[1].replace(')', '')
    perc = perc.replace('%', '')
    return float(tmp[0]), float(perc)


def _parse_simple_section(lines):
    """Parse a simple dnadiff report section."""
    results = {}
    for line in lines:
        tmp = re.split("\s+", line)
        if '%' not in tmp[1] and '%' not in tmp[2]:
            results[tmp[0]] = {'ref':float(tmp[1]), 'qry':float(tmp[2])}
            pass
        else:
            ref_prop, ref_prop_perc = _parse_percent_field(tmp[1])
            query_prop, query_prop_perc = _parse_percent_field(tmp[2])
            results[tmp[0]] = {'ref':ref_prop, 
                    'qry':query_prop} 
    return results


def _parse_dnadiff_into_sections(report_file):
    """Parse dnadiff output lines into sections."""
    report_fh = open(report_file, 'r')
    section = "NO_SECTION"
    sections = defaultdict(list)
    for line in report_fh:
        line = line.strip()
        if len(line) == 0:
            continue
        if line.startswith('/') or line.startswith('NUCMER') or line.startswith('[REF]'):
            continue
        if line.startswith('['):
            section = line
            section = section.replace('[', '')
            section = section.replace(']', '')
        else:
            sections[section].append(line)
    return sections

def readFile(fol,f):
    #'none.fastq_H18-208_R00000419_v3_F60_D10.report'
    #print(fol + f)
#    depth=int(f.split('.')[1].split('_')[5][1:])
    prop=int(f.split('_')[-1].split('.')[0])
    sample=str(f.split('_')[0])
    
    sections = _parse_dnadiff_into_sections(fol + f)
    results_bases = _parse_simple_section(sections['Bases'])
    df=pd.DataFrame.from_dict(results_bases,orient='index').T
#    df['Depth']=depth
    df['prop support']=prop
    df['sample']=sample

    results_snps = _parse_complex_section(sections['SNPs'])
    df2=pd.DataFrame.from_dict(results_snps['TotalSNPs'],orient='index').T
    df=pd.merge(df,df2,left_index=True,right_index=True)
    #print(df)
    #print(df2)
    return df

def getFiles(fol):
    f=os.listdir(fol)
    f=[i for i in f if i.endswith('.report')]
    dfs=[]
    for i in f:
        dfs.append(readFile(fol,i))
    df=pd.concat(dfs)
    df.to_csv('all_bases.csv')
    return df

def plot(df):
    g=sns.line


getFiles(sys.argv[1])

