
tars=tar/*

for tarFile in ${tars}
do
	echo ${tarFile}
	b=$(basename $tarFile _f5s.tar.gz)
	echo $b
	tar -xzf ${tarFile} 
	single_to_multi_fast5 -i ./ -s multi/ -f ${b}
	rm *.fast5
done
