#!/usr/bin/env python3
from ete3 import PhyloTree
import sys

treeFile=open(sys.argv[1],'rt').read()
t = PhyloTree(treeFile,format=1)
print(t)

