#!/usr/bin/env python3
import sys
import numpy as np
import pandas as pd
from argparse import ArgumentParser, SUPPRESS
# roc curve and auc score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from itertools import permutations
import pickle
import vcf

basesN=['A', 'C', 'G', 'T']
perm = permutations(basesN,2)
N,perms=0,{}
for p in perm:
    perms.setdefault(p[0],{}).setdefault(p[1],N)
    N+=1

def totalReads(row):
    l=row['INFO'].split(';')
    return int(l[2].replace('TotalReads=',''))

def supportFraction(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[4].replace('SupportFraction=',''))
    else:
        return None

def BaseCalledReadsWithVariant(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[0].replace('BaseCalledReadsWithVariant=',''))
    else:
        return None

def BaseCalledFraction(row):
    l=row['INFO'].split(';')
    if len(l)>2:
        return float(l[1].replace('BaseCalledFraction=',''))
    else:
        return None


def addBaseChangeN(row):
    r=row['REF']
    a=row['ALT']
    p=perms[r][a]
    return p

def alphaBeta(row):
    if row['REF'] == 'A' and row['ALT'] == 'G':
        return 0
    elif row['REF'] == 'A' and row['ALT'] == 'T':
        return 1
    elif row['REF'] == 'A' and row['ALT'] == 'C':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'A':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'G':
        return 1
    elif row['REF'] == 'C' and row['ALT'] == 'T':
        return 0
    elif row['REF'] == 'G' and row['ALT'] == 'A':
        return 0
    elif row['REF'] == 'G' and row['ALT'] == 'C':
        return 1
    elif row['REF'] == 'G' and row['ALT'] == 'T':
        return 1
    elif row['REF'] == 'T' and row['ALT'] == 'A':
        return 1
    elif row['REF'] == 'T' and row['ALT'] == 'C':
        return 0
    elif row['REF'] == 'T' and row['ALT'] == 'G':
        return 1


class classifier:
    def __init__(self,inVCF,pysamstats,outVCF,model,variantCaller, mw,probFilt):
        self.inVCF=inVCF
        self.pysamstats=pysamstats
        self.outVCF=outVCF
        self.modelFile=model
        self.loadModel()
        self.vc=variantCaller
        self.keep=set()
        self.probFilt=float(probFilt)
        self.maskWeak = mw

    def loadModel(self):
        self.model = pickle.load(open(self.modelFile, 'rb'))

    def maskProbs(self,r):
        if r['probs'] >= self.probFilt:
            return False
        else:
            return True

    def ps(self,row):
        if row['ALT']=='N':
            x=None
        else:
            x = float(row[row['ALT']+'_PS'])
        return x


    def readVCF(self):
        names=['CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO','FORMAT','EXTRA']
        dfs=[]
        for vcf_file in self.inVCF:
            with open(vcf_file,"r") as fi:
                matchStr='##contig=<ID='
                self.chrom=[ln.split(",")[0].replace(matchStr,'') for ln in fi if ln.startswith(matchStr)][0]
            dfs.append(pd.read_csv(vcf_file,sep='\t',comment='#',names=names))
        df=pd.concat(dfs)
        if len(df) == 0:
            return 0
        df['ALT_len']=df.ALT.map(len)
        df['REF_len']=df.REF.map(len)
        df['INDEL length']=df['ALT_len'] -  df['REF_len']
        if self.vc == 'nanopolish':
            df['Total reads']=df.apply(totalReads,axis=1)
            df['Support fraction']=df.apply(supportFraction,axis=1)
            df['BaseCalledReadsWithVariant']=df.apply(BaseCalledReadsWithVariant,axis=1)
            df['BaseCalledFraction']=df.apply(BaseCalledFraction,axis=1)

        df['5prime proximity']=df['POS']-df['POS'].shift(1)
        df['3prime proximity']=df['POS']-df['POS'].shift(-1)
        df['3prime proximity']=df['3prime proximity'].abs()
        df['proximty']=df[['5prime proximity','3prime proximity']].min(axis=1)
        df['proximty'].fillna(5000,inplace=True)
        df['alphabeta']=df.apply(alphaBeta,axis=1)
        df=df[df.REF_len==1]
        df=df[df.ALT_len==1]
        if len(df) > 0:
            df['baseChange']=df.apply(addBaseChangeN,axis=1)

        psdf=pd.read_csv(self.pysamstats,sep='\t')
        df=df.merge(psdf,left_on=['CHROM','POS'],right_on=['chrom','pos'],how='left')

        df['total']=df['A']+df['C']+df['G']+df['T']
        bases=['A','T','C','G','insertions','deletions']
        for b in bases:
            df['{0} %'.format(b)]=(df[b]/df['reads_all'])*100
            df['{0}_PS'.format(b)]=(df[b]/df['total'])
        df['reads_all']=df[['A','T','C','G']].sum(axis=1)
        df['top_base'] = df[['A','T','C','G']].max(axis=1)
        df['top_base_seq'] = df[['A','T','C','G']].idxmax(axis=1)
        df['majority base %'] = (df['top_base'] / df['reads_all'])
        df['Top Base matches Nanopolish'] = np.where(df.ALT == df.top_base_seq,1,0)
        df=df[df['majority base %'].notna()]
        if len(df)>0:
            df['ps']=df.apply(self.ps, axis=1)
        #masked=df[df['Support fraction']==0]
        #s=set(masked.POS)
        #self.masked.update(s)
        #df=df[df['Support fraction']>0]
        self.SNPs=df
        return len(df)

    def classify(self):
        feature_combinations={
            'nanopolish':['QUAL','Support fraction','Total reads','proximty','baseChange','majority base %','Top Base matches Nanopolish','deletions %','insertions %'],
            'medaka':['QUAL','reads_all','proximty','baseChange','majority base %','Top Base matches Nanopolish','deletions %','insertions %'],
            'clair':['QUAL','reads_all','proximty','baseChange','majority base %','Top Base matches Nanopolish','deletions %','insertions %']
            }
        features=feature_combinations[self.vc]
        print(self.SNPs[features])
        X=np.array(self.SNPs[features])
        preds = self.model.predict(X)
        probs = self.model.predict_proba(X)
        probs=pd.DataFrame(probs,columns=[True,False])
        p=probs.max(axis=1)
        self.SNPs['preds']=preds
        self.SNPs['probs']=p
#        self.SNPs['preds']=self.SNPs.apply(self.maskProbs,axis=1)
        self.SNPs['mask']=self.SNPs.apply(self.maskProbs,axis=1)
        keep=self.SNPs[self.SNPs.preds==True]
        s=set(keep.POS)
        self.keep.update(s)
        mask=self.SNPs[self.SNPs['mask']==True]
        psmask=self.SNPs[self.SNPs['ps']<0.8]
        self.mask=set(mask['POS'])
        print(len(self.mask))
        self.mask.update(list(psmask['POS']))
        print(len(self.mask))

    def _vcf_reader(self):
        for vcf_file in self.inVCF:
            vcf_reader = vcf.Reader(open(vcf_file, 'r'))
            for record in vcf_reader:
                yield record

    def filter(self):
        #vcf_reader = vcf.Reader(open(self.inVCF, 'r'))
        vcf_reader = self._vcf_reader()
        vcf_oneread= vcf.Reader(open(self.inVCF[0], 'r'))
        vcf_writer = vcf.Writer(open(self.outVCF, 'w'), vcf_oneread)
        for record in vcf_reader:
            if record.POS not in self.keep: continue
            else:
                if self.maskWeak == True:
                    if record.POS in self.mask:
                        record.ALT = 'N'
                        print('masking',record.POS)
                vcf_writer.write_record(record)

        vcf_writer.close()

        if self.vc in ['medaka','clair']:
            print('adding nanopolish window to header')
            f = open(self.outVCF, 'r')
            contents = f.readlines()
            f.close()
    
            contents.insert(1, '##nanopolish_window={0}:0-2500000\n'.format(self.chrom))
    
            f = open(self.outVCF, 'w')
            contents = "".join(contents)
            f.write(contents)
            f.close() 


def run(opts):
    c=classifier(opts.inVCF,
            opts.pysamstats,
            opts.outVCF,
            opts.model,
            opts.variant_caller,
            opts.maskWeak,
            opts.probFilt)
    pos=c.readVCF()
    if pos>0:
        c.classify()
    c.filter()



if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='Filter VCF file using pre trained model file')
    parser.add_argument('-i', '--inVCF', required=True,nargs='+',
                             help='Input VCF file')
    parser.add_argument('-o', '--outVCF', required=True,
                             help='Output VCF file')
    parser.add_argument('-f', '--probFilt',required=False,default=0,
                             help='probability threshold filter')
    parser.add_argument('-v', '--variant_caller',required=False,default='nanopolish',
                             help='variant caller used')
    parser.add_argument('-m', '--model', required=True,
                             help='model file')
    parser.add_argument('-p', '--pysamstats', required=True,
                             help='pysamstats file')
    parser.add_argument('-mw', '--maskWeak', required=False,action='store_true',
                             help='mask weakly supported positions from pysam')
    opts, unknown_args = parser.parse_known_args()
    run(opts)

