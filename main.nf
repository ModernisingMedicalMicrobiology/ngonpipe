


//################################# Params ###################################//
params.base="$baseDir"
params.blastdb=params.base + '/genes/genes.fasta'
params.refgenes=params.base + '/genes/refgenes.fasta'
params.accessoryGenes=params.base + '/genes/accessory_genes.fasta'
params.blastmast=params.base + '/genes/mast.fasta'
params.ngstarMeta=params.base + '/genes/'
params.ref=params.base + '/refs/R00000419.fasta.gz'
params.generef=params.base + '/genes/genes.fasta'
params.CLAIRMODEL=params.base + '/models/clair/model'

params.mapfull = true
params.assemblefull = true
params.assembler='ra'
params.genomeSize='2.4m'
params.compareSeqs = params.base + '/compareSeqs'
params.mode = 'notcrumpit'

params.centdb = ''
params.mongodb = ''
params.makeConensus = true 
params.clairConsensus = true
params.taxid = 485


//############################ channels ######################################//
Channel
	.fromPath( "${params.ref}" )
        .map{ file -> tuple(file.baseName, file) }
	.view()
	.into{ refinputs; refinputs2; refinputs3; refinputs4 } 

Channel
	.fromPath( "${params.generef}" )
        .map{ file -> tuple(file.baseName, file) }
	.view()
	.into{ generefinputs; generefinputs2 } 

Channel
	.fromPath( "${params.infastq}" )
        .splitCsv()
        .map{ row -> tuple(row[0], row[1]) }
	.view()
	.into{ runs; runs2; runs3; runs4; runs5; runs6 }
//################################# processes ################################//
process classify {
    tag { run }
    errorStrategy 'ignore'
    
    input:
    set val(run),val(fqs) from runs
    
    output:
    set val(run), file("${run}.cent.tsv.gz") into centFiles, centFiles2
    
    script:
    cdb=params.centdb
    mdb=params.mongodb
    if(params.mode == 'crumpit')
        """
        mongoexport --db ${run} \
            --collection cent_stats \
            --out ${run}.cent.csv \
            --host $mdb --type=csv \
            --fields read_id,seqID,taxID,score,s2ndBestScore,hitLength,queryLength,numMatches
        
        sed -E 's/("([^"]*)")?,/\\2\\t/g' ${run}.cent.csv | gzip > ${run}.cent.tsv.gz
        """
    else
        """
        data=`ls -m ${fqs}* | tr -d '\\n' | tr -d ' '`
        centrifuge -f -x $cdb \
           --mm -q -U \$data \
           -S ${run}.cent.tsv --min-hitlen 16 -k 1 
        gzip ${run}.cent.tsv
        """ 
}

process binReads {
    errorStrategy 'ignore'
    tag { run }

    input:
    set val(run), file("${run}.cent.tsv.gz"),val(p) from centFiles.combine(runs2,by:0)

    output:
    set val(run),file("bins/${run}_all_*") into maps,maps2,maps3,maps4,maps5

    script:
    tax=params.taxid
    """
    binReads.py -s ${run} \
        -fq ${p}* \
        -cf ${run}.cent.tsv.gz \
        -tax ${tax} \
        -br all

    """

}


// ####################### accessory genes from all reads ##################### //
 
process overlapAccGenes {                                                                   
    tag { run } 
    scratch true
    cpus 2
                                                                                
    input:
    set val(run),val(fqs) from runs6

    output:
    set val(run),val(fqs),file('overlaps.paf.gz') into genePafs

    script:
    ref=params.accessoryGenes
    """                                                                     
    minimap2 -t ${task.cpus} -x ava-ont \
	  $ref ${fqs}* | gzip > overlaps.paf.gz
    """
}

process binAccGenes {
    tag { run }

    input:     
    set val(run),val(fqs),file('overlaps.paf.gz'),file('cent.tsv.gz') from genePafs.combine(centFiles2, by:0)

    output:
    set val(run),file("trimmed.fastq") into binnedAccGenes
    set val(run),file("untrimmed.fastq") into binnedUTAccGenes

    script:
    """

    binPafReads.py -p overlaps.paf.gz -f ${fqs}* -ot trimmed.fastq -o untrimmed.fastq -c cent.tsv.gz -t 485

    """
}

process mapAccGenes {
    tag { run }
    scratch true
    cpus=1
    
    input:
    set val(run),file("trimmed.fastq") from binnedAccGenes

    output:
    set val(run), file('genes.sorted.bam'),file('genes.sorted.bam.bai') into mappedAcc

    script:
    ref=params.accessoryGenes
    """
    minimap2 -t ${task.cpus} -ax map-ont $ref \
	trimmed.fastq | samtools view -F2052  -bS -o reads.bam -
    samtools sort -@ ${task.cpus} reads.bam -o genes.sorted.bam
    rm reads.bam
    samtools index genes.sorted.bam
    """
}

process accgenedepth {
  tag { run }
  publishDir 'accessoryGeneDepths'

  input:
  set val(run), file('genes.sorted.bam'),file('genes.sorted.bam.bai') from mappedAcc

  output:
  set val(run),file("${run}.csv") into Accdepth

  script:
  """
  samtools depth -aa genes.sorted.bam > ${run}_depth.tsv
  coverageStats.py ${run}_depth.tsv ${run}
  mv coverage_stats.csv ${run}.csv  
  """
}


process assembleAccessory {
    tag { run } 
    cpus 4 
    errorStrategy 'retry'
                                                                                
    input:
    set val(run),file('untrimmed.fastq') from binnedUTAccGenes

    output:
    set val(run), file("${run}.contigs.fa")  into accessoryassembled

    script:
    """ 
    wtdbg2 -t ${task.cpus} -i untrimmed.fastq -fo contigs -L 3000 
    wtpoa-cns -t ${task.cpus} -i contigs.ctg.lay.gz -fo ${run}.contigs.fa
    """
}

process blastAccessory {
    tag { run }
    publishDir 'accessoryblast'

    input:
    set val(run), file("${run}.contigs.fa") from accessoryassembled

    output:
    set val(run), file("${run}.O6.blast.txt"), file("${run}.genes.O6.blast.txt") into blastAccAssm

    script:
    db=params.base + '/plasmids/plasmids.fasta'
    db2=params.accessoryGenes
    outfmt="6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen"
    """
    blastn -query ${run}.contigs.fa -db $db -outfmt "$outfmt" -max_target_seqs 10000 > ${run}.O6.blast.txt
    blastn -query ${run}.contigs.fa -db $db2 -outfmt "$outfmt" -max_target_seqs 10000 > ${run}.genes.O6.blast.txt
    """
}

process accessoryResults {
    tag { run }
    publishDir 'accessoryResults'

    input:
    set val(run), file("${run}.O6.blast.txt"), file("${run}.genes.O6.blast.txt") from blastAccAssm

    output:
    set val(run), file("${run}_accessory_results.csv") into accessory_results

    script:
    """
    detect_accessory.py -p ${run}.O6.blast.txt -b ${run}.genes.O6.blast.txt -s ${run}
    """

}


// ############################# whole genome assembly ########################
if (params.assemblefull == true) {
    process filtLong {
        tag { run }                                                             

        input:
        set val(run),file('fastq.fq') from maps2

        output:
        set val(run),file('fastq.fq.gz') into filtLong_ch

        script:
        """
        filtlong --min_length 1000 \
            --keep_percent 90 \
            --target_bases 500000000 \
            fastq.fq |\
            gzip > fastq.fq.gz

        """
    }


    process assemble {
        tag { run } 
	errorStrategy 'ignore'
        cpus 4 
        publishDir 'assembledContigs', overwrite: true, mode: 'copy'
                                                                                    
        input:
        set val(run),file('fastq.fq.gz') from filtLong_ch
    
        output:
        set val(run), file("${run}.contigs.fa")  into assembled,assembled2
        set val(run), file("${run}.contigs.fa"),file("fastq.fq.gz")  into assembled3
    
        script:
        genomeSize=params.genomeSize
        if(params.assembler == 'wtbdg2')
            """ 
            wtdbg2 -t ${task.cpus} -i fastq.fq.gz -fo contigs -L 3000 
            wtpoa-cns -t ${task.cpus} -i contigs.ctg.lay.gz -fo ${run}.contigs.fa
            """
        else if(params.assembler == 'flye')
            """
            flye --nano-raw fastq.fq.gz -g $genomeSize -t ${task.cpus} -o contigs
            cp contigs/00-assembly/*.fasta ${run}.contigs.fa
            """
        else if(params.assembler == 'canu')
            """ 
            canu -p ${run} -d canu_contigs \
                genomeSize=$genomeSize \
                -nanopore-raw fastq.fq.gz \
                stopOnLowCoverage=0 useGrid=false \
                minThreads=${task.cpus} \
                maxThreads=${task.cpus}
            cp canu_contigs/${run}.contigs.fasta ${run}.contigs.fa
            """
        else if(params.assembler == 'ra')
            """
            ra -x ont -t ${task.cpus} fastq.fq.gz > ${run}.contigs.fa
            """
    }

    process centrifugecontigs {
        tag { sample }
        cpus 2 
        //publishDir 'centrifugeContigs', overwrite: true, mode: 'copy'

        input:
        set val(sample), file('fasta') from assembled2

        output:
        set val(sample), file("${sample}_contigs.out"),file("${sample}_contigs.report") into centcontigsPut
    
        script:
        db=params.centdb                                             
        """
        centrifuge -p ${task.cpus} -f -U fasta $db --report-file ${sample}_contigs.report --mm > ${sample}_contigs.out 
        """
    }
    
    process remap {
        tag { run }
        cpus 2

        input:
        set val(run), file("${run}.ctg.lay.fa"),file('fastq.fq.gz') from assembled3   

        output:
        set val(run), file("${run}.ctg.lay.fa"),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai") into remapped

        script:
        """
        minimap2 -t ${task.cpus} -ax map-ont ${run}.ctg.lay.fa fastq.fq.gz | samtools view -q 50 -bS - | samtools sort -o ${run}.sorted.bam 
        samtools index ${run}.sorted.bam
        """
    }

 
    process blastn {
    	tag { sample }
    
    	//publishDir 'blasts', mode: 'copy', overwrite: true
    	
    	input:
    	set val(sample), file('fasta') from assembled 
    
    	output:
    	set val(sample), file("*.O6.blast.txt") into blastOut
    
    	script:
        db=params.blastdb
    	"""
    	blastn -query fasta -db $db -outfmt 6 -max_target_seqs 10000 > ${sample}.O6.blast.txt
    	"""
    }

    
    process parseBlast6 {
        errorStrategy 'ignore'
    	tag { sample }
    
    	//publishDir 'ngstarFullAssembly', mode: 'copy', overwrite: true, pattern: '*.csv'
    	
    	input:
    	set val(sample), file('O6.blast.txt') from blastOut
    
    	output:
    	set val(sample), file("${sample}.csv"),file('windows') into assembly_pb6_out
        set val(sample),file("windows/*.txt") into assembly_gene_windows
    
    	script:
    	meta=params.ngstarMeta
    	"""
    	pb6.py -b O6.blast.txt -m $meta -o ${sample}.csv
        mkdir windows
    	awk -F, '{print \$2":"\$8"-"\$9  > "windows/"\$15"_"\$8".txt" }' ${sample}.csv
    	rm windows/gene_x*
    	"""
    }   

    process cutRemapBam {
        tag {run}
        //publishDir 'cutRemapDepths', pattern: '*_cutDepths', overwrite: true, mode: 'copy'

        input:
        set val(run),file('ref'),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai"),file("${run}.csv"),file('windows') from remapped.combine(assembly_pb6_out, by:0)

        output:
        set val(run),file('ref'),file("${run}.cut.bam"),file("${run}.cut.bam.bai"),file("${run}_cut.fastq") into RemapCuts1,RemapCuts2
        set val(run),file("${run}_cutDepths") into cutRemapDepths
        set val(run),file("fasta_cuts/*") into WGAgenes
        set val(run),file("fastq_cuts/*") into binnedWGAGenes 

        script:
        """
        mkdir cuts fastq_cuts ${run}_cutDepths fasta_cuts
        regions=windows/*.txt
        for region in \${regions}
        do
            reg=\$(cat \${region})
            fileName=\$(basename \$region .txt)
            geneName=(\${fileName//_/ })
            samtools view -b ${run}.sorted.bam \$reg -o cuts/${run}.\${geneName}.bam
            samtools index cuts/${run}.\${geneName}.bam
            samtools depth -r \$reg cuts/${run}.\${geneName}.bam > ${run}_cutDepths/${run}.\${geneName}_depth.tsv
            bamToFastq -i cuts/${run}.\${geneName}.bam -fq fastq_cuts/\${geneName}.fastq
            head -n 1600 fastq_cuts/\${geneName}.fastq >> ${run}_cut.fastq
            samtools faidx ref \${reg} > fasta_cuts/\${geneName}.fasta
        done
        samtools merge ${run}.cut.bam cuts/*bam
        samtools index ${run}.cut.bam
        """
    }

    WGAgenes
        .transpose()
        .map { run, file -> tuple(run, file.baseName, 'WGA_genes', file) }
        .view()
        .set{ WGAgenebins }

    binnedWGAGenes
        .transpose()
        .map { run, file -> tuple(run, file.baseName, 'WGA_genes', file) }
        .filter{ it[1] in [/penA/,/POR/,/TBPB/] }
        .view()
        .set{ WGA_geneFQBins }


}

//#################################### mapping #################################


if (params.mapfull == true){

    process blastRef {
    	tag { r }
    
    	//publishDir 'refblasts', mode: 'copy', overwrite: true
    	
    	input:
    	set val(r),file('ref') from refinputs3
    
    	output:
    	set val(r), file("*.O6.blast.txt") into refblastOut
    
    	script:
        db=params.refgenes
    	"""

    	zless ref | blastn -query - -db $db -outfmt 6 -max_target_seqs 10000 > ${r}.O6.blast.txt

    	"""
    }
    
    process parseBlast6Ref {
    	tag { r }
	errorStrategy 'ignore'
    
    	//publishDir 'refngstar', mode: 'copy', overwrite: true
    	
    	input:
    	set val(r), file('O6.blast.txt') from refblastOut
    
    	output:
    	set val(r), file("${r}.csv"),file('windows') into pb6_out1
        set val(r), file("windows/*.txt") into gene_windows,gene_windws2
    
    	script:
    	meta=params.ngstarMeta
    	"""

    	pb6.py -b O6.blast.txt -m $meta -o ${r}.csv
        mkdir windows
    	awk -F, '{print \$2":"\$8"-"\$9  > "windows/"\$15"_"\$8".txt" }' ${r}.csv
        rm windows/gene_x*

    	"""
    }

    process map {                                                                   
        tag { run + ' ' + r } 
        cpus 3
        publishDir 'bams', pattern: '*.bam*', overwrite: true
                                                                                    
        input:
        set val(r),file('ref'),val(run),file('fastq') from refinputs.combine(maps)
    
        output:
        set val(r),val(run),file('ref'), file(fastq),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai") into aligned
        set val(r),val(run),file('ref'),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai") into fullAligned,aligned2,fullAligned3
        set val(run),file('ref'),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai") into fullAligned4,fullAligned5
    
        script:
        """                                                                     
        minimap2 -t 2 -ax map-ont 'ref' 'fastq' | samtools view -q 50 -bS - | samtools sort -o ${run}.sortedfull.bam
        samtools index ${run}.sortedfull.bam
	subSampleBam.py -b ${run}.sortedfull.bam -s 100 -o ${run}.sorted.bam
        """
    }

    process mtrProm {
	errorStrategy 'ignore'
        tag { run }
        publishDir 'mtrProm', overwrite: true
    
        input:
        set val(run),file('ref'),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai") from fullAligned5
    
        output:
        set val(run),file("${run}_mtrProm.tab") into mtrProm_ch
    
        script:
        """
        indel_class.py -b ${run}.sorted.bam -p 1332809 -c R00000419 -s ${run}  > ${run}_mtrProm.tab
        """
    }

    process pysamStats {
        tag { run }
    
        input:
	set val(r),val(run),file('ref.fa.gz'),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai") from fullAligned3
    
        output:
	set val(run), file("${run}_pysamfile.txt") into pysamstats_ch,pysamstats_ch2,pysamstats_ch3
	set val(run), file("${run}_pysamfile.txt"), file('ref.fa') into pysamstats_ch4
    
        script:
        """
        zless ref.fa.gz > ref.fa
        pysamstats -t variation_strand ${run}.sorted.bam -f ref.fa > ${run}_pysamfile.txt
        """
    
    }

if (params.makeConensus == true){

if (params.clairConsensus == true){

process clair {
    cpus 4
    label 'clair'
    //errorStrategy 'ignore'
    tag {run}

    input:
    set val(run), file('ref.fa.gz'), file("${run}.sorted.bam"), file("${run}.sorted.bam.bai") from fullAligned4

    output:
    set val(run), file("${run}.vcf"),file('ref.fa') into clairVCFs,clairVCFs2

    script:
    ref=params.ref
    CLAIRMODEL=params.CLAIRMODEL
    """
    zcat ${ref} > ref.fa
    samtools faidx ref.fa
    clair.py callVarBam \
        --chkpnt_fn ${CLAIRMODEL} \
        --ref_fn ref.fa \
        --bam_fn ${run}.sorted.bam \
        --ctgName R00000419 \
        --sampleName ${run} \
        --minCoverage 1 \
        --threads ${task.cpus} \
        --call_fn ${run}.vcf
    """
}


process makeGenomeClairConensus {
    tag { run }

    publishDir 'clairConsensuSeqs'

    input:
    set val(run),file("${run}.vcf"),file('ref.fa'), file("${run}_pysamfile.txt"),('ref2.fa') from clairVCFs.combine(pysamstats_ch4, by:0)

    output:
    file("${run}.fasta.gz") into genomeConsensusFiles
    file("input.txt") into genomeInputFiles
    set val("${run}"), file("${run}_polishedGenome.fa"),file('ref.fa') into unmasked

    script:
    modelBase=params.base + '/models'
    """

    filterVCF_model.py -i ${run}.vcf \
		-v clair \
		-o ${run}_genomefiltered.vcf \
		-m ${modelBase}/clair_composite_model.sav  \
		-p ${run}_pysamfile.txt \
		-f 0 

    nanopolish vcf2fasta --skip-checks -g ref.fa ${run}_genomefiltered.vcf > ${run}_polishedGenome.fa

    maskDepth.py -p ${run}_pysamfile.txt -f ${run}_polishedGenome.fa -o ${run}_polishedGenome_masked.fa -d 10 -mw 0.8

    cat ${run}_polishedGenome_masked.fa | gzip > ${run}.fasta.gz
    run=${run}
    runShort=\${run:0:10}
    fp=\$(realpath ${run}.fasta.gz)
    echo -e "\${runShort}\\t\${fp}" >> input.txt
    """
    
}
}
	
//    process runListCompare {
//        publishDir 'trees', overwrite: true, mode: 'copy'
//        
//        input:
//        file('consensus.fasta.gz*') from genomeConsensusFiles.collect()
//	file('input.txt*') from genomeInputFiles.collect()
//
//        output:
//        file("output_tree") into trees
//
//        script:
//        compareBase=params.compareSeqs
//        rlcPath=params.rlcPath
//        """
//        cp ${compareBase}/inputs.txt  inputs.txt
//        cp ${compareBase}/inputs.ini  inputs.ini
//        sed -i "s:REPLACETEXT:${compareBase}:g" inputs.txt
//	cat input.txt* >> inputs.txt
//	python ${rlcPath}/runListCompare.py inputs.ini
//
//	"""
//
//      }
}

    process genomeDepth {
        tag { run }
        publishDir 'genomeDepths', pattern: '*.csv', overwrite: true, mode: 'copy'
        
        input:
        set val(r),val(run),file('ref'),file("${run}.sorted.bam"),file("${run}.sorted.bam.bai") from aligned2

        output:
        set val(r),val(run),file("${run}_${r}_depth.csv") into genomeDepths

        script:
        scriptBase=params.base + '/scripts'
        """
        samtools depth -aa ${run}.sorted.bam > ${run}_${r}_depth.tsv
        coverageStats.py ${run}_${r}_depth.tsv ${run}

        mv coverage_stats.csv ${run}_${r}_depth.csv
        """
}

    process getAllRefVariantsClair {
        tag { run }
        publishDir 'refVariants', pattern: '*.csv', overwrite: true, mode: 'copy'

        input:
	set val(run), file("${run}_polishedGenome.fa"),file('ref.fa'),file('pysamstats.txt') from unmasked.combine(pysamstats_ch3, by:0)

        output:
        set val(run),file('*.csv') into allrefVariants

        script:
        geneBase=params.base + '/genes'
        """
        getRefVariants2.py -f ${run}_polishedGenome.fa \
		-r ref.fa \
		-o ${run}_all.csv \
		-s ${run} \
		-m ${geneBase}/ref_gene_muts.xlsx -p pysamstats.txt
	"""
    }
}

//#################### gene bining and local assemblies #######################
process mapGenes {                                                                   
    tag { run } 
    cpus 3
    //publishDir 'genebams', pattern: '*.bam', overwrite: true
                                                                                
    input:
    set val(r),file('ref'),val(run),file('fastq') from generefinputs.combine(maps4)

    output:
    set val(run),file('ref'), file("${run}.sorted.bam"),file('fastq') into geneBams

    script:
    """                                                                     
    minimap2 -t 2 -ax map-ont 'ref' 'fastq' | samtools view -F4 -bS - | samtools sort -o ${run}.sorted.bam       
    """
}

process binGenes {
    errorStrategy 'ignore'
    tag { run }

    input:     
    set val(run),file('ref'), file("${run}.sorted.bam"),file('full.fastq') from geneBams

    output:
    set val(run),file("*.fastq") into binnedGenes,binnedGenes2,binnedGenes3

    script:
    meta=params.ngstarMeta
    """
    samtools index ${run}.sorted.bam
    binGenes.py -f full.fastq -b ${run}.sorted.bam -m ${meta} -s 400 
    """
}

binnedGenes
    .transpose()
    .map { run, file -> tuple(run, file.baseName, 'mapBinGenes', file) }
    .filter{ it[1] in [/penA/,/POR/,/TBPB/] }
    .view()
    .into{ geneBins;geneBins2 }


process assembleGenes {
    tag { run + ' ' + gene }
    cpus 2
    errorStrategy 'retry' 
    //publishDir 'assemblyGenes', pattern: '*.fa', overwrite: true, mode: 'copy'

    input:
    set val(run),val(gene),val(oper),file("genes.fastq") from geneBins

    output:
    set val(run),val(gene),file('genes.fastq'),file("${run}_${gene}.fa") into assembledGenes
    set val(run),val(gene),val(oper),file("${run}_${gene}.fa") into assembledGenes2

    script:
    """
    wtdbg2 -t ${task.cpus} -i genes.fastq -fo contigs -L 3000 
    wtpoa-cns -t ${task.cpus} -i contigs.ctg.lay.gz -fo ${run}_${gene}.fa
    """
}

// #################### choose gene reference, map and polish #################
geneSeqs=assembledGenes2.mix(WGAgenebins)

process geneblastn {
    tag { run + ' ' + gene + ' ' + oper}
    //publishDir 'blasts', mode: 'copy', overwrite: true
	
    input:
    set val(run),val(gene),val(oper),file("${run}_${gene}.fa") from geneSeqs

    output:
    set val(run),val(gene),file("*.O6.blast.txt"),val(oper) into blastOutPolish

    script:
    db=params.blastdb
    """
    blastn -query ${run}_${gene}.fa -db $db -outfmt 6 -max_target_seqs 10000 > ${run}_${gene}_${oper}.O6.blast.txt
    """
}

process geneparseBlast6 {
    tag { sample + ' ' + gene + ' ' + oper}

    //publishDir 'ngstar', mode: 'copy', overwrite: true
	
    input:
    set val(sample),val(gene), file('O6.blast.txt'),val(oper) from blastOutPolish

    output:
    set val(sample),val(gene), file("${sample}_${gene}_${oper}.csv"),val(oper) into pb6_out_polish
    //set val(sample),file('windows/*.txt') into pb6_out

    script:
    meta=params.ngstarMeta
    """
    pb6.py -b O6.blast.txt -m $meta -o ${sample}_${gene}_${oper}.csv
    """
}

process chooseGeneRef {
    tag { sample + ' ' + gene + ' ' + oper}
    errorStrategy 'ignore'
    
    input:
    set val(r),file('ref'),val(sample),val(gene), file("${sample}_${gene}_${oper}.csv"),val(oper) from generefinputs2.combine(pb6_out_polish).filter{var_bool || it[3] in [/penA/,/POR/,/TBPB/]}

    output:
    set val(sample),val(gene),val(oper),file("${gene}.fa") into geneRefs
    
    script:
    """
    awk -F, 'FNR==2{print \$3}' ${sample}_${gene}_${oper}.csv > ${gene}.txt
    samtools faidx ref -r ${gene}.txt > ${gene}.fa
    """
}

fqGeneBinMix=geneBins2.mix(WGA_geneFQBins)

process remapGene {
    tag { run + ' ' + gene + ' ' + oper}

    input:
    set val(run),val(gene),val(oper),file("${gene}.fa"),file("genes.fastq") from geneRefs.combine(fqGeneBinMix, by:[0,1,2]).filter{var_bool || it[2] != Eval.me('/polishedRegion/')}

    output:
    set val(run),val(oper),val(gene),file("${gene}.fa"),file("genes.fastq"),file("${run}_${gene}.sorted.bam"),file("${run}_${gene}.sorted.bam.bai") into remappedGene,remappedGene2

    script:
    """
    minimap2 -t 2 -ax map-ont ${gene}.fa genes.fastq | samtools view -F4 -bS - | samtools sort -o ${run}_${gene}.sorted.bam
    samtools index ${run}_${gene}.sorted.bam
    """
}

process pysamStatsremap {
    tag { run + ' ' + gene + ' ' + oper }

    input:
    set val(run),val(oper),val(gene),file("${gene}.fa"),file("genes.fastq"),file("${run}_${gene}.sorted.bam"),file("${run}_${gene}.sorted.bam.bai") from remappedGene2

    output:
    set val(run),val(oper),val(gene),file("${run}_pysamfile.txt") into pysamstatsremap_ch

    script:
    """
    pysamstats -t variation_strand ${run}_${gene}.sorted.bam -f ${gene}.fa > ${run}_pysamfile.txt
    """
}


process callRemapGene {
    tag { run + ' ' + gene + ' ' + oper }
    label 'clair'
    cpus 1

    input:
    set val(run),val(oper),val(gene),file("${gene}.fa"),file("genes.fastq"),file("${run}_${gene}.sorted.bam"),file("${run}_${gene}.sorted.bam.bai") from remappedGene

    output:
    set val(run),val(oper),val(gene),file("${gene}.fa"),file("${run}_${gene}.vcf") into polishedRemapGene
    
    script:
    CLAIRMODEL=params.CLAIRMODEL
    """
    cntname=`(grep '>' ${gene}.fa | cut -c 2-)`
    echo \$cntname
    samtools faidx ${gene}.fa

    clair.py callVarBam \
        --ctgName \$cntname \
        --chkpnt_fn ${CLAIRMODEL} \
        --ref_fn ${gene}.fa \
        --bam_fn ${run}_${gene}.sorted.bam \
        --sampleName ${run} \
        --minCoverage 1 \
        --threads ${task.cpus} \
        --call_fn ${run}_${gene}.vcf
    """
}

process makeRemapGeneConsensus {
    errorStrategy 'ignore'
    tag { run + ' ' + gene + ' ' + operRG }

    input:
    set val(run),val(oper),val(gene),file("${run}_${gene}.fa"),file("${run}_${gene}.vcf"),file('pysamstats.txt') from polishedRemapGene.combine(pysamstatsremap_ch, by:[0,1,2])

    output:
    set val(run),val(gene),file("${run}_${gene}_${operRG}.fa"),val(operRG) into RGpcons,RGpcons2
    set val(run),file("${run}_${gene}_filtered.vcf") into filteredVCFs
    
    script:
    operRG=oper + 'RemapGene'
    modelBase=params.base + '/models'
    """
    filterVCF_model.py -i ${run}_${gene}.vcf \
		-v clair \
		-o ${run}_${gene}_filtered.vcf \
		-m ${modelBase}/clair_composite_model.sav  \
		-p pysamstats.txt \
		-f 0

    nanopolish vcf2fasta --skip-checks -g ${run}_${gene}.fa ${run}_${gene}_filtered.vcf > ${run}_${gene}_${operRG}.fa
    """

}

process geneblastnRemapGene {
    tag { run + ' ' + gene + ' ' + oper}
    //publishDir 'blasts', mode: 'copy', overwrite: true
	
    input:
    set val(run),val(gene),file("${run}_${gene}.fa"),val(oper) from RGpcons

    output:
    set val(run),val(gene),file("*.O6.blast.txt"),val(oper) into blastOutRGpcon

    script:
    db=params.blastdb
    """
    blastn -query ${run}_${gene}.fa -db $db -outfmt 6 -max_target_seqs 10000 > ${run}_${gene}_${oper}.O6.blast.txt

    """
}

process geneparseBlast6RemapGene {
    tag { sample + ' ' + gene + ' ' + oper}

    input:
    set val(sample),val(gene), file('O6.blast.txt'),val(oper) from blastOutRGpcon

    output:
    set val(sample),val(gene), file("${sample}_${gene}_${oper}.csv"),val(oper) into pb6_out_RemapGene
    set val(sample), val(oper), val(gene), file("${sample}_${gene}_${oper}.csv") into mast_POR
    set val(sample),val(oper),val(gene),file("${sample}_${gene}_${oper}.csv") into mast_TBPB

    script:
    meta=params.ngstarMeta
    scriptBase=params.base + '/scripts'
    """
    pb6.py -b O6.blast.txt -m $meta -o ${sample}_${gene}_${oper}.csv
    """
}

mast_POR
    .filter{ it[2] in [/POR/] }
    .view()
    .set{mast_POR_ch}

mast_TBPB
    .filter{ it[2] in [/TBPB/] }
    .view()
    .set{ mast_TBPB_ch }

mast_POR_ch
    .combine(mast_TBPB_ch, by:[0,1])
    .view()
    .set{ mast_ch }

process mast_type {
    tag { sample + ' ' + oper}
    publishDir 'mastTypes', mode: 'copy', overwrite: true

    input:
    set val(sample),val(oper), val(POR),
        file("${sample}_${POR}_${oper}.csv"),
        val(TBPB),
        file("${sample}_${TBPB}_${oper}.csv") from mast_ch

    output:
    set val(sample),val(oper),file("${sample}_${oper}_mast_type.csv") into mastTypes_ch

    script:
    geneBase=params.base + '/genes'
    """
    mastType.py \
        ${geneBase}/st.txt \
        ${sample}_${POR}_${oper}.csv \
        ${sample}_${TBPB}_${oper}.csv \
        ${sample}_${oper} 
    """
}

process getPenA {
    tag { run + ' ' + gene + ' ' + oper}
    publishDir 'penA', mode: 'copy', overwrite: true

    input:
    set val(run),val(gene),file("${run}_${gene}.fa"),val(oper) from RGpcons2.filter{it[1] in [/penA/]}

    output:
    set val(run),val(gene),file('*.csv') into penAOut

    script:
    geneBase=params.base + '/genes'
    scriptBase=params.base + '/scripts'
    """

    sixpack -mstart -sequence ${run}_${gene}.fa -outfile penA.sixpack -outseq ${run}_${gene}.prots.fa
    blastp -query ${run}_${gene}.prots.fa -db ${geneBase}/penA_alleles_prot.fasta -evalue 0.001 -outfmt 5 > bp5.txt
    blastn -query ${run}_${gene}.fa -db ${geneBase}/penA.fasta -evalue 0.001 -outfmt 6 > bn6.txt
    getPenA.py -b bp5.txt -n bn6.txt -s ${run} -d ${gene} -op ${oper} -m ${geneBase}/penA_alleles_metadata.xlsx

    """
}
